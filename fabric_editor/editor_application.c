#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/array.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/error.h>
#include <core/log.h>
#include <core/memory_stats.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>
#include <core/unit_tests.h>

#include <core/stretchy_buffers.inl>

#include <modules/entity_core/camera_component.h>
#include <modules/entity_core/entity_api.h>
#include <modules/entity_core/selected_component.h>
#include <modules/entity_core/transform_component.h>
#include <modules/renderer/mesh_component.h>
#include <modules/ui/ui.h>
#include <modules/window/window_api.h>

#include <cglm/struct.h>
#include <string.h>
#include <vendor/cimgui/cimgui.h>
#include <vendor/cimgui/cimguizmo.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>
#include <vulkan/vulkan.h>

#include <modules/asset_cache/asset_cache.h>
#include <modules/asset_import/asset_import.h>
#include <modules/editor_panels/asset_browser.h>
#include <modules/editor_panels/log.h>
#include <modules/editor_panels/properties.h>
#include <modules/editor_panels/scene_graph.h>
#include <modules/renderer/renderer.h>
#include <modules/ui/style_colors.inl>
#include <modules/ui_widgets/ui_widgets.h>
#include <modules/vulkan_backend/vulkan_backend.h>

static struct fa_logger_api* fa_logger_api;
static struct fa_os_api* fa_os_api;
static struct fa_memory_stats_api* fa_memory_stats_api;
static struct fa_window_api* fa_window_api;
static struct fa_vulkan_backend_api* fa_vulkan_backend_api;
static struct fa_renderer_api* fa_renderer_api;
static struct fa_ui_api* fa_ui_api;
static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_asset_cache_api* fa_asset_cache_api;
static struct fa_entity_api* fa_entity_api;
static struct fa_properties_panel_api* fa_properties_panel_api;
static struct fa_scene_graph_panel_api* fa_scene_graph_panel_api;
static struct fa_log_panel_api* fa_log_panel_api;
static struct fa_asset_browser_panel_api* fa_asset_browser_panel_api;
static struct fa_unit_test_api* fa_unit_test_api;

// Uncomment this line to enable conservative viewport resizing (only resizes the framebuffer when
// the manual resize operation finished
#define DISABLE_CONSERVATIVE_RESIZE 1

typedef struct fa_application_t
{
    // #todo: multiple windows
    /** Store a handle to the main editor window */
    fa_window_t window_handle;

    fa_render_backend_t* render_backend;

    fa_clock_t clock;
    double time;
    double delta_time;

    fa_entity_context_t* entity_ctx;
    fa_entity_t main_camera_entity;
} fa_application_t;

static fa_application_t* editor;

// temporary function to block out the editor layout until each panel is implemented
static void draw_template_panels(void)
{
    FA_PROFILER_ZONE(ctx, true);

    // Draw Asset Panel
    {
        fa_asset_browser_panel_api->draw();
    }

    // Draw Log
    {
        fa_log_panel_api->draw();
    }

    // Draw Property Editor
    {
        fa_properties_panel_api->draw();
    }

    // Draw Scene Graph
    {
        fa_scene_graph_panel_api->draw();
    }

    igShowDemoWindow(NULL);
    FA_PROFILER_ZONE_END(ctx);
}

static void draw_main_menubar(void)
{
    FA_PROFILER_ZONE(ctx, true);
    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, style_color_surface);
    igPushStyleColor_U32(ImGuiCol_Border, CIM_COL32_BLACK_TRANS);
    igBeginMainMenuBar();
    igPopStyleColor(2);

    igPushStyleVar_Vec2(ImGuiStyleVar_WindowPadding, (ImVec2){ 15, 15 });
    igPushStyleColor_Vec4(ImGuiCol_PopupBg, style_color_active);
    igPushStyleColor_Vec4(ImGuiCol_Border, (ImVec4){ 0.4f, 0.4f, 0.4f, 1.f });

    if (igBeginMenu("File", true))
    {
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igEndMenu();
    }

    if (igBeginMenu("Edit", true))
    {
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igEndMenu();
    }

    static bool style_editor_open = false;
    if (igBeginMenu("Tools", true))
    {
        if (igMenuItem_BoolPtr("Style Editor", NULL, &style_editor_open, true))
        {
        }
        if (igMenuItem_Bool("Log memory stats", NULL, false, true))
        {
            fa_memory_stats_api->log_mem_stats();
        }
        if (igMenuItem_Bool("Run unit tests", NULL, false, true))
        {
            fa_unit_test_api->run_tests_all();
        }
        igEndMenu();
    }

    if (igBeginMenu("Windows", true))
    {
        igEndMenu();
    }

    igPopStyleVar(1);
    igPopStyleColor(2);

    // NOTE: pop all style vars/colors before showing the style editor to prevent
    // hiding the true style values.
    if (style_editor_open)
    {
        igBegin(ICON_FA_IMAGE " Style Editor", &style_editor_open, 0);
        igShowStyleEditor(NULL);
        igEnd();
    }

    igEndMainMenuBar();
    FA_PROFILER_ZONE_END(ctx);
}

static void draw_status_bar(void)
{
    FA_PROFILER_ZONE(ctx, true);

    const ImGuiStyle* style = igGetStyle();
    ImGuiViewport* viewport = igGetMainViewport();

    // Notify of viewport change so GetFrameHeight() can be accurate in case of DPI change
    igSetCurrentViewport(NULL, (ImGuiViewportP*)viewport);

    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, style_color_primary);

    if (igBeginViewportSideBar("StatusBar", viewport, ImGuiDir_Down, igGetTextLineHeight() + 1 * igGetStyle()->FramePadding.y, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_MenuBar))
    {
        if (igBeginMenuBar())
        {
            static double last_n_frametimes[50] = { 0. };
            static double mean_frametime = 0.;
            static uint32_t current_frame = 0;
            current_frame = (current_frame + 1) % FA_ARRAY_COUNT(last_n_frametimes);
            mean_frametime += (editor->delta_time - last_n_frametimes[current_frame]) / FA_ARRAY_COUNT(last_n_frametimes);
            last_n_frametimes[current_frame] = editor->delta_time;

            igSetCursorPosY(igGetCursorPosY() - igGetStyle()->ItemSpacing.y);

            igText("%.2f ms [%.0f]", mean_frametime * 1000., (1. / mean_frametime));

            // if the debugger is attached, add a little notification to the far right of the status bar.
            // an attached debugger disables our ability to hot reload.
            const char* text = "";
            const char* tooltip_text = "";
            if (fa_os_api->info->is_debugger_attached())
            {
                text = ICON_FA_BUG;
                tooltip_text = "Hot-reload disabled due to attached debugger";
            }
            else
            {
                text = ICON_FA_FIRE;
                tooltip_text = "Hot-reload active";
            }

            const float width = igGetWindowWidth();

            ImVec2 size;
            igCalcTextSize(&size, text, NULL, true, -1.f);
            igSameLine(width - (size.x + style->FramePadding.x + style->ItemSpacing.x), 0);

            igTextUnformatted(text, NULL);
            if (igIsItemHovered(0))
            {
                igBeginTooltip();
                igText(tooltip_text);
                igEndTooltip();
            }

            igEndMenuBar();
        }
        igEnd();
    }
    igPopStyleColor(1);
    FA_PROFILER_ZONE_END(ctx);
}

static void draw_dockspace(void)
{
    FA_PROFILER_ZONE(ctx, true);
    static ImGuiDockNodeFlags DockspaceFlags = ImGuiDockNodeFlags_None;

    // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
    // because it would be confusing to have two docking targets within each others.
    ImGuiWindowFlags WindowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    const ImGuiViewport* ImGuiMainViewport = igGetMainViewport();
    igSetNextWindowPos(ImGuiMainViewport->Pos, 0, (ImVec2){ 0, 0 });
    igSetNextWindowSize(ImGuiMainViewport->Size, 0);
    igSetNextWindowViewport(ImGuiMainViewport->ID);
    igPushStyleVar_Float(ImGuiStyleVar_WindowRounding, 0.0f);
    igPushStyleVar_Float(ImGuiStyleVar_WindowBorderSize, 0.0f);
    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, (ImVec4){ 0.14f, 0.14f, 0.14f, 1.00f });
    WindowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    WindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
    if (DockspaceFlags & ImGuiDockNodeFlags_PassthruCentralNode)
    {
        WindowFlags |= ImGuiWindowFlags_NoBackground;
    }

    // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
    // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
    // all active windows docked into it will lose their parent and become undocked.
    // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
    // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
    igPushStyleVar_Vec2(ImGuiStyleVar_WindowPadding, (ImVec2){ 0.0f, 0.0f });
    igBegin("DockSpace", NULL, WindowFlags);

    igPopStyleVar(1);
    igPopStyleColor(1);
    igPopStyleVar(2);

    const ImGuiID DockspaceID = igGetID_Str("DockSpace");
    igDockSpace(DockspaceID, (ImVec2){ 0.0f, 0.0f }, DockspaceFlags, NULL);

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

static bool scene_color_needs_resize = false;
static vec2s scene_framebuffer_size = { { 1280, 720 } };

static void draw_scene_viewport(fa_camera_component_t* camera_component)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint64_t scene_color_descriptor = fa_vulkan_backend_api->get_scene_color_descriptor();

    igBegin(ICON_FA_HASHTAG " Scene", NULL, 0);

    ImVec2 current_scene_panel_size;
    igGetContentRegionAvail(&current_scene_panel_size);

    // Resize when our framebuffer size is not equal to the current framebuffer size.
    // Wait until the mouse is released before triggered the resize so we don't end up
    // resizing every frame along the way.
    if ((current_scene_panel_size.x > 0 && current_scene_panel_size.y > 0) &&
        (scene_framebuffer_size.x != current_scene_panel_size.x ||
         scene_framebuffer_size.y != current_scene_panel_size.y))
    {
        if (FA_IS_DEFINED(DISABLE_CONSERVATIVE_RESIZE) || !igIsMouseDown(ImGuiMouseButton_Left))
        {
            scene_color_needs_resize = true;
            scene_framebuffer_size.x = current_scene_panel_size.x;
            scene_framebuffer_size.y = current_scene_panel_size.y;
        }
    }

    static bool is_moving = false;
    if (igIsWindowHovered(0) || is_moving)
    {
        // #todo: refactor into an input api
        static ImVec2 last_mouse_pos;
        ImVec2 mouse_pos;
        igGetMousePos(&mouse_pos);

        // #todo: needs refactoring
        const vec2s mouse_delta = { { mouse_pos.x - last_mouse_pos.x, mouse_pos.y - last_mouse_pos.y } };
        last_mouse_pos = mouse_pos;
        static float camera_speed = 0.0f;
        const float max_camera_speed = 10.f;
        const float camera_acceleration = max_camera_speed * 20.f;

        if (igIsMouseDragging(ImGuiMouseButton_Right, 0.f))
        {
            is_moving = true;
            camera_speed = glm_clamp(camera_speed + camera_acceleration * (float)editor->delta_time, 0, max_camera_speed);

            camera_component->yaw += mouse_delta.x * camera_speed * (float)editor->delta_time;
            camera_component->pitch -= mouse_delta.y * camera_speed * (float)editor->delta_time;
            fa_recompute_camera_matrices(camera_component);

            const float camera_move_speed = 25.f;
            if (igIsKeyDown(87))
            {
                camera_component->pos = glms_vec3_add(camera_component->pos, glms_vec3_scale(camera_component->forward, (float)editor->delta_time * camera_move_speed));
            }
            if (igIsKeyDown(83))
            {
                camera_component->pos = glms_vec3_add(camera_component->pos, glms_vec3_scale(camera_component->forward, -(float)editor->delta_time * camera_move_speed));
            }
            if (igIsKeyDown(68))
            {
                camera_component->pos = glms_vec3_add(camera_component->pos, glms_vec3_scale(camera_component->right, (float)editor->delta_time * camera_move_speed));
            }
            if (igIsKeyDown(65))
            {
                camera_component->pos = glms_vec3_add(camera_component->pos, glms_vec3_scale(camera_component->right, -(float)editor->delta_time * camera_move_speed));
            }
            if (igIsKeyDown(69))
            {
                camera_component->pos = glms_vec3_add(camera_component->pos, glms_vec3_scale(camera_component->up, (float)editor->delta_time * camera_move_speed));
            }
            if (igIsKeyDown(81))
            {
                camera_component->pos = glms_vec3_add(camera_component->pos, glms_vec3_scale(camera_component->up, -(float)editor->delta_time * camera_move_speed));
            }

            fa_recompute_camera_matrices(camera_component);
        }
        else
        {
            is_moving = false;
        }
    }

    const float cursor_pos_y = igGetCursorPosY();
    ImVec2 content_region;
    igGetContentRegionAvail(&content_region);

    igImage((ImTextureID)scene_color_descriptor,
            current_scene_panel_size,
            (ImVec2){ 0.f, 0.f },
            (ImVec2){ 1.f, 1.f },
            (ImVec4){ 1.f, 1.f, 1.f, 1.f },
            (ImVec4){ 0.f, 0.f, 0.f, 0.f });

    // menu buttons
    {
        const ImGuiStyle* style = igGetStyle();
        ImVec2 text_size;
        igCalcTextSize(&text_size, ICON_FA_PLAY, NULL, false, -1);

        const float button_size = text_size.x + 4 * style->FramePadding.x;
        const float center_pos = content_region.x / 2;

        igSetCursorPos(IM_VEC2(center_pos - button_size, cursor_pos_y + style->FramePadding.y));

        ImVec4 button_col = style_color_overlay_4;
        button_col.w = 0.5f;
        igPushStyleColor_Vec4(ImGuiCol_Button, button_col);
        igButton(ICON_FA_PLAY, (ImVec2){ button_size, 0.f });
        igSameLine(0, 2 * style->FramePadding.x);
        igButton(ICON_FA_PAUSE, (ImVec2){ button_size, 0.f });
        igPopStyleColor(1);
    }

    // gizmo
    {
        fa_entity_context_t* entity_ctx = editor->entity_ctx;
        const fa_component_type_t transform_type = fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME);
        const fa_component_type_t selected_type = fa_entity_api->component_type_get(FA_SELECTED_COMPONENT_NAME);
        const fa_entity_filter_t selected_transform_filter = fa_entity_api->entity_filter_create(entity_ctx, (fa_entity_filter_desc_t){ .type_ids = { transform_type, selected_type } });

        // #todo: support moving multiple entities simultaneously with the delta outputs from the manipulate
        if (fa_sbuff_num(selected_transform_filter.entities) == 1)
        {
            fa_transform_component_t* transform_component = fa_entity_api->component_get(entity_ctx, selected_transform_filter.entities[0], transform_type);
            static MODE mode = WORLD;
            static OPERATION op = TRANSLATE;

            ImGuizmo_SetDrawlist(igGetWindowDrawList());
            ImVec2 window_pos;
            igGetWindowPos(&window_pos);
            ImGuizmo_SetRect(window_pos.x, window_pos.y, scene_framebuffer_size.x, scene_framebuffer_size.y);
            if (ImGuizmo_Manipulate((const float*)camera_component->view.raw, (const float*)camera_component->proj.raw, op, mode, (float*)transform_component->mat.raw, NULL, NULL, NULL, NULL))
            {
                vec3s pos, rot, scale;
                ImGuizmo_DecomposeMatrixToComponents((const float*)transform_component->mat.raw, (float*)pos.raw, (float*)rot.raw, (float*)scale.raw);
                transform_component->pos = pos;
                transform_component->rot = rot;
                transform_component->scale = scale;
                fa_recompute_transform_matrix(transform_component);
            }
        }
    }

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

#pragma region Application Api Implementation

static bool editor_tick(fa_application_t* app)
{
    const fa_window_t window = app->window_handle;

    // Update frame time and delta time
    const fa_clock_t now = fa_os_api->time->now();
    const double delta_time = fa_os_api->time->time_sub(now, app->clock);

    app->clock = now;
    app->delta_time = delta_time;
    app->time += delta_time;

    const fa_component_type_t camera_type = fa_entity_api->component_type_get(FA_CAMERA_COMPONENT_NAME);
    fa_camera_component_t* camera_component = fa_entity_api->component_get(editor->entity_ctx, editor->main_camera_entity, camera_type);

    fa_window_api->update_window(window);
    fa_renderer_api->frame_begin();

    {
        FA_PROFILER_ZONE_N(ctx1, "draw ui", true);
        draw_dockspace();

        draw_main_menubar();

        draw_status_bar();

        draw_template_panels();

        draw_scene_viewport(camera_component);
        FA_PROFILER_ZONE_END(ctx1);
    }

    fa_view_info_t view_info;
    view_info.camera_pos = camera_component->pos;
    view_info.camera_proj = camera_component->proj;
    view_info.camera_view = camera_component->view;
    view_info.near_plane = camera_component->near_plane;
    view_info.far_plane = camera_component->far_plane;
    fa_renderer_api->frame_draw(&view_info);

    if (scene_color_needs_resize)
    {
        scene_color_needs_resize = false;
        fa_vulkan_backend_api->resize_scene_color_framebuffer((uint32_t)scene_framebuffer_size.x, (uint32_t)scene_framebuffer_size.y);

        // update the projection matrix with the new aspect ratio
        camera_component->aspect = scene_framebuffer_size.x / scene_framebuffer_size.y;
        fa_recompute_camera_matrices(camera_component);
    }

    return !fa_window_api->window_wants_close(window);
}

static void editor_destroy(fa_application_t* app)
{
    fa_window_api->destroy_window(app->window_handle);
}

static fa_application_t* editor_create(void)
{
    FA_PROFILER_ZONE(ctx, true);

    editor->entity_ctx = fa_entity_api->context_create("Editor");

    // setup the editor viewport camera
    {
        // #todo: this entity should be nameless so it doesn't appear in the scene graph
        editor->main_camera_entity = fa_entity_api->entity_create(editor->entity_ctx, "Editor Camera");
        const fa_component_type_t camera_type = fa_entity_api->component_type_get(FA_CAMERA_COMPONENT_NAME);
        fa_camera_component_t* camera_component = fa_entity_api->component_add(editor->entity_ctx, editor->main_camera_entity, camera_type);
        camera_component->pos = (vec3s){ -1.f, 2.f, 5.f };
        camera_component->near_plane = 0.001f;
        camera_component->far_plane = 100.f;
        camera_component->pitch = 0.f;
        camera_component->yaw = 0.f;
        camera_component->aspect = scene_framebuffer_size.x / scene_framebuffer_size.y;
        fa_recompute_camera_matrices(camera_component);
    }

    {
        const fa_entity_t sphere_entity = fa_entity_api->entity_create(editor->entity_ctx, "Sphere");
        fa_dynamic_mesh_component_t* sphere_mesh = fa_entity_api->component_add(editor->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_DYNAMIC_MESH_COMPONENT_NAME));
        fa_transform_component_t* transform = fa_entity_api->component_add(editor->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME));

        const fa_asset_handle_t sphere_mesh_handle = fa_asset_cache_api->load_asset("content/models/sphere.obj", FA_ASSET_TYPE_MESH);
        memcpy(sphere_mesh, fa_asset_cache_api->get_asset_data(sphere_mesh_handle), sizeof(fa_dynamic_mesh_component_t));

        fa_material_t* bamboo = fa_renderer_api->material_create();
        bamboo->albedo = fa_renderer_api->texture_create("content/textures/bamboo-wood/albedo.png");
        bamboo->metallic = fa_renderer_api->texture_create("content/textures/bamboo-wood/metalness.png");
        bamboo->roughness = fa_renderer_api->texture_create("content/textures/bamboo-wood/roughness.png");
        bamboo->normal = fa_renderer_api->texture_create("content/textures/bamboo-wood/normal.png");
        bamboo->ao = fa_renderer_api->texture_create("content/textures/white.png");
        sphere_mesh->material = bamboo;

        transform->pos = GLMS_VEC3_ZERO;
        fa_recompute_transform_matrix(transform);
    }
    //{
    //    const fa_entity_t sphere_entity = fa_entity_api->entity_create(editor->entity_ctx);
    //    fa_mesh_component_t* sphere_mesh = fa_entity_api->component_add(editor->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_MESH_COMPONENT_NAME));
    //    fa_transform_component_t* transform = fa_entity_api->component_add(editor->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME));

    //    const fa_asset_handle_t sphere_mesh_handle = fa_asset_cache_api->load_asset("content/models/sphere.obj", FA_ASSET_TYPE_MESH);
    //    memcpy(sphere_mesh, fa_asset_cache_api->get_asset_data(sphere_mesh_handle), sizeof(fa_mesh_component_t));

    //    fa_material_t* rustediron_material = fa_renderer_api->material_create();
    //    rustediron_material->albedo = fa_renderer_api->texture_create("content/textures/rustediron/albedo.png");
    //    rustediron_material->metallic = fa_renderer_api->texture_create("content/textures/rustediron/metalness.png");
    //    rustediron_material->roughness = fa_renderer_api->texture_create("content/textures/rustediron/roughness.png");
    //    rustediron_material->normal = fa_renderer_api->texture_create("content/textures/rustediron/normal.png");
    //    rustediron_material->ao = fa_renderer_api->texture_create("content/textures/white.png");
    //    sphere_mesh->material = rustediron_material;

    //    transform->mat = GLMS_MAT4_IDENTITY;
    //    transform->mat = glms_rotate(transform->mat, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
    //    transform->mat = glms_translate(transform->mat, (vec3s){ 2.5f, 0.f, 0.f });

    //    fa_renderer_api->mesh_create(sphere_mesh, transform);
    //}
    //{
    //    const fa_entity_t sphere_entity = fa_entity_api->entity_create(editor->entity_ctx);
    //    fa_mesh_component_t* sphere_mesh = fa_entity_api->component_add(editor->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_MESH_COMPONENT_NAME));
    //    fa_transform_component_t* transform = fa_entity_api->component_add(editor->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME));

    //    const fa_asset_handle_t sphere_mesh_handle = fa_asset_cache_api->load_asset("content/models/sphere.obj", FA_ASSET_TYPE_MESH);
    //    memcpy(sphere_mesh, fa_asset_cache_api->get_asset_data(sphere_mesh_handle), sizeof(fa_mesh_component_t));

    //    fa_material_t* bamboo_wood = fa_renderer_api->material_create();
    //    bamboo_wood->albedo = fa_renderer_api->texture_create("content/textures/bamboo-wood/albedo.png");
    //    bamboo_wood->metallic = fa_renderer_api->texture_create("content/textures/bamboo-wood/metalness.png");
    //    bamboo_wood->roughness = fa_renderer_api->texture_create("content/textures/bamboo-wood/roughness.png");
    //    bamboo_wood->normal = fa_renderer_api->texture_create("content/textures/bamboo-wood/normal.png");
    //    bamboo_wood->ao = fa_renderer_api->texture_create("content/textures/bamboo-wood/ao.png");
    //    sphere_mesh->material = bamboo_wood;

    //    transform->mat = GLMS_MAT4_IDENTITY;
    //    transform->mat = glms_rotate(transform->mat, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
    //    transform->mat = glms_translate(transform->mat, (vec3s){ -2.5f, 0.f, 0.f });

    //    fa_renderer_api->mesh_create(sphere_mesh, transform);
    //}

    {
        fa_point_light_t* pl = fa_renderer_api->light_point_create();
        pl->pos = (vec3s){ -5.f, 4.f, 1.f };
        pl->color = (vec3s){ 200.f, 200.f, 200.f };

        fa_point_light_t* pl2 = fa_renderer_api->light_point_create();
        pl2->pos = (vec3s){ 0.f, 0.f, 4.f };
        pl2->color = (vec3s){ 150.f, 150.f, 150.f };
    }

    editor->window_handle = fa_window_api->get_window();

    FA_PROFILER_ZONE_END(ctx);
    return (fa_application_t*)&editor;
}

static fa_application_t* editor_get(void)
{
    return (fa_application_t*)editor;
}

static void editor_exit(fa_application_t* app)
{
    fa_ui_api->destroy();

    fa_window_api->close_window(app->window_handle);
}

static fa_entity_context_t* get_entity_context(void)
{
    return editor->entity_ctx;
}

#pragma endregion

#pragma region Module

static struct fa_application_api editor_application_api = {
    .create = editor_create,
    .get = editor_get,
    .destroy = editor_destroy,
    .exit = editor_exit,
    .tick = editor_tick,
    .get_active_entity_context = get_entity_context,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = fa_get_api(FA_LOGGER_API_NAME);
    fa_window_api = fa_get_api(FA_WINDOW_API_NAME);
    fa_memory_stats_api = fa_get_api(FA_MEMORY_STATS_API_NAME);
    fa_vulkan_backend_api = fa_get_api(FA_VULKAN_BACKEND_API_NAME);
    fa_renderer_api = fa_get_api(FA_RENDERER_API_NAME);
    fa_ui_api = fa_get_api(FA_UI_API_NAME);
    fa_widgets_api = fa_get_api(FA_UI_WIDGETS_API_NAME);
    fa_os_api = fa_get_api(FA_OS_API_NAME);
    fa_asset_cache_api = fa_get_api(FA_ASSET_CACHE_API_NAME);
    fa_entity_api = fa_get_api(FA_ENTITY_API_NAME);
    fa_properties_panel_api = fa_get_api(FA_PROPERTIES_PANEL_API_NAME);
    fa_scene_graph_panel_api = fa_get_api(FA_SCENE_GRAPH_PANEL_API_NAME);
    fa_log_panel_api = fa_get_api(FA_LOG_PANEL_API_NAME);
    fa_asset_browser_panel_api = fa_get_api(FA_ASSET_BROWSER_PANEL_API_NAME);
    fa_unit_test_api = fa_get_api(FA_UNIT_TEST_API_NAME);

    fa_add_or_remove_api(&editor_application_api, FA_APPLICATION_API_NAME, load);

    editor = registry->static_variable(FA_APPLICATION_API_NAME, "editor", sizeof(fa_application_t));
}

#pragma endregion