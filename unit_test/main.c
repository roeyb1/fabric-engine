#pragma once

#include "core/memory_stats.h"
#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>
#include <core/unit_tests.h>

#include <cglm/struct.h>
#include <vulkan/vulkan.h>

#include <modules/renderer/renderer.h>
#include <modules/ui/ui.h>
#include <modules/vulkan_backend/vulkan_backend.h>
#include <modules/window/window_api.h>

struct fa_window_api* window_api;
struct fa_vulkan_backend_api* vulkan_backend_api;
struct fa_renderer_api* renderer_api;
struct fa_job_scheduler_api* job_api;
struct fa_application_api* app_api;
struct fa_ui_api* ui_api;

extern void register_core_tests(struct fa_unit_test_api*);

// Initialize the core engine subsystems
static void init_tests()
{
    fa_add_api(fa_api_registry, fa_os_api, FA_OS_API_NAME);
    fa_add_api(fa_api_registry, fa_logger_api, FA_LOGGER_API_NAME);
    fa_add_api(fa_api_registry, fa_allocator_api, FA_ALLOCATOR_API_NAME);

    fa_add_api(fa_api_registry, fa_unit_test_api, FA_UNIT_TEST_API_NAME);
    register_core_tests(fa_unit_test_api);

    fa_init_job_scheduler(fa_os_api->thread, fa_os_api->info->get_num_logical_processors(), 128, 128 * 1024);

    // there currently isn't a fiber api for non-win32 platforms so we can't use the job system there
    fa_module_api->load_module("modules/fa_entity_core");

    fa_module_api->load_module("modules/fa_asset_import");

    fa_module_api->load_module("modules/fa_asset_cache");

    fa_module_api->load_module("modules/fa_window");
    window_api = fa_api_registry->get(FA_WINDOW_API_NAME);

    fa_module_api->load_module("modules/fa_vulkan_backend");
    vulkan_backend_api = fa_api_registry->get(FA_VULKAN_BACKEND_API_NAME);

    fa_module_api->load_module("modules/fa_renderer");
    renderer_api = fa_api_registry->get(FA_RENDERER_API_NAME);

    fa_module_api->load_module("modules/fa_ui");
    ui_api = fa_api_registry->get(FA_UI_API_NAME);

    fa_module_api->load_module("modules/fa_ui_widgets");
}

// Shutdown the core engine subsystems and clean up
static void shutdown_tests()
{
    // #todo: figure out why this isn't working
    // fa_shutdown_job_scheduler();
    fa_module_api->unload_module("modules/fa_ui_widgets");

    fa_module_api->unload_module("modules/fa_ui");

    fa_module_api->unload_module("modules/fa_renderer");

    fa_module_api->unload_module("modules/fa_vulkan_backend");

    fa_module_api->unload_module("modules/fa_window");

    fa_module_api->unload_module("modules/fa_asset_import");

    fa_module_api->unload_module("modules/fa_entity_core");

    // print the memory stats when the engine is finished running so we can see if there were any leaks.
    fa_memory_stats_api->log_mem_stats();
}

int main(int argc, char** argv)
{
    init_tests();

    fa_unit_test_api->run_tests_all();

    shutdown_tests();
    return 0;
}