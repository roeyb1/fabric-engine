#pragma once

#include <core/core_types.h>

enum { MAX_FRAMES_IN_FLIGHT = 2 };

enum { MAX_SWAPCHAIN_IMAGES = 6 };

typedef struct fa_render_backend_t fa_render_backend_t;

typedef struct fa_vulkan_buffer_t
{
    VkBuffer buffer;
    VkDeviceMemory memory;
    VkBufferUsageFlags usage;
    VkMemoryPropertyFlags properties;
    size_t size;
    /* static_str */ const char* debug_name;
} fa_vulkan_buffer_t;

typedef struct fa_vulkan_texture_t
{
    VkImage image;
    VkImageView view;
    VkDeviceMemory memory;
    uint32_t binding_slot;
} fa_vulkan_texture_t;

typedef struct fa_draw_data_t
{
    uint32_t material_index; // #todo [roey]
} fa_draw_data_t;

typedef struct fa_gpu_scene_t
{
    fa_vulkan_buffer_t view_info_buffer[MAX_SWAPCHAIN_IMAGES];

    fa_vulkan_buffer_t global_transform_buffer[MAX_SWAPCHAIN_IMAGES];
    fa_vulkan_buffer_t global_vertex_buffer[MAX_SWAPCHAIN_IMAGES];
    fa_vulkan_buffer_t global_index_buffer[MAX_SWAPCHAIN_IMAGES];
    fa_vulkan_buffer_t global_material_buffer[MAX_SWAPCHAIN_IMAGES];
    fa_vulkan_buffer_t global_draw_data_buffer[MAX_SWAPCHAIN_IMAGES];

    // #todo [roey]: split this into individual batches per material?
    fa_vulkan_buffer_t command_draw_data_buffer[MAX_SWAPCHAIN_IMAGES];

    uint32_t draw_count;
} fa_gpu_scene_t;

typedef struct fa_vk_buffer_write_desc_t
{
    fa_vulkan_buffer_t buffer;
    const void* data;
    size_t dst_offset;
    size_t size;
} fa_vk_buffer_write_desc_t;

struct fa_vulkan_backend_api
{
    fa_render_backend_t* (*create)(void);
    void (*init_ui)(void);

    void (*destroy_ui)(void);
    void (*destroy)(void);

    uint32_t (*begin_frame)(void);

    void (*submit_scene)(const fa_gpu_scene_t* gpu_scene);

    fa_vulkan_buffer_t (*buffer_alloc)(size_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, const char* name);
    void (*buffer_free)(fa_vulkan_buffer_t buffer);
    void (*buffer_realloc)(fa_vulkan_buffer_t* buffer, size_t new_size);
    void (*buffer_set_data)(fa_vulkan_buffer_t buffer, void* data, size_t offset, size_t size);
    void (*batch_buffer_set_data)(/* sbuff */ fa_vk_buffer_write_desc_t* writes);

    fa_vulkan_texture_t (*texture_create)(uint32_t width, uint32_t height, void* pixel_data);
    void (*texture_destroy)(fa_vulkan_texture_t texture_id);

    VkDescriptorSet (*descriptor_get_bindless_set)(void);
    void (*descriptor_write)(uint32_t write_count, const VkWriteDescriptorSet* descriptor_writes);

    // #todo [roey]: refactor these into the renderer
    uint64_t (*get_scene_color_descriptor)(void);
    void (*resize_scene_color_framebuffer)(uint32_t new_width, uint32_t new_height);
};

#define FA_VULKAN_BACKEND_API_NAME "FA_VULKAN_BACKEND_API"
