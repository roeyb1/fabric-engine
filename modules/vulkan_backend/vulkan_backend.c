#include <vulkan/vulkan.h>

#include "vulkan_backend.h"
#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/array.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/error.h>
#include <core/log.h>
#include <core/os.h>
#include <core/stretchy_buffers.inl>

#include <modules/window/window_api.h>

#include "core/profiler.h"
#include <GLFW/glfw3.h>
#include <cglm/struct.h>

#include <cimgui/cimgui.h>
#include <cimgui/cimguizmo.h>
#include <cimgui/imgui/backends/imgui_impl_glfw.h>
#include <cimgui/imgui/backends/imgui_impl_vulkan.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static struct fa_logger_api* fa_logger_api;
static struct fa_allocator_api* fa_allocator_api;
static struct fa_os_api* fa_os_api;
static struct fa_window_api* fa_window_api;

#define FA_RENDER_USE_VALIDATION 1
#define FA_RENDER_USE_VSYNC 1

// ----------------------------------------------------------------------------------------------

enum {
    MAX_STORAGE_BUFFERS = 1024,
    MAX_SAMPLED_IMAGES = 1024,
    MAX_STORAGE_IMAGES = 1024,
    MAX_SAMPLERS = 1024,
    MAX_UNIFORM_BUFFERS = 1024,
};

enum { MAX_DRAWABLE_OBJECTS = 1024 };

typedef struct vertex_t
{
    vec3s position;
    vec3s normal;
    vec2s uv;
} vertex_t;

static VkVertexInputBindingDescription vertex_binding_desc = {
    .binding = 0,
    .stride = sizeof(vertex_t),
    .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
};

static VkVertexInputAttributeDescription vertex_attribute_descriptions[] = {
    {
        .binding = 0,
        .location = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(vertex_t, position),
    },
    {
        .binding = 0,
        .location = 1,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(vertex_t, normal),
    },
    {
        .binding = 0,
        .location = 2,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = offsetof(vertex_t, uv),
    },
};

typedef struct mesh_t
{
    /* sbuff */ vertex_t* vertices;
    /* sbuff */ uint32_t* indices;
} mesh_t;

// ----------------------------------------------------------------------------------------------

#define LOG_CATEGORY "Vulkan"

static const char* get_vk_error(VkResult result)
{
    // switch copied from glfw/vulkan.c
    switch (result)
    {
    case VK_SUCCESS:
        return "Success";
    case VK_NOT_READY:
        return "A fence or query has not yet completed";
    case VK_TIMEOUT:
        return "A wait operation has not completed in the specified time";
    case VK_EVENT_SET:
        return "An event is signaled";
    case VK_EVENT_RESET:
        return "An event is unsignaled";
    case VK_INCOMPLETE:
        return "A return array was too small for the result";
    case VK_ERROR_OUT_OF_HOST_MEMORY:
        return "A host memory allocation has failed";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY:
        return "A device memory allocation has failed";
    case VK_ERROR_INITIALIZATION_FAILED:
        return "Initialization of an object could not be completed for implementation-specific reasons";
    case VK_ERROR_DEVICE_LOST:
        return "The logical or physical device has been lost";
    case VK_ERROR_MEMORY_MAP_FAILED:
        return "Mapping of a memory object has failed";
    case VK_ERROR_LAYER_NOT_PRESENT:
        return "A requested layer is not present or could not be loaded";
    case VK_ERROR_EXTENSION_NOT_PRESENT:
        return "A requested extension is not supported";
    case VK_ERROR_FEATURE_NOT_PRESENT:
        return "A requested feature is not supported";
    case VK_ERROR_INCOMPATIBLE_DRIVER:
        return "The requested version of Vulkan is not supported by the driver or is otherwise incompatible";
    case VK_ERROR_TOO_MANY_OBJECTS:
        return "Too many objects of the type have already been created";
    case VK_ERROR_FORMAT_NOT_SUPPORTED:
        return "A requested format is not supported on this device";
    case VK_ERROR_SURFACE_LOST_KHR:
        return "A surface is no longer available";
    case VK_SUBOPTIMAL_KHR:
        return "A swapchain no longer matches the surface properties exactly, but can still be used";
    case VK_ERROR_OUT_OF_DATE_KHR:
        return "A surface has changed in such a way that it is no longer compatible with the swapchain";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
        return "The display used by a swapchain does not use the same presentable image layout";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
        return "The requested window is already connected to a VkSurfaceKHR, or to some other non-Vulkan API";
    case VK_ERROR_VALIDATION_FAILED_EXT:
        return "A validation layer found an error";
    default:
        return "ERROR: UNKNOWN VULKAN ERROR";
    }
}

#define VK_CHECK_RESULT(result) \
    check_vk_result(result)

static inline void check_vk_result(VkResult result)
{
    if (result != VK_SUCCESS)
        FA_LOG_CATEGORY(Fatal, "%s", get_vk_error(result));
}

// ----------------------------------------------------------------------------------------------

typedef struct transform_t
{
    mat4 transform;
} transform_t;

typedef struct draw_data_t
{
    uint32_t transform_offset;
    uint32_t vertex_offset;

    uint32_t pad[2]; // pad to vec4
} draw_data_t;

typedef struct vulkan_swapchain_t
{
    fa_window_t window;

    VkSwapchainKHR swapchain;
    VkSurfaceKHR surface;

    VkFormat format;
    VkExtent2D extent;

    VkImage images[MAX_SWAPCHAIN_IMAGES];
    VkImageView image_views[MAX_SWAPCHAIN_IMAGES];

    VkFormat depth_format;

    VkFramebuffer framebuffers[MAX_SWAPCHAIN_IMAGES];

    uint32_t image_count;

    VkSemaphore image_available[MAX_FRAMES_IN_FLIGHT];
    VkSemaphore render_finished[MAX_FRAMES_IN_FLIGHT];
} vulkan_swapchain_t;

typedef struct vulkan_bindless_setup_t
{
    VkDescriptorPool pool;
    VkDescriptorSetLayout layout;
    VkDescriptorSet set;

    uint32_t next_texture_slot;
} vulkan_bindless_setup_t;

typedef struct vulkan_device_t
{
    VkPhysicalDevice physical_device;
    VkPhysicalDeviceProperties2 physical_device_properties;
    VkPhysicalDeviceDescriptorIndexingProperties descriptor_indexing_properties;

    VkDevice device;

    uint32_t graphics_family_index;
    uint32_t present_family_index;
    VkQueue graphics_queue;
    VkQueue present_queue;

    vulkan_bindless_setup_t bindless_setup;
} vulkan_device_t;

typedef struct framebuffer_attachment_t
{
    VkImage image;
    VkDeviceMemory mem;
    VkImageView view;
} framebuffer_attachment_t;

typedef struct fa_pipeline_t
{
    VkPipelineLayout pipeline_layout;
    VkPipeline pipeline;

    VkRenderPass render_pass;
} fa_pipeline_t;

// for now I'm just going with an oop-type approach and dumping everything in this struct
// to work through the vulkan base. this MUST be restructured later.
typedef struct fa_render_backend_t
{
    VkInstance instance;
    VkDebugUtilsMessengerEXT debug_messenger;

    // #todo: eventually support multiple devices and swapchains (for multi display or multi window)
    vulkan_device_t device;
    vulkan_swapchain_t swapchain;

    VkRenderPass ui_renderpass;

    VkBuffer view_info[MAX_SWAPCHAIN_IMAGES];
    VkDeviceMemory view_info_memory[MAX_SWAPCHAIN_IMAGES];

    uint32_t current_image_index;

    VkSampler global_repeat_sampler;
    VkSampler global_clamp_sampler;

    fa_pipeline_t editor_grid_pipeline;

    // -----------------------------------------------------------------------------------------------------------------
    // #todo [roey]: refactor everything below this line
    VkDescriptorPool ui_descriptor_pool;
    VkDescriptorSet scene_texture_descriptor;

    VkPipelineLayout pipeline_layout;
    VkPipeline pipeline;

    VkCommandPool command_pool;
    VkCommandBuffer command_buffers[MAX_SWAPCHAIN_IMAGES];

    VkCommandPool transient_command_pool;

    VkCommandPool ui_command_pool;
    VkCommandBuffer ui_command_buffers[MAX_SWAPCHAIN_IMAGES];

    uint32_t scene_width, scene_height;
    framebuffer_attachment_t color, depth;
    VkFramebuffer scene_framebuffer;
    VkSampler scene_sampler;

    VkRenderPass scene_renderpass;

    uint32_t current_frame_index;
    VkFence in_flight_fences[MAX_FRAMES_IN_FLIGHT];
    VkFence images_in_flight[MAX_SWAPCHAIN_IMAGES];
    bool framebuffer_resized;
} fa_render_backend_t;

static fa_render_backend_t* render_backend;

#if FA_RENDER_USE_VALIDATION
static const char* validationLayers[] = { "VK_LAYER_KHRONOS_validation" };

PFN_vkSetDebugUtilsObjectNameEXT pfnSetDebugUtilsObjectName;
#endif

static const char* device_extensions[] = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_SHADER_DRAW_PARAMETERS_EXTENSION_NAME,
};

enum { MAX_INSTANCE_EXTENSIONS = 64 };
static const char* instance_extensions[MAX_INSTANCE_EXTENSIONS];
static uint32_t num_instance_extensions = 0;

// ----------------------------------------------------------------------------------------------

static VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
    void* user_data)
{
    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
        FA_LOG_CATEGORY(Warning, "validation layer: %s", callback_data->pMessage);
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
        FA_LOG_CATEGORY(Error, "validation layer: %s", callback_data->pMessage);
        break;
    default:
        break;
    }

    return VK_FALSE;
}

static void framebuffer_resize_callback(GLFWwindow* window, int width, int height)
{
    render_backend->framebuffer_resized = true;
}

VkResult create_debug_utils_messenger_ext(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* create_info, const VkAllocationCallbacks* allocator, VkDebugUtilsMessengerEXT* debug_messenger)
{
    const PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func)
    {
        return func(instance, create_info, allocator, debug_messenger);
    }

    return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void destroy_debug_utils_messenger_ext(VkInstance instance, VkDebugUtilsMessengerEXT debug_messenger, const VkAllocationCallbacks* allocator)
{
    const PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func)
    {
        func(instance, debug_messenger, allocator);
    }
}

static void populate_debug_messenger_create_info(VkDebugUtilsMessengerCreateInfoEXT* create_info)
{
    *create_info = (VkDebugUtilsMessengerCreateInfoEXT){
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        .pfnUserCallback = vk_debug_callback,
        .pUserData = NULL,
    };
}

static void setup_debug_messenger(void)
{
#if FA_RENDER_USE_VALIDATION
    VkDebugUtilsMessengerCreateInfoEXT create_info;
    populate_debug_messenger_create_info(&create_info);

    VK_CHECK_RESULT(create_debug_utils_messenger_ext(render_backend->instance, &create_info, NULL, &render_backend->debug_messenger));
#endif
}

static void create_surface()
{
    VK_CHECK_RESULT(glfwCreateWindowSurface(render_backend->instance, (GLFWwindow*)render_backend->swapchain.window.handle, NULL, &render_backend->swapchain.surface));
}

static void setup_vk_instance_extensions(void)
{
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    for (uint32_t i = 0; i < glfwExtensionCount; ++i)
    {
        instance_extensions[i] = glfwExtensions[i];
        num_instance_extensions++;
    }

#if FA_RENDER_USE_VALIDATION
    instance_extensions[num_instance_extensions++] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
#endif
}

static void create_vk_instance()
{
    check(glfwVulkanSupported());

    VkApplicationInfo appInfo = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "Fabric Engine",
        .applicationVersion = VK_MAKE_VERSION(0, 0, 1),
        .pEngineName = "Fabric Engine",
        .engineVersion = VK_MAKE_VERSION(0, 0, 1),
        .apiVersion = VK_API_VERSION_1_2,
    };

    VkInstanceCreateInfo createInfo = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &appInfo,
    };

    setup_vk_instance_extensions();

    FA_LOG_CATEGORY(Verbose, "required extensions:");
    for (uint32_t i = 0; i < num_instance_extensions; ++i)
    {
        FA_LOG_CATEGORY(Verbose, "\t %s", instance_extensions[i]);
    }

    createInfo.enabledExtensionCount = num_instance_extensions;
    createInfo.ppEnabledExtensionNames = instance_extensions;

#if FA_RENDER_USE_VALIDATION
    createInfo.enabledLayerCount = FA_ARRAY_COUNT(validationLayers);
    createInfo.ppEnabledLayerNames = validationLayers;

    VkValidationFeatureEnableEXT enabled_validation_features[] = {
        VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT,
        VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT,
    };

    const VkValidationFeaturesEXT validation_features = {
        .sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT,
        .enabledValidationFeatureCount = FA_ARRAY_COUNT(enabled_validation_features),
        .pEnabledValidationFeatures = enabled_validation_features,
    };

    VkDebugUtilsMessengerCreateInfoEXT debug_create_info;
    populate_debug_messenger_create_info(&debug_create_info);
    debug_create_info.pNext = &validation_features;

    createInfo.pNext = &debug_create_info;
#endif

    VK_CHECK_RESULT(vkCreateInstance(&createInfo, NULL, &render_backend->instance));
}

static bool check_device_extension_support(VkPhysicalDevice device)
{
    uint32_t extension_count = 0;
    vkEnumerateDeviceExtensionProperties(device, NULL, &extension_count, NULL);

    FA_CREATE_TEMP_ALLOC(temp_alloc);

    VkExtensionProperties* available_extensions = fa_temp_alloc(temp_alloc, sizeof(VkExtensionProperties) * extension_count);
    vkEnumerateDeviceExtensionProperties(device, NULL, &extension_count, available_extensions);

    const uint32_t device_extension_count = FA_ARRAY_COUNT(device_extensions);
    uint32_t found_count = 0;
    for (uint32_t i = 0; i < device_extension_count; ++i)
    {
        for (uint32_t j = 0; j < extension_count; ++j)
        {
            if (strcmp(device_extensions[i], available_extensions[j].extensionName) == 0)
            {
                found_count++;
                break;
            }
        }
    }

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
    return found_count == device_extension_count;
}

static bool is_physical_device_suitable(VkPhysicalDevice device)
{
    VkPhysicalDeviceProperties properties;
    VkPhysicalDeviceFeatures features;
    vkGetPhysicalDeviceProperties(device, &properties);
    vkGetPhysicalDeviceFeatures(device, &features);

    const bool supports_device_extensions = check_device_extension_support(device);
    // We will require all gpus to support vk 1.2
    return properties.apiVersion >= VK_API_VERSION_1_1 && supports_device_extensions;
}

static void get_queue_families(vulkan_device_t* vk_device)
{
    FA_CREATE_TEMP_ALLOC(temp_alloc);
    uint32_t queue_family_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(vk_device->physical_device, &queue_family_count, NULL);

    VkQueueFamilyProperties* queue_families = fa_temp_alloc(temp_alloc, sizeof(VkQueueFamilyProperties) * queue_family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(vk_device->physical_device, &queue_family_count, queue_families);

    for (uint32_t i = 0; i < queue_family_count; ++i)
    {
        VkBool32 present_support = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(vk_device->physical_device, i, render_backend->swapchain.surface, &present_support);

        if (present_support)
        {
            vk_device->present_family_index = i;
        }

        if (queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            vk_device->graphics_family_index = i;

            // if the same queue supports both present and graphics we will stop searching
            if (present_support)
            {
                break;
            }
        }
    }

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
}

static void pick_physical_device(void)
{
    FA_CREATE_TEMP_ALLOC(temp_alloc);
    uint32_t device_count = 0;
    vkEnumeratePhysicalDevices(render_backend->instance, &device_count, NULL);

    checkf(device_count > 0, "No physical devices found");

    VkPhysicalDevice* physical_devices = fa_temp_alloc(temp_alloc, sizeof(VkPhysicalDevice) * device_count);
    vkEnumeratePhysicalDevices(render_backend->instance, &device_count, physical_devices);

    for (uint32_t i = 0; i < device_count; ++i)
    {
        if (is_physical_device_suitable(physical_devices[i]))
        {
            render_backend->device.physical_device = physical_devices[i];
            break;
        }
    }

    checkf(render_backend->device.physical_device != VK_NULL_HANDLE, "Failed to find suitable physical device");

    get_queue_families(&render_backend->device);

    render_backend->device.descriptor_indexing_properties = (VkPhysicalDeviceDescriptorIndexingProperties){
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES,
        .pNext = NULL,
    };

    render_backend->device.physical_device_properties = (VkPhysicalDeviceProperties2){
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
        .pNext = &render_backend->device.descriptor_indexing_properties,
    };

    vkGetPhysicalDeviceProperties2(render_backend->device.physical_device, &render_backend->device.physical_device_properties);

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
}

static void create_logical_device(vulkan_device_t* device)
{
    float queue_priority = 1.f;

    uint32_t queue_count = 0;
    FA_CREATE_TEMP_ALLOC(temp_alloc);

    VkDeviceQueueCreateInfo* queue_create_infos = NULL;
    if (device->graphics_family_index == device->present_family_index)
    {
        queue_create_infos = fa_temp_alloc(temp_alloc, sizeof(VkDeviceQueueCreateInfo));

        *queue_create_infos = (VkDeviceQueueCreateInfo){
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = device->graphics_family_index,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority
        };
        queue_count = 1;
    }
    else
    {
        queue_create_infos = fa_temp_alloc(temp_alloc, 2 * sizeof(VkDeviceQueueCreateInfo));

        queue_create_infos[0] = (VkDeviceQueueCreateInfo){
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = device->graphics_family_index,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority
        };

        queue_create_infos[1] = (VkDeviceQueueCreateInfo){
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = device->present_family_index,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority
        };

        queue_count = 2;
    }

    VkPhysicalDeviceFeatures device_features = {
        .samplerAnisotropy = VK_TRUE,
        .multiDrawIndirect = VK_TRUE,
    };

    VkPhysicalDeviceVulkan12Features vk12_features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
        .descriptorIndexing = VK_TRUE,
        .shaderInputAttachmentArrayDynamicIndexing = VK_TRUE,
        .shaderUniformTexelBufferArrayDynamicIndexing = VK_TRUE,
        .shaderStorageTexelBufferArrayDynamicIndexing = VK_TRUE,
        .shaderUniformBufferArrayNonUniformIndexing = VK_TRUE,
        .shaderSampledImageArrayNonUniformIndexing = VK_TRUE,
        .shaderStorageBufferArrayNonUniformIndexing = VK_TRUE,
        .shaderStorageImageArrayNonUniformIndexing = VK_TRUE,
        .shaderInputAttachmentArrayNonUniformIndexing = VK_TRUE,
        .shaderUniformTexelBufferArrayNonUniformIndexing = VK_TRUE,
        .shaderStorageTexelBufferArrayNonUniformIndexing = VK_TRUE,
        .descriptorBindingUniformBufferUpdateAfterBind = VK_TRUE,
        .descriptorBindingSampledImageUpdateAfterBind = VK_TRUE,
        .descriptorBindingStorageImageUpdateAfterBind = VK_TRUE,
        .descriptorBindingStorageBufferUpdateAfterBind = VK_TRUE,
        .descriptorBindingUniformTexelBufferUpdateAfterBind = VK_TRUE,
        .descriptorBindingStorageTexelBufferUpdateAfterBind = VK_TRUE,
        .descriptorBindingUpdateUnusedWhilePending = VK_TRUE,
        .descriptorBindingPartiallyBound = VK_TRUE,
        .descriptorBindingVariableDescriptorCount = VK_TRUE,
        .runtimeDescriptorArray = VK_TRUE,
    };

    VkDeviceCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pQueueCreateInfos = queue_create_infos,
        .queueCreateInfoCount = queue_count,
        .pEnabledFeatures = &device_features,
        .enabledExtensionCount = FA_ARRAY_COUNT(device_extensions),
        .ppEnabledExtensionNames = device_extensions,
        .pNext = &vk12_features,
    };

#if FA_RENDER_USE_VALIDATION
    create_info.enabledLayerCount = FA_ARRAY_COUNT(validationLayers);
    create_info.ppEnabledLayerNames = validationLayers;
#else
    create_info.enabledLayerCount = 0;
#endif

    VK_CHECK_RESULT(vkCreateDevice(device->physical_device, &create_info, NULL, &device->device));

    vkGetDeviceQueue(device->device, device->graphics_family_index, 0, &device->graphics_queue);
    vkGetDeviceQueue(device->device, device->present_family_index, 0, &device->present_queue);

#if FA_RENDER_USE_VALIDATION
    // Get some device extension function pointers
    pfnSetDebugUtilsObjectName = (PFN_vkSetDebugUtilsObjectNameEXT)vkGetDeviceProcAddr(render_backend->device.device, "vkSetDebugUtilsObjectNameEXT");
#endif

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
}

static void create_bindless(vulkan_device_t* device)
{
    const uint32_t max_storage_buffers = device->descriptor_indexing_properties.maxPerStageDescriptorUpdateAfterBindStorageBuffers;
    const uint32_t max_sampled_images = device->descriptor_indexing_properties.maxPerStageDescriptorUpdateAfterBindSampledImages;
    const uint32_t max_storage_images = device->descriptor_indexing_properties.maxPerStageDescriptorUpdateAfterBindStorageImages;
    const uint32_t max_samplers = device->descriptor_indexing_properties.maxPerStageDescriptorUpdateAfterBindSamplers;
    const uint32_t max_uniform_buffers = device->descriptor_indexing_properties.maxPerStageDescriptorUpdateAfterBindUniformBuffers;

    uint32_t num_storage_buffers = MAX_STORAGE_BUFFERS;
    if (max_storage_buffers < MAX_STORAGE_BUFFERS)
    {
        FA_LOG_CATEGORY(Warning, "Device only supports a maximum of %d storage buffers. %d requested", max_storage_buffers, MAX_STORAGE_BUFFERS);
        num_storage_buffers = max_storage_buffers;
    }

    uint32_t num_sampled_images = MAX_SAMPLED_IMAGES;
    if (max_sampled_images < MAX_SAMPLED_IMAGES)
    {
        FA_LOG_CATEGORY(Warning, "Device only supports a maximum of %d sampled images. %d requested", max_sampled_images, MAX_SAMPLED_IMAGES);
        num_sampled_images = max_sampled_images;
    }

    uint32_t num_storage_images = MAX_STORAGE_IMAGES;
    if (max_storage_images < MAX_STORAGE_IMAGES)
    {
        FA_LOG_CATEGORY(Warning, "Device only supports a maximum of %d storage images. %d requested", max_storage_images, MAX_STORAGE_IMAGES);
        num_storage_images = max_storage_images;
    }

    uint32_t num_samplers = MAX_SAMPLERS;
    if (max_samplers < MAX_SAMPLERS)
    {
        FA_LOG_CATEGORY(Warning, "Device only supports a maximum of %d storage buffers. %d requested", max_samplers, MAX_SAMPLERS);
        num_samplers = max_samplers;
    }

    uint32_t num_uniform_buffers = MAX_UNIFORM_BUFFERS;
    if (max_uniform_buffers < MAX_UNIFORM_BUFFERS)
    {
        FA_LOG_CATEGORY(Warning, "Device only supports a maximum of %d uniform buffers. %d requested", max_uniform_buffers, MAX_UNIFORM_BUFFERS);
        num_samplers = max_samplers;
    }

    const VkDescriptorPoolSize pool_sizes[] = {
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, num_storage_buffers },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, num_sampled_images },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, num_storage_images },
        { VK_DESCRIPTOR_TYPE_SAMPLER, num_samplers },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, num_uniform_buffers },
    };

    const VkDescriptorPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT | VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT,
        .maxSets = 1,
        .poolSizeCount = FA_ARRAY_COUNT(pool_sizes),
        .pPoolSizes = pool_sizes,
    };

    VK_CHECK_RESULT(vkCreateDescriptorPool(device->device, &pool_info, NULL, &device->bindless_setup.pool));

    const VkDescriptorSetLayoutBinding layout_bindings[] = {
        { .binding = 0, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = num_storage_buffers, .stageFlags = VK_SHADER_STAGE_ALL },
        { .binding = 1, .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, .descriptorCount = num_sampled_images, .stageFlags = VK_SHADER_STAGE_ALL },
        { .binding = 2, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, .descriptorCount = num_storage_images, .stageFlags = VK_SHADER_STAGE_ALL },
        { .binding = 3, .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER, .descriptorCount = num_samplers, .stageFlags = VK_SHADER_STAGE_ALL },
        { .binding = 4, .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = num_uniform_buffers, .stageFlags = VK_SHADER_STAGE_ALL },
    };

    VkDescriptorBindingFlags flags[] = {
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
    };
    VkDescriptorSetLayoutBindingFlagsCreateInfo binding_flags = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO,
        .bindingCount = FA_ARRAY_COUNT(layout_bindings),
        .pBindingFlags = flags,
    };

    const VkDescriptorSetLayoutCreateInfo layout_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = FA_ARRAY_COUNT(layout_bindings),
        .pBindings = layout_bindings,
        .pNext = &binding_flags,
        .flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT,
    };

    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device->device, &layout_info, NULL, &device->bindless_setup.layout));

    const VkDescriptorSetAllocateInfo set_alloc_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = device->bindless_setup.pool,
        .descriptorSetCount = 1,
        .pSetLayouts = &device->bindless_setup.layout,
    };

    VK_CHECK_RESULT(vkAllocateDescriptorSets(device->device, &set_alloc_info, &device->bindless_setup.set));
}

static void destroy_bindless(vulkan_device_t* device)
{
    vkDestroyDescriptorSetLayout(device->device, device->bindless_setup.layout, NULL);
    vkDestroyDescriptorPool(device->device, device->bindless_setup.pool, NULL);
}

static VkSurfaceFormatKHR choose_swap_surface_format(const VkSurfaceFormatKHR* available_formats, uint32_t num)
{
    check(num > 0);

    for (uint32_t i = 0; i < num; ++i)
    {
        if (available_formats[i].format == VK_FORMAT_B8G8R8A8_UNORM && available_formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            return available_formats[i];
        }
    }
    return available_formats[0];
}

static VkPresentModeKHR choose_present_mode(const VkPresentModeKHR* present_modes, uint32_t num)
{
    check(num > 0);

    for (uint32_t i = 0; i < num; ++i)
    {
        if (FA_IS_DEFINED(FA_RENDER_USE_VSYNC))
        {
            if (present_modes[i] == VK_PRESENT_MODE_FIFO_KHR)
            {
                return present_modes[i];
            }
        }
        else
        {
            if (present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
            {
                return present_modes[i];
            }
        }
    }
    return VK_PRESENT_MODE_FIFO_KHR;
}

static VkExtent2D choose_swap_extent(const VkSurfaceCapabilitiesKHR* capabilities)
{
    if (capabilities->currentExtent.width != UINT32_MAX)
    {
        return capabilities->currentExtent;
    }
    else
    {
        int width, height;
        glfwGetFramebufferSize((GLFWwindow*)render_backend->swapchain.window.handle, &width, &height);

        VkExtent2D actual_extent = {
            .width = width,
            .height = height
        };

        actual_extent.width = fa_clamp(actual_extent.width, capabilities->minImageExtent.width, capabilities->maxImageExtent.width);
        actual_extent.height = fa_clamp(actual_extent.height, capabilities->minImageExtent.height, capabilities->maxImageExtent.height);

        return actual_extent;
    }
}

static VkImageView create_image_view(VkImage image, VkFormat format, VkImageAspectFlags aspect_flags)
{
    const VkImageViewCreateInfo view_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = format,
        .subresourceRange.aspectMask = aspect_flags,
        .subresourceRange.baseMipLevel = 0,
        .subresourceRange.levelCount = 1,
        .subresourceRange.baseArrayLayer = 0,
        .subresourceRange.layerCount = 1,
    };

    VkImageView view;
    VK_CHECK_RESULT(vkCreateImageView(render_backend->device.device, &view_info, NULL, &view));

    return view;
}

static void create_swapchain(vulkan_swapchain_t* swapchain)
{
    FA_CREATE_TEMP_ALLOC(temp_alloc);

    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(render_backend->device.physical_device, swapchain->surface, &capabilities);
    uint32_t format_count = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(render_backend->device.physical_device, swapchain->surface, &format_count, NULL);
    VkSurfaceFormatKHR* formats = fa_temp_alloc(temp_alloc, format_count * sizeof(VkSurfaceFormatKHR));
    vkGetPhysicalDeviceSurfaceFormatsKHR(render_backend->device.physical_device, swapchain->surface, &format_count, formats);

    uint32_t present_mode_count = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(render_backend->device.physical_device, swapchain->surface, &present_mode_count, NULL);
    VkPresentModeKHR* present_modes = fa_temp_alloc(temp_alloc, present_mode_count * sizeof(VkPresentModeKHR));
    vkGetPhysicalDeviceSurfacePresentModesKHR(render_backend->device.physical_device, swapchain->surface, &present_mode_count, present_modes);

    VkSurfaceFormatKHR format = choose_swap_surface_format(formats, format_count);
    VkPresentModeKHR present_mode = choose_present_mode(present_modes, present_mode_count);
    VkExtent2D extent = choose_swap_extent(&capabilities);

    uint32_t image_count = capabilities.minImageCount + 1;
    if (capabilities.minImageCount > 0 && image_count > capabilities.maxImageCount)
    {
        image_count = capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR create_info = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = swapchain->surface,
        .minImageCount = image_count,
        .imageFormat = format.format,
        .imageColorSpace = format.colorSpace,
        .imageExtent = extent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = capabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = present_mode,
        .clipped = VK_TRUE,
        .oldSwapchain = NULL,
    };

    const uint32_t queue_family_indices[] = { render_backend->device.graphics_family_index, render_backend->device.present_family_index };

    if (render_backend->device.graphics_family_index != render_backend->device.present_family_index)
    {
        create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        create_info.queueFamilyIndexCount = 2;
        create_info.pQueueFamilyIndices = queue_family_indices;
    }
    else
    {
        create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        create_info.queueFamilyIndexCount = 0;
    }

    VK_CHECK_RESULT(vkCreateSwapchainKHR(render_backend->device.device, &create_info, NULL, &swapchain->swapchain));

    vkGetSwapchainImagesKHR(render_backend->device.device, swapchain->swapchain, &image_count, swapchain->images);
    checkf(image_count < MAX_SWAPCHAIN_IMAGES, "Only support maximum of %d swapchain images (%d requested)!", MAX_SWAPCHAIN_IMAGES, image_count);

    swapchain->image_count = image_count;
    swapchain->extent = extent;
    swapchain->format = format.format;

    for (uint32_t i = 0; i < image_count; ++i)
    {
        swapchain->image_views[i] = create_image_view(swapchain->images[i], swapchain->format, VK_IMAGE_ASPECT_COLOR_BIT);
    }

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
}

static void create_render_pass(void)
{
    const VkAttachmentDescription color_attachment = {
        .format = render_backend->swapchain.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };

    VkAttachmentReference color_attachment_ref = {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkSubpassDescription subpass = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attachment_ref,
        .pDepthStencilAttachment = NULL,
    };

    VkSubpassDependency dependency = {
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .srcAccessMask = 0,
        .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    };

    VkAttachmentDescription attachments[] = { color_attachment };

    const VkRenderPassCreateInfo render_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = FA_ARRAY_COUNT(attachments),
        .pAttachments = attachments,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 1,
        .pDependencies = &dependency,
    };

    VK_CHECK_RESULT(vkCreateRenderPass(render_backend->device.device, &render_pass_info, NULL, &render_backend->ui_renderpass));
}

static void create_scene_renderpass(void)
{
    const VkAttachmentDescription color_attachment = {
        .format = render_backend->swapchain.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    const VkAttachmentDescription depth_attachment = {
        .format = render_backend->swapchain.depth_format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentReference color_attachment_ref = {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentReference depth_attachment_ref = {
        .attachment = 1,
        .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };

    VkSubpassDescription subpass = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attachment_ref,
        .pDepthStencilAttachment = &depth_attachment_ref,
    };

    VkSubpassDependency dependencies[] = {
        {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .srcAccessMask = VK_ACCESS_SHADER_READ_BIT,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT,
        },
        {
            .srcSubpass = 0,
            .dstSubpass = VK_SUBPASS_EXTERNAL,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
            .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT,
        },
    };

    VkAttachmentDescription attachments[] = { color_attachment, depth_attachment };

    const VkRenderPassCreateInfo render_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = FA_ARRAY_COUNT(attachments),
        .pAttachments = attachments,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = FA_ARRAY_COUNT(dependencies),
        .pDependencies = dependencies,
    };

    VK_CHECK_RESULT(vkCreateRenderPass(render_backend->device.device, &render_pass_info, NULL, &render_backend->scene_renderpass));
}

static const char* read_shader_file(fa_block_alloc_t* allocator, const char* filename, uint32_t* out_size)
{
    FILE* file = fopen(filename, "rb");
    if (file == NULL)
    {
        char buffer[256];
        FA_LOG_CATEGORY(Fatal, "Failed to load shader file (%s). Current directory is %s", filename, fa_os_api->file->get_cwd(buffer, 256));
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    const uint32_t file_len = ftell(file);
    fseek(file, 0, 0);

    char* buffer = fa_temp_alloc(allocator, file_len * sizeof(char));
    fread(buffer, file_len, 1, file);
    fclose(file);

    *out_size = file_len;
    return buffer;
}

static VkShaderModule create_shader_module(const char* buffer, uint32_t code_size)
{
    const VkShaderModuleCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = code_size,
        .pCode = (const uint32_t*)buffer,
    };

    VkShaderModule shader_module;
    VK_CHECK_RESULT(vkCreateShaderModule(render_backend->device.device, &create_info, NULL, &shader_module));
    return shader_module;
}

static void create_graphics_pipeline(void)
{
    FA_CREATE_TEMP_ALLOC(temp_alloc);
    uint32_t vert_shader_code_size = 0;
    uint32_t frag_shader_code_size = 0;
    const char* vert_shader_code = read_shader_file(temp_alloc, "content/shaders/vert.spv", &vert_shader_code_size);
    const char* frag_shader_code = read_shader_file(temp_alloc, "content/shaders/pbr.frag.spv", &frag_shader_code_size);

    VkShaderModule vert_shader_module = create_shader_module(vert_shader_code, vert_shader_code_size);
    VkShaderModule frag_shader_module = create_shader_module(frag_shader_code, frag_shader_code_size);

    VkPipelineShaderStageCreateInfo vert_shader_stage_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = vert_shader_module,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo frag_shader_stage_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = frag_shader_module,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo shader_stages[] = { vert_shader_stage_info, frag_shader_stage_info };

    VkPipelineVertexInputStateCreateInfo vertex_input_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vertex_binding_desc,
        .vertexAttributeDescriptionCount = FA_ARRAY_COUNT(vertex_attribute_descriptions),
        .pVertexAttributeDescriptions = vertex_attribute_descriptions,
    };

    VkPipelineInputAssemblyStateCreateInfo input_assembly = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkViewport viewport = {
        .x = 0.f,
        .y = (float)render_backend->swapchain.extent.height,
        .width = (float)render_backend->swapchain.extent.width,
        .height = -(float)render_backend->swapchain.extent.height, // negative sign flips the y axis to respect opengl convention
        .minDepth = 0.f,
        .maxDepth = 1.f,
    };

    VkRect2D scissor = {
        .offset = { 0, 0 },
        .extent = render_backend->swapchain.extent,
    };

    VkPipelineViewportStateCreateInfo viewport_state = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor,
    };

    VkPipelineRasterizationStateCreateInfo rasterizer = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .lineWidth = 1.f,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.f,
        .depthBiasClamp = 0.f,
        .depthBiasSlopeFactor = 0.f,
    };

    VkPipelineMultisampleStateCreateInfo multisample = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .sampleShadingEnable = VK_FALSE,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .minSampleShading = 1.f,
        .pSampleMask = NULL,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };

    VkPipelineColorBlendAttachmentState color_blend_attach = {
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
    };

    VkPipelineColorBlendStateCreateInfo color_blending = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attach,
        .blendConstants = { 0.f, 0.f, 0.f, 0.f },
    };

    VkDynamicState dynamic_states[] = { VK_DYNAMIC_STATE_VIEWPORT };

    VkPipelineDynamicStateCreateInfo dynamic_state = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = FA_ARRAY_COUNT(dynamic_states),
        .pDynamicStates = dynamic_states,
    };

    VkPipelineLayoutCreateInfo pipeline_layout_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &render_backend->device.bindless_setup.layout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = NULL,
    };

    VK_CHECK_RESULT(vkCreatePipelineLayout(render_backend->device.device, &pipeline_layout_info, NULL, &render_backend->pipeline_layout));

    VkPipelineDepthStencilStateCreateInfo depth_stencil = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .minDepthBounds = 0.f,
        .maxDepthBounds = 1.f,
        .stencilTestEnable = VK_FALSE,
    };

    VkGraphicsPipelineCreateInfo pipeline_info = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = shader_stages,
        .pVertexInputState = &vertex_input_info,
        .pInputAssemblyState = &input_assembly,
        .pViewportState = &viewport_state,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisample,
        .pDepthStencilState = &depth_stencil,
        .pColorBlendState = &color_blending,
        .pDynamicState = &dynamic_state,
        .layout = render_backend->pipeline_layout,
        .renderPass = render_backend->scene_renderpass,
        .subpass = 0,
    };

    VK_CHECK_RESULT(vkCreateGraphicsPipelines(render_backend->device.device, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &render_backend->pipeline));

    vkDestroyShaderModule(render_backend->device.device, vert_shader_module, NULL);
    vkDestroyShaderModule(render_backend->device.device, frag_shader_module, NULL);

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
}

static void create_editor_plane_pipeline(fa_pipeline_t* editor_plane_pipeline)
{
    FA_CREATE_TEMP_ALLOC(temp_alloc);
    uint32_t vert_shader_code_size = 0;
    uint32_t frag_shader_code_size = 0;
    const char* vert_shader_code = read_shader_file(temp_alloc, "content/shaders/editor_grid.vert.spv", &vert_shader_code_size);
    const char* frag_shader_code = read_shader_file(temp_alloc, "content/shaders/editor_grid.frag.spv", &frag_shader_code_size);

    VkShaderModule vert_shader_module = create_shader_module(vert_shader_code, vert_shader_code_size);
    VkShaderModule frag_shader_module = create_shader_module(frag_shader_code, frag_shader_code_size);

    VkPipelineShaderStageCreateInfo vert_shader_stage_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = vert_shader_module,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo frag_shader_stage_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = frag_shader_module,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo shader_stages[] = { vert_shader_stage_info, frag_shader_stage_info };

    VkPipelineVertexInputStateCreateInfo vertex_input_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 0,
        .pVertexBindingDescriptions = NULL,
        .vertexAttributeDescriptionCount = 0,
        .pVertexAttributeDescriptions = NULL,
    };

    VkPipelineInputAssemblyStateCreateInfo input_assembly = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkViewport viewport = {
        .x = 0.f,
        .y = (float)render_backend->swapchain.extent.height,
        .width = (float)render_backend->swapchain.extent.width,
        .height = -(float)render_backend->swapchain.extent.height, // negative sign flips the y axis to respect opengl convention
        .minDepth = 0.f,
        .maxDepth = 1.f,
    };

    VkRect2D scissor = {
        .offset = { 0, 0 },
        .extent = render_backend->swapchain.extent,
    };

    VkPipelineViewportStateCreateInfo viewport_state = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor,
    };

    VkPipelineRasterizationStateCreateInfo rasterizer = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .lineWidth = 1.f,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.f,
        .depthBiasClamp = 0.f,
        .depthBiasSlopeFactor = 0.f,
    };

    VkPipelineMultisampleStateCreateInfo multisample = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .sampleShadingEnable = VK_FALSE,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .minSampleShading = 1.f,
        .pSampleMask = NULL,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };

    VkPipelineColorBlendAttachmentState color_blend_attach = {
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .alphaBlendOp = VK_BLEND_OP_ADD,
    };

    VkPipelineColorBlendStateCreateInfo color_blending = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attach,
        .blendConstants = { 0.f, 0.f, 0.f, 0.f },
    };

    VkDynamicState dynamic_states[] = { VK_DYNAMIC_STATE_VIEWPORT };

    VkPipelineDynamicStateCreateInfo dynamic_state = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = FA_ARRAY_COUNT(dynamic_states),
        .pDynamicStates = dynamic_states,
    };

    VkPipelineLayoutCreateInfo pipeline_layout_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &render_backend->device.bindless_setup.layout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = NULL,
    };

    VK_CHECK_RESULT(vkCreatePipelineLayout(render_backend->device.device, &pipeline_layout_info, NULL, &editor_plane_pipeline->pipeline_layout));

    VkPipelineDepthStencilStateCreateInfo depth_stencil = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .minDepthBounds = 0.f,
        .maxDepthBounds = 1.f,
        .stencilTestEnable = VK_FALSE,
    };

    VkGraphicsPipelineCreateInfo pipeline_info = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = shader_stages,
        .pVertexInputState = &vertex_input_info,
        .pInputAssemblyState = &input_assembly,
        .pViewportState = &viewport_state,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisample,
        .pDepthStencilState = &depth_stencil,
        .pColorBlendState = &color_blending,
        .pDynamicState = &dynamic_state,
        .layout = editor_plane_pipeline->pipeline_layout,
        .renderPass = render_backend->scene_renderpass,
        .subpass = 0,
    };

    VK_CHECK_RESULT(vkCreateGraphicsPipelines(render_backend->device.device, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &editor_plane_pipeline->pipeline));

    vkDestroyShaderModule(render_backend->device.device, vert_shader_module, NULL);
    vkDestroyShaderModule(render_backend->device.device, frag_shader_module, NULL);

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
}

static void create_framebuffers(void)
{
    for (uint32_t i = 0; i < render_backend->swapchain.image_count; ++i)
    {
        VkImageView attachments[] = { render_backend->swapchain.image_views[i] };

        VkFramebufferCreateInfo framebuffer_create_info = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = render_backend->ui_renderpass,
            .attachmentCount = FA_ARRAY_COUNT(attachments),
            .pAttachments = attachments,
            .width = render_backend->swapchain.extent.width,
            .height = render_backend->swapchain.extent.height,
            .layers = 1,
        };

        VK_CHECK_RESULT(vkCreateFramebuffer(render_backend->device.device, &framebuffer_create_info, NULL, &render_backend->swapchain.framebuffers[i]));
    }
}

static void create_scene_framebuffer()
{
    VkImageView attachments[] = { render_backend->color.view, render_backend->depth.view };

    const VkFramebufferCreateInfo framebuffer_create_info = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = render_backend->scene_renderpass,
        .attachmentCount = FA_ARRAY_COUNT(attachments),
        .pAttachments = attachments,
        .width = render_backend->scene_width,
        .height = render_backend->scene_height,
        .layers = 1,
    };

    VK_CHECK_RESULT(vkCreateFramebuffer(render_backend->device.device, &framebuffer_create_info, NULL, &render_backend->scene_framebuffer));
}

static void create_command_pool(void)
{
    const VkCommandPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .queueFamilyIndex = render_backend->device.graphics_family_index,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
    };

    VK_CHECK_RESULT(vkCreateCommandPool(render_backend->device.device, &pool_info, NULL, &render_backend->command_pool));

    const VkCommandPoolCreateInfo transient_pool_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .queueFamilyIndex = render_backend->device.graphics_family_index,
        .flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
    };

    VK_CHECK_RESULT(vkCreateCommandPool(render_backend->device.device, &transient_pool_info, NULL, &render_backend->transient_command_pool));
}

static void create_ui_command_pool(void)
{
    const VkCommandPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .queueFamilyIndex = render_backend->device.graphics_family_index,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
    };

    VK_CHECK_RESULT(vkCreateCommandPool(render_backend->device.device, &pool_info, NULL, &render_backend->ui_command_pool));
}

static VkCommandBuffer begin_single_use_commands(void)
{
    const VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandPool = render_backend->transient_command_pool,
        .commandBufferCount = 1,
    };

    VkCommandBuffer cmd_buff;
    vkAllocateCommandBuffers(render_backend->device.device, &alloc_info, &cmd_buff);

    const VkCommandBufferBeginInfo begin_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    vkBeginCommandBuffer(cmd_buff, &begin_info);

    return cmd_buff;
}

static void end_single_use_commands(VkCommandBuffer cmd_buff)
{
    vkEndCommandBuffer(cmd_buff);

    const VkSubmitInfo submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &cmd_buff,
    };

    vkQueueSubmit(render_backend->device.graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
    vkQueueWaitIdle(render_backend->device.graphics_queue);

    vkFreeCommandBuffers(render_backend->device.device, render_backend->transient_command_pool, 1, &cmd_buff);
}

static uint32_t find_memory_type(uint32_t type_filter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties mem_props;
    vkGetPhysicalDeviceMemoryProperties(render_backend->device.physical_device, &mem_props);

    for (uint32_t i = 0; i < mem_props.memoryTypeCount; ++i)
    {
        if ((type_filter & (1 << i)) && (mem_props.memoryTypes[i].propertyFlags & properties) == properties)
        {
            return i;
        }
    }

    FA_LOG_CATEGORY(Error, "Failed to find a suitable memory type!");
    return UINT32_MAX;
}

static void create_buffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer* buffer_out, VkDeviceMemory* memory_out)
{
    FA_PROFILER_ZONE(ctx, true);
    const VkBufferCreateInfo buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VK_CHECK_RESULT(vkCreateBuffer(render_backend->device.device, &buffer_info, NULL, buffer_out));

    VkMemoryRequirements mem_reqs;
    vkGetBufferMemoryRequirements(render_backend->device.device, *buffer_out, &mem_reqs);

    const VkMemoryAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_reqs.size,
        .memoryTypeIndex = find_memory_type(mem_reqs.memoryTypeBits, properties),
    };

    VK_CHECK_RESULT(vkAllocateMemory(render_backend->device.device, &alloc_info, NULL, memory_out));

    vkBindBufferMemory(render_backend->device.device, *buffer_out, *memory_out, 0);
    FA_PROFILER_ZONE_END(ctx);
}

static void copy_data_to_buffer(VkDeviceMemory memory, const void* data_to_copy, VkDeviceSize offset, VkDeviceSize size)
{
    FA_PROFILER_ZONE(ctx, true);
    void* data;
    vkMapMemory(render_backend->device.device, memory, offset, size, 0, &data);
    memcpy(data, data_to_copy, size);
    vkUnmapMemory(render_backend->device.device, memory);
    FA_PROFILER_ZONE_END(ctx);
}

static void copy_buffer(VkBuffer src, VkBuffer dst, VkDeviceSize src_offset, VkDeviceSize dst_offset, VkDeviceSize size)
{
    FA_PROFILER_ZONE(ctx, true);
    const VkCommandBuffer cmd_buff = begin_single_use_commands();

    const VkBufferCopy copy_region = {
        .srcOffset = src_offset,
        .dstOffset = dst_offset,
        .size = size,
    };

    vkCmdCopyBuffer(cmd_buff, src, dst, 1, &copy_region);

    end_single_use_commands(cmd_buff);
    FA_PROFILER_ZONE_END(ctx);
}

static void set_object_name(uint64_t object_handle, VkDebugReportObjectTypeEXT object_type, const char* name)
{
#if FA_RENDER_USE_VALIDATION
    if (pfnSetDebugUtilsObjectName)
    {
        const VkDebugUtilsObjectNameInfoEXT name_info = {
            .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT,
            .objectHandle = object_handle,
            .objectType = (VkObjectType)object_type,
            .pObjectName = name,
        };

        pfnSetDebugUtilsObjectName(render_backend->device.device, &name_info);
    }
#endif
}

static void transition_image_layout(VkImage image, VkFormat format, VkImageLayout old_layout, VkImageLayout new_layout)
{
    const VkCommandBuffer cmd_buff = begin_single_use_commands();

    VkImageMemoryBarrier barrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .oldLayout = old_layout,
        .newLayout = new_layout,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .subresourceRange.baseMipLevel = 0,
        .subresourceRange.levelCount = 1,
        .subresourceRange.baseArrayLayer = 0,
        .subresourceRange.layerCount = 1,
    };

    VkPipelineStageFlags source = 0;
    VkPipelineStageFlags dest = 0;
    if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        source = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dest = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        source = VK_PIPELINE_STAGE_TRANSFER_BIT;
        dest = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
        FA_LOG_CATEGORY(Fatal, "Unsupported layout transition %d to %d", old_layout, new_layout);
    }

    vkCmdPipelineBarrier(cmd_buff,
                         source, dest,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &barrier);

    end_single_use_commands(cmd_buff);
}

static void copy_buffer_to_image(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
    const VkCommandBuffer cmd_buff = begin_single_use_commands();

    const VkBufferImageCopy region = {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .imageSubresource.mipLevel = 0,
        .imageSubresource.baseArrayLayer = 0,
        .imageSubresource.layerCount = 1,
        .imageOffset = { 0, 0 },
        .imageExtent = { width, height, 1 },
    };

    vkCmdCopyBufferToImage(cmd_buff, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    end_single_use_commands(cmd_buff);
}

static void create_image(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage* image, VkDeviceMemory* image_mem)
{
    const VkImageCreateInfo image_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .extent.width = width,
        .extent.height = height,
        .extent.depth = 1,
        .mipLevels = 1,
        .arrayLayers = 1,
        .format = format,
        .tiling = tiling,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .flags = 0,
    };

    VK_CHECK_RESULT(vkCreateImage(render_backend->device.device, &image_info, NULL, image));

    VkMemoryRequirements mem_reqs;
    vkGetImageMemoryRequirements(render_backend->device.device, *image, &mem_reqs);

    const VkMemoryAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_reqs.size,
        .memoryTypeIndex = find_memory_type(mem_reqs.memoryTypeBits, properties),
    };

    VK_CHECK_RESULT(vkAllocateMemory(render_backend->device.device, &alloc_info, NULL, image_mem));

    vkBindImageMemory(render_backend->device.device, *image, *image_mem, 0);
}

static void create_global_samplers(void)
{
    VkSamplerCreateInfo sampler_info = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .anisotropyEnable = VK_TRUE,
        .maxAnisotropy = render_backend->device.physical_device_properties.properties.limits.maxSamplerAnisotropy,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .mipLodBias = 0.f,
        .minLod = 0.f,
        .maxLod = 0.f,
    };

    VK_CHECK_RESULT(vkCreateSampler(render_backend->device.device, &sampler_info, NULL, &render_backend->global_repeat_sampler));

    sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    VK_CHECK_RESULT(vkCreateSampler(render_backend->device.device, &sampler_info, NULL, &render_backend->global_clamp_sampler));

    const VkDescriptorImageInfo repeat_sampler_descriptor = {
        .sampler = render_backend->global_repeat_sampler,
    };
    const VkDescriptorImageInfo clamp_sampler_descriptor = {
        .sampler = render_backend->global_clamp_sampler,
    };

    VkDescriptorImageInfo write_infos[] = { repeat_sampler_descriptor, clamp_sampler_descriptor };

    const VkWriteDescriptorSet clamp_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstBinding = 3,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER,
        .dstSet = render_backend->device.bindless_setup.set,
        .dstArrayElement = 0,
        .pImageInfo = write_infos,
    };
    vkUpdateDescriptorSets(render_backend->device.device, 1, &clamp_write, 0, NULL);
}

static VkFormat find_supported_format(VkFormat* candidates, uint32_t num_candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
    for (uint32_t i = 0; i < num_candidates; ++i)
    {
        VkFormatProperties properties;
        vkGetPhysicalDeviceFormatProperties(render_backend->device.physical_device, candidates[i], &properties);

        if (tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features) == features)
        {
            return candidates[i];
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features) == features)
        {
            return candidates[i];
        }
    }

    FA_LOG_CATEGORY(Error, "Failed to find supported format");
    return VK_FORMAT_UNDEFINED;
}

static VkFormat find_depth_format(void)
{
    VkFormat formats[] = { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT };
    return find_supported_format(formats, FA_ARRAY_COUNT(formats), VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

static void create_color_resources(void)
{
    const VkFormat color_format = render_backend->swapchain.format;

    create_image(render_backend->scene_width, render_backend->scene_height, color_format,
                 VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &render_backend->color.image, &render_backend->color.mem);

    render_backend->color.view = create_image_view(render_backend->color.image, color_format, VK_IMAGE_ASPECT_COLOR_BIT);
}

static void create_depth_resources(void)
{
    const VkFormat depth_format = find_depth_format();

    create_image(render_backend->scene_width, render_backend->scene_height, depth_format,
                 VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &render_backend->depth.image, &render_backend->depth.mem);

    render_backend->depth.view = create_image_view(render_backend->depth.image, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT);

    render_backend->swapchain.depth_format = depth_format;
}

static void create_scene_sampler(void)
{
    const VkSamplerCreateInfo sampler_info = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .anisotropyEnable = VK_TRUE,
        .maxAnisotropy = render_backend->device.physical_device_properties.properties.limits.maxSamplerAnisotropy,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .mipLodBias = 0.f,
        .minLod = 0.f,
        .maxLod = 0.f,
    };

    VK_CHECK_RESULT(vkCreateSampler(render_backend->device.device, &sampler_info, NULL, &render_backend->scene_sampler));
}

static void create_ui_descriptor_pool(void)
{
    const VkDescriptorPoolSize sampler_size = {
        .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
    };

    const VkDescriptorPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .poolSizeCount = 1,
        .pPoolSizes = &sampler_size,
        .maxSets = render_backend->swapchain.image_count,
        .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
    };

    VK_CHECK_RESULT(vkCreateDescriptorPool(render_backend->device.device, &pool_info, NULL, &render_backend->ui_descriptor_pool));
}

static void create_scene_descriptor_set(void)
{
    render_backend->scene_texture_descriptor = ImGui_ImplVulkan_AddTexture(render_backend->scene_sampler, render_backend->color.view, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
}

static void create_descriptor_sets(void)
{
    for (uint32_t i = 0; i < render_backend->swapchain.image_count; ++i)
    {
    }
}

static void create_command_buffers(void)
{
    const VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = render_backend->command_pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = render_backend->swapchain.image_count,
    };

    VK_CHECK_RESULT(vkAllocateCommandBuffers(render_backend->device.device, &alloc_info, render_backend->command_buffers));
}

static void create_ui_command_buffers(void)
{
    const VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = render_backend->ui_command_pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = render_backend->swapchain.image_count,
    };

    VK_CHECK_RESULT(vkAllocateCommandBuffers(render_backend->device.device, &alloc_info, render_backend->ui_command_buffers));
}

static void create_sync_objects(void)
{
    const VkSemaphoreCreateInfo semaphore_info = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
    };
    const VkFenceCreateInfo fence_info = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT,
    };

    for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        VK_CHECK_RESULT(vkCreateSemaphore(render_backend->device.device, &semaphore_info, NULL, &render_backend->swapchain.image_available[i]));
        VK_CHECK_RESULT(vkCreateSemaphore(render_backend->device.device, &semaphore_info, NULL, &render_backend->swapchain.render_finished[i]));
        VK_CHECK_RESULT(vkCreateFence(render_backend->device.device, &fence_info, NULL, &render_backend->in_flight_fences[i]));
    }

    for (uint32_t i = 0; i < MAX_SWAPCHAIN_IMAGES; ++i)
    {
        render_backend->images_in_flight[i] = VK_NULL_HANDLE;
    }
}

static void destroy_scene_buffer_resources(void)
{
    vkDestroySampler(render_backend->device.device, render_backend->scene_sampler, NULL);

    vkDestroyImageView(render_backend->device.device, render_backend->color.view, NULL);
    vkDestroyImage(render_backend->device.device, render_backend->color.image, NULL);
    vkFreeMemory(render_backend->device.device, render_backend->color.mem, NULL);

    vkDestroyImageView(render_backend->device.device, render_backend->depth.view, NULL);
    vkDestroyImage(render_backend->device.device, render_backend->depth.image, NULL);
    vkFreeMemory(render_backend->device.device, render_backend->depth.mem, NULL);

    vkDestroyFramebuffer(render_backend->device.device, render_backend->scene_framebuffer, NULL);

    vkFreeDescriptorSets(render_backend->device.device, render_backend->ui_descriptor_pool, 1, &render_backend->scene_texture_descriptor);

    vkFreeCommandBuffers(render_backend->device.device, render_backend->command_pool, (uint32_t)render_backend->swapchain.image_count, render_backend->command_buffers);
}

static void recreate_scene_buffer_resources(void)
{
    vkDeviceWaitIdle(render_backend->device.device);

    destroy_scene_buffer_resources();

    create_color_resources();
    create_depth_resources();
    create_scene_sampler();
    create_scene_framebuffer();
    create_scene_descriptor_set();
    create_command_buffers();
}

static void cleanup_swapchain(void)
{
    for (uint32_t i = 0; i < render_backend->swapchain.image_count; ++i)
    {
        vkDestroyFramebuffer(render_backend->device.device, render_backend->swapchain.framebuffers[i], NULL);
    }

    vkFreeCommandBuffers(render_backend->device.device, render_backend->command_pool, (uint32_t)render_backend->swapchain.image_count, render_backend->command_buffers);
    vkFreeCommandBuffers(render_backend->device.device, render_backend->ui_command_pool, (uint32_t)render_backend->swapchain.image_count, render_backend->ui_command_buffers);

    vkDestroyPipeline(render_backend->device.device, render_backend->editor_grid_pipeline.pipeline, NULL);
    vkDestroyPipelineLayout(render_backend->device.device, render_backend->editor_grid_pipeline.pipeline_layout, NULL);
    vkDestroyPipeline(render_backend->device.device, render_backend->pipeline, NULL);
    vkDestroyPipelineLayout(render_backend->device.device, render_backend->pipeline_layout, NULL);
    vkDestroyRenderPass(render_backend->device.device, render_backend->ui_renderpass, NULL);
    vkDestroyRenderPass(render_backend->device.device, render_backend->scene_renderpass, NULL);

    for (uint32_t i = 0; i < render_backend->swapchain.image_count; ++i)
    {
        vkDestroyImageView(render_backend->device.device, render_backend->swapchain.image_views[i], NULL);
    }
    vkDestroySwapchainKHR(render_backend->device.device, render_backend->swapchain.swapchain, NULL);

    for (uint32_t i = 0; i < render_backend->swapchain.image_count; ++i)
    {
        vkDestroyBuffer(render_backend->device.device, render_backend->view_info[i], NULL);
        vkFreeMemory(render_backend->device.device, render_backend->view_info_memory[i], NULL);
    }
}

static void recreate_swapchain(void)
{
    // don't recreate the swapchain if the window isn't visible. (Indicated by having width or height equal 0)
    int width = 0, height = 0;
    glfwGetFramebufferSize((GLFWwindow*)render_backend->swapchain.window.handle, &width, &height);
    if (width == 0 || height == 0)
    {
        return;
    }

    vkDeviceWaitIdle(render_backend->device.device);

    cleanup_swapchain();

    create_swapchain(&render_backend->swapchain);
    create_render_pass();
    create_scene_renderpass();
    create_editor_plane_pipeline(&render_backend->editor_grid_pipeline);
    create_graphics_pipeline();
    create_framebuffers();
    create_ui_command_buffers();
    create_command_buffers();

    render_backend->framebuffer_resized = false;
}

static void init_ui(void)
{
    create_ui_descriptor_pool();

    ImGui_ImplGlfw_InitForVulkan((struct GLFWwindow*)fa_window_api->get_window().handle, true);

    struct ImGui_ImplVulkan_InitInfo init_info = {
        .Instance = render_backend->instance,
        .PhysicalDevice = render_backend->device.physical_device,
        .Device = render_backend->device.device,
        .QueueFamily = render_backend->device.graphics_family_index,
        .Queue = render_backend->device.graphics_queue,
        .PipelineCache = NULL,
        .DescriptorPool = render_backend->ui_descriptor_pool,
        .Allocator = NULL,
        .MinImageCount = 2,
        .ImageCount = render_backend->swapchain.image_count,
        .CheckVkResultFn = check_vk_result,
    };

    ImGui_ImplVulkan_Init(&init_info, render_backend->ui_renderpass);

    // Upload fonts to GPU
    {
        const VkCommandBuffer cmd_buff = begin_single_use_commands();

        ImGui_ImplVulkan_CreateFontsTexture(cmd_buff);

        end_single_use_commands(cmd_buff);

        ImGui_ImplVulkan_DestroyFontUploadObjects();
    }

    create_scene_descriptor_set();
}

static fa_render_backend_t* create_vk_renderer(void)
{
    FA_PROFILER_ZONE(ctx, true);
    render_backend->swapchain.window = fa_window_api->get_window();
    glfwSetFramebufferSizeCallback((GLFWwindow*)(render_backend->swapchain.window.handle), framebuffer_resize_callback);

    create_vk_instance();
    setup_debug_messenger();
    create_surface();
    pick_physical_device();
    create_logical_device(&render_backend->device);

    create_bindless(&render_backend->device);
    create_global_samplers();

    create_swapchain(&render_backend->swapchain);
    create_command_pool();
    create_ui_command_pool();

    render_backend->scene_width = render_backend->swapchain.extent.width;
    render_backend->scene_height = render_backend->swapchain.extent.height;

    create_color_resources();
    create_scene_sampler();
    create_depth_resources();
    create_render_pass();
    create_scene_renderpass();
    create_editor_plane_pipeline(&render_backend->editor_grid_pipeline);
    create_graphics_pipeline();
    create_framebuffers();
    create_scene_framebuffer();

    create_descriptor_sets();
    create_command_buffers();
    create_ui_command_buffers();
    create_sync_objects();

    FA_PROFILER_ZONE_END(ctx);
    return render_backend;
}

static void update_ui(uint32_t current_image, ImDrawData* draw_data)
{
    FA_PROFILER_ZONE(ctx, true);

    const VkCommandBuffer cmd_buff = render_backend->ui_command_buffers[current_image];

    VK_CHECK_RESULT(vkResetCommandBuffer(cmd_buff, 0));

    const VkCommandBufferBeginInfo begin_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    VK_CHECK_RESULT(vkBeginCommandBuffer(cmd_buff, &begin_info));

    VkClearValue clear_colors[] = {
        { .color = { { 0.f, 0.f, 0.f, 1.f } } },
        { .depthStencil = { 1.f, 0 } }
    };

    const VkRenderPassBeginInfo render_pass_begin = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = render_backend->ui_renderpass,
        .framebuffer = render_backend->swapchain.framebuffers[current_image],
        .renderArea.offset = { 0, 0 },
        .renderArea.extent = render_backend->swapchain.extent,
        .clearValueCount = FA_ARRAY_COUNT(clear_colors),
        .pClearValues = clear_colors,
    };

    vkCmdBeginRenderPass(cmd_buff, &render_pass_begin, VK_SUBPASS_CONTENTS_INLINE);

    ImGui_ImplVulkan_RenderDrawData(draw_data, cmd_buff, VK_NULL_HANDLE);

    vkCmdEndRenderPass(cmd_buff);

    VK_CHECK_RESULT(vkEndCommandBuffer(cmd_buff));

    FA_PROFILER_ZONE_END(ctx);
}

static uint32_t begin_frame(void)
{
    FA_PROFILER_ZONE(ctx, true);

    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    igNewFrame();
    ImGuizmo_BeginFrame();

    FA_PROFILER_ZONE_END(ctx);
    return render_backend->current_image_index;
}

static void destroy_ui(void)
{
    vkDeviceWaitIdle(render_backend->device.device);

    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();

    vkDestroyDescriptorPool(render_backend->device.device, render_backend->ui_descriptor_pool, NULL);
}

static void destroy_vk(void)
{
    vkDeviceWaitIdle(render_backend->device.device);

    vkDestroySampler(render_backend->device.device, render_backend->scene_sampler, NULL);

    vkDestroyImageView(render_backend->device.device, render_backend->color.view, NULL);
    vkDestroyImage(render_backend->device.device, render_backend->color.image, NULL);
    vkFreeMemory(render_backend->device.device, render_backend->color.mem, NULL);

    vkDestroyImageView(render_backend->device.device, render_backend->depth.view, NULL);
    vkDestroyImage(render_backend->device.device, render_backend->depth.image, NULL);
    vkFreeMemory(render_backend->device.device, render_backend->depth.mem, NULL);

    vkDestroyFramebuffer(render_backend->device.device, render_backend->scene_framebuffer, NULL);

    for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        vkDestroyFence(render_backend->device.device, render_backend->in_flight_fences[i], NULL);
        vkDestroySemaphore(render_backend->device.device, render_backend->swapchain.render_finished[i], NULL);
        vkDestroySemaphore(render_backend->device.device, render_backend->swapchain.image_available[i], NULL);
    }

    cleanup_swapchain();

    vkDestroyCommandPool(render_backend->device.device, render_backend->ui_command_pool, NULL);
    vkDestroyCommandPool(render_backend->device.device, render_backend->transient_command_pool, NULL);
    vkDestroyCommandPool(render_backend->device.device, render_backend->command_pool, NULL);

    vkDestroySampler(render_backend->device.device, render_backend->global_clamp_sampler, NULL);
    vkDestroySampler(render_backend->device.device, render_backend->global_repeat_sampler, NULL);
    destroy_bindless(&render_backend->device);

    vkDestroyDevice(render_backend->device.device, NULL);

    destroy_debug_utils_messenger_ext(render_backend->instance, render_backend->debug_messenger, NULL);
    vkDestroySurfaceKHR(render_backend->instance, render_backend->swapchain.surface, NULL);
    vkDestroyInstance(render_backend->instance, NULL);
}

static uint64_t get_scene_color_descriptor(void)
{
    return (uint64_t)render_backend->scene_texture_descriptor;
}

static void resize_scene_color_framebuffer(uint32_t new_width, uint32_t new_height)
{
    FA_LOG_CATEGORY(Verbose, "Resized scene framebuffer");
    render_backend->scene_width = new_width;
    render_backend->scene_height = new_height;
    recreate_scene_buffer_resources();
}

#pragma region Public API

static VkDescriptorSet descriptor_get_bindless_set(void)
{
    return render_backend->device.bindless_setup.set;
}

static void descriptor_write(uint32_t write_count, const VkWriteDescriptorSet* descriptor_writes)
{
    vkUpdateDescriptorSets(render_backend->device.device, write_count, descriptor_writes, 0, NULL);
}

static void submit_scene(const fa_gpu_scene_t* gpu_scene)
{
    FA_PROFILER_ZONE(ctx, true);

    VkResult result;
    uint32_t image_index = UINT32_MAX;
    {
        FA_PROFILER_ZONE_N(ctx1, "acquire image", true);
        vkWaitForFences(render_backend->device.device, 1, &render_backend->in_flight_fences[render_backend->current_frame_index], VK_TRUE, UINT64_MAX);

        result = vkAcquireNextImageKHR(render_backend->device.device, render_backend->swapchain.swapchain, UINT64_MAX, render_backend->swapchain.image_available[render_backend->current_frame_index], VK_NULL_HANDLE, &image_index);
        FA_PROFILER_ZONE_END(ctx1);
    }

    if (result == VK_ERROR_OUT_OF_DATE_KHR)
    {
        recreate_swapchain();
        igEndFrame();
        FA_PROFILER_ZONE_END(ctx);
        return;
    }

    checkf(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR, "Failed to acquire swapchain image!");

    ImDrawData* ig_draw_data = NULL;
    {
        FA_PROFILER_ZONE_N(ctx1, "ig render", true);
        igRender();
        ig_draw_data = igGetDrawData();
        FA_PROFILER_ZONE_END(ctx1);
    }

    if (render_backend->images_in_flight[image_index] != VK_NULL_HANDLE)
    {
        vkWaitForFences(render_backend->device.device, 1, &render_backend->images_in_flight[image_index], VK_TRUE, UINT64_MAX);
    }

    render_backend->images_in_flight[image_index] = render_backend->in_flight_fences[render_backend->current_frame_index];

    // Record scene command buffer
    {
        FA_PROFILER_ZONE_N(ctx1, "Record command buffer", true);
        const VkCommandBuffer cmd_buff = render_backend->command_buffers[image_index];
        vkResetCommandBuffer(cmd_buff, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

        const VkCommandBufferBeginInfo begin_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = 0,
            .pInheritanceInfo = NULL,
        };

        VK_CHECK_RESULT(vkBeginCommandBuffer(cmd_buff, &begin_info));

        const VkViewport viewport = {
            .x = 0.f,
            .y = (float)render_backend->scene_height,
            .width = (float)render_backend->scene_width,
            .height = -(float)render_backend->scene_height, // negative sign flips the y axis to respect opengl convention
            .minDepth = 0.f,
            .maxDepth = 1.f,
        };

        vkCmdSetViewport(cmd_buff, 0, 1, &viewport);

        const VkClearValue clear_colors[] = {
            { .color = { { 0.f, 0.f, 0.f, 1.f } } },
            { .depthStencil = { 1.f, 0 } }
        };

        const VkRenderPassBeginInfo render_pass_begin = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .renderPass = render_backend->scene_renderpass,
            .framebuffer = render_backend->scene_framebuffer,
            .renderArea.offset = { 0, 0 },
            .renderArea.extent = { render_backend->scene_width, render_backend->scene_height },
            .clearValueCount = FA_ARRAY_COUNT(clear_colors),
            .pClearValues = clear_colors,
        };

        vkCmdBeginRenderPass(cmd_buff, &render_pass_begin, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindDescriptorSets(cmd_buff, VK_PIPELINE_BIND_POINT_GRAPHICS, render_backend->pipeline_layout, 0, 1, &render_backend->device.bindless_setup.set, 0, NULL);

        // Draw all the meshes
        {
            vkCmdBindPipeline(cmd_buff, VK_PIPELINE_BIND_POINT_GRAPHICS, render_backend->pipeline);

            const VkBuffer vertex_buffer[] = { gpu_scene->global_vertex_buffer[image_index].buffer };
            const VkDeviceSize offset[] = { 0 };

            vkCmdBindVertexBuffers(cmd_buff, 0, 1, vertex_buffer, offset);
            vkCmdBindIndexBuffer(cmd_buff, gpu_scene->global_index_buffer[image_index].buffer, 0, VK_INDEX_TYPE_UINT32);

            vkCmdDrawIndexedIndirect(cmd_buff, gpu_scene->command_draw_data_buffer[image_index].buffer, 0, gpu_scene->draw_count, sizeof(VkDrawIndexedIndirectCommand));
        }

        // Draw the editor grid
        {
            vkCmdBindPipeline(cmd_buff, VK_PIPELINE_BIND_POINT_GRAPHICS, render_backend->editor_grid_pipeline.pipeline);

            vkCmdDraw(cmd_buff, 6, 1, 0, 0);
        }

        vkCmdEndRenderPass(cmd_buff);

        VK_CHECK_RESULT(vkEndCommandBuffer(cmd_buff));

        FA_PROFILER_ZONE_END(ctx1);
    }

    update_ui(image_index, ig_draw_data);

    VkPipelineStageFlags wait_stages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    VkSemaphore wait_semaphores[] = { render_backend->swapchain.image_available[render_backend->current_frame_index] };
    VkSemaphore signal_semaphores[] = { render_backend->swapchain.render_finished[render_backend->current_frame_index] };

    VkCommandBuffer command_buffers_to_submit[] = {
        render_backend->command_buffers[image_index],
        render_backend->ui_command_buffers[image_index]
    };

    const VkSubmitInfo submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = FA_ARRAY_COUNT(wait_semaphores),
        .pWaitSemaphores = wait_semaphores,
        .pWaitDstStageMask = wait_stages,
        .commandBufferCount = FA_ARRAY_COUNT(command_buffers_to_submit),
        .pCommandBuffers = command_buffers_to_submit,
        .signalSemaphoreCount = FA_ARRAY_COUNT(signal_semaphores),
        .pSignalSemaphores = signal_semaphores
    };

    vkResetFences(render_backend->device.device, 1, &render_backend->in_flight_fences[render_backend->current_frame_index]);

    {
        FA_PROFILER_ZONE_N(ctx1, "queue submit", true);
        VK_CHECK_RESULT(vkQueueSubmit(render_backend->device.graphics_queue, 1, &submit_info, render_backend->in_flight_fences[render_backend->current_frame_index]));
        FA_PROFILER_ZONE_END(ctx1);
    }

    VkSwapchainKHR swapchains[] = { render_backend->swapchain.swapchain };
    const VkPresentInfoKHR present_info = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = FA_ARRAY_COUNT(signal_semaphores),
        .pWaitSemaphores = signal_semaphores,
        .swapchainCount = FA_ARRAY_COUNT(swapchains),
        .pSwapchains = swapchains,
        .pImageIndices = &image_index,
        .pResults = NULL,
    };

    {
        FA_PROFILER_ZONE_N(ctx1, "present", true);
        result = vkQueuePresentKHR(render_backend->device.present_queue, &present_info);
        FA_PROFILER_ZONE_END(ctx1);
    }

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || render_backend->framebuffer_resized)
    {
        recreate_swapchain();
        FA_PROFILER_ZONE_END(ctx);
        return;
    }
    checkf(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR, "Failed to acquire swapchain image!");

    render_backend->current_frame_index = (render_backend->current_frame_index + 1) % MAX_FRAMES_IN_FLIGHT;
    render_backend->current_image_index = (render_backend->current_image_index + 1) % render_backend->swapchain.image_count;

    FA_PROFILER_ZONE_END(ctx);
}

static fa_vulkan_buffer_t buffer_alloc(size_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, const char* name)
{
    fa_vulkan_buffer_t buffer = { 0 };
    buffer.properties = properties;
    buffer.usage = usage;
    buffer.size = size;
    buffer.debug_name = name;

    // allow creating 0 size buffers which just fills the buffer data to be used with a later realloc
    if (size != 0)
    {
        create_buffer(size, usage, properties, &buffer.buffer, &buffer.memory);
        if (name)
        {
            set_object_name((uint64_t)buffer.buffer, VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT, name);
        }
    }
    return buffer;
}

static void buffer_free(fa_vulkan_buffer_t buffer)
{
    vkDestroyBuffer(render_backend->device.device, buffer.buffer, NULL);
    vkFreeMemory(render_backend->device.device, buffer.memory, NULL);
}

static void buffer_realloc(fa_vulkan_buffer_t* buffer, size_t size)
{
    check(buffer);

    const VkBuffer old_buffer = buffer->buffer;
    const VkDeviceMemory old_memory = buffer->memory;
    const size_t old_size = buffer->size;

    buffer->size = size;
    create_buffer(size, buffer->usage, buffer->properties, &buffer->buffer, &buffer->memory);

    if (buffer->debug_name)
    {
        set_object_name((uint64_t)buffer->buffer, VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT, buffer->debug_name);
    }

    if (old_buffer != VK_NULL_HANDLE)
    {
        // #todo: copy old buffer to new buffer
        (void)old_size;
        // copy_buffer(old_buffer, buffer->buffer, 0, 0, old_size);
        vkDestroyBuffer(render_backend->device.device, old_buffer, NULL);
        vkFreeMemory(render_backend->device.device, old_memory, NULL);
    }
}

static void batch_buffer_set_data(fa_vk_buffer_write_desc_t* writes)
{
    FA_PROFILER_ZONE(ctx, true);
    FA_CREATE_TEMP_ALLOC(ta);

    const uint32_t num_writes = (uint32_t)fa_sbuff_num(writes);
    if (num_writes == 0)
    {
        return;
    }

    VkDeviceSize staging_buffer_size = 0;
    VkDeviceSize* offsets_in_staging_buffer = fa_temp_alloc(ta, num_writes * sizeof(VkDeviceSize));
    for (uint32_t i = 0; i < num_writes; ++i)
    {
        if (writes[i].buffer.properties & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            offsets_in_staging_buffer[i] = staging_buffer_size;
            staging_buffer_size += writes[i].size;
        }
    }

    // copy all the individual buffer data to the host-coherent mapped memory of the staging buffer
    VkBuffer staging_buf;
    VkDeviceMemory staging_mem;
    create_buffer(staging_buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &staging_buf, &staging_mem);
    void* staging_data;
    vkMapMemory(render_backend->device.device, staging_mem, 0, staging_buffer_size, 0, &staging_data);
    for (uint32_t i = 0; i < num_writes; ++i)
    {
        if (writes[i].buffer.properties & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            memcpy((char*)staging_data + offsets_in_staging_buffer[i], writes[i].data, writes[i].size);
        }
        else if (writes[i].buffer.properties & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
        {
            copy_data_to_buffer(writes[i].buffer.memory, writes[i].data, writes[i].dst_offset, writes[i].size);
        }
    }
    vkUnmapMemory(render_backend->device.device, staging_mem);

    VkCommandBuffer* cmd_buffers = fa_temp_alloc(ta, sizeof(VkCommandBuffer) * num_writes);
    {
        FA_PROFILER_ZONE_N(ctx1, "batch_upload_buffer_data", true);
        // create an individual command buffer for each copy operation
        const VkCommandBufferAllocateInfo alloc_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandPool = render_backend->transient_command_pool,
            .commandBufferCount = num_writes,
        };

        vkAllocateCommandBuffers(render_backend->device.device, &alloc_info, cmd_buffers);

        for (uint64_t i = 0; i < num_writes; ++i)
        {
            if (writes[i].buffer.properties & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
            {
                const VkCommandBufferBeginInfo begin_info = {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                    .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                };

                vkBeginCommandBuffer(cmd_buffers[i], &begin_info);
                const VkBufferCopy copy_region = {
                    .srcOffset = offsets_in_staging_buffer[i],
                    .dstOffset = writes[i].dst_offset,
                    .size = writes[i].size,
                };

                vkCmdCopyBuffer(cmd_buffers[i], staging_buf, writes[i].buffer.buffer, 1, &copy_region);

                vkEndCommandBuffer(cmd_buffers[i]);
            }
        }

        const VkFenceCreateInfo fence_create_info = {
            .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            .flags = 0,
            .pNext = NULL
        };

        VkFence finished_fence = VK_NULL_HANDLE;
        VK_CHECK_RESULT(vkCreateFence(render_backend->device.device, &fence_create_info, NULL, &finished_fence));

        const VkSubmitInfo submit_info = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = num_writes,
            .pCommandBuffers = cmd_buffers,
        };

        vkQueueSubmit(render_backend->device.graphics_queue, 1, &submit_info, finished_fence);
        vkWaitForFences(render_backend->device.device, 1, &finished_fence, VK_TRUE, UINT64_MAX);

        vkDestroyFence(render_backend->device.device, finished_fence, NULL);
        FA_PROFILER_ZONE_END(ctx1);
    }

    vkFreeCommandBuffers(render_backend->device.device, render_backend->transient_command_pool, num_writes, cmd_buffers);

    vkDestroyBuffer(render_backend->device.device, staging_buf, NULL);
    vkFreeMemory(render_backend->device.device, staging_mem, NULL);

    FA_DESTROY_TEMP_ALLOC(ta);
    FA_PROFILER_ZONE_END(ctx);
}

static void buffer_set_data(fa_vulkan_buffer_t buffer, void* data, size_t offset, size_t size)
{
    check(size <= buffer.size);
    // if the buffer is device local we need to create a staging buffer to upload the data
    if (buffer.properties & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
    {
        FA_PROFILER_ZONE_N(ctx, "vk staging buffer upload", true);

        VkBuffer staging_buf;
        VkDeviceMemory staging_mem;

        create_buffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &staging_buf, &staging_mem);
        copy_data_to_buffer(staging_mem, data, offset, size);
        copy_buffer(staging_buf, buffer.buffer, 0, offset, size);

        vkDestroyBuffer(render_backend->device.device, staging_buf, NULL);
        vkFreeMemory(render_backend->device.device, staging_mem, NULL);

        FA_PROFILER_ZONE_END(ctx);
    }
    else if (buffer.properties & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
    {
        FA_PROFILER_ZONE_N(ctx, "vk buffer upload", true);

        void* buff;
        vkMapMemory(render_backend->device.device, buffer.memory, offset, size, 0, &buff);
        memcpy(buff, data, size);
        vkUnmapMemory(render_backend->device.device, buffer.memory);

        FA_PROFILER_ZONE_END(ctx);
    }
}

static fa_vulkan_texture_t texture_create(uint32_t width, uint32_t height, void* pixel_data)
{
    FA_PROFILER_ZONE(ctx, true);
    checkf(width != 0 && height != 0 && pixel_data != NULL, "Invalid texture data!");

    const VkDeviceSize image_size = width * height * 4;

    fa_vulkan_texture_t result;

    VkBuffer staging_buf;
    VkDeviceMemory staging_mem;

    create_buffer(image_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &staging_buf, &staging_mem);

    copy_data_to_buffer(staging_mem, pixel_data, 0, image_size);

    create_image(width, height, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &result.image, &result.memory);

    // #todo [roey]: should probably merge this into a single command buffer
    transition_image_layout(result.image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copy_buffer_to_image(staging_buf, result.image, width, height);
    transition_image_layout(result.image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    vkDestroyBuffer(render_backend->device.device, staging_buf, NULL);
    vkFreeMemory(render_backend->device.device, staging_mem, NULL);

    result.view = create_image_view(result.image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT);
    result.binding_slot = ++render_backend->device.bindless_setup.next_texture_slot;

    // Write the texture to the descriptor
    {
        const VkDescriptorImageInfo image_info = {
            .imageView = result.view,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        };

        const VkWriteDescriptorSet texture_write = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
            .dstBinding = 1,
            .dstSet = render_backend->device.bindless_setup.set,
            .dstArrayElement = result.binding_slot,
            .pImageInfo = &image_info,
        };

        vkUpdateDescriptorSets(render_backend->device.device, 1, &texture_write, 0, NULL);
    }

    FA_PROFILER_ZONE_END(ctx);
    return result;
}

static void texture_destroy(fa_vulkan_texture_t texture)
{
    vkDestroyImageView(render_backend->device.device, texture.view, NULL);
    vkDestroyImage(render_backend->device.device, texture.image, NULL);
    vkFreeMemory(render_backend->device.device, texture.memory, NULL);
}

#pragma endregion

static struct fa_vulkan_backend_api vulkan_backend_api = {
    .create = create_vk_renderer,
    .init_ui = init_ui,
    .destroy_ui = destroy_ui,
    .destroy = destroy_vk,
    .begin_frame = begin_frame,
    .submit_scene = submit_scene,
    .buffer_alloc = buffer_alloc,
    .buffer_free = buffer_free,
    .buffer_realloc = buffer_realloc,
    .buffer_set_data = buffer_set_data,
    .batch_buffer_set_data = batch_buffer_set_data,
    .texture_create = texture_create,
    .texture_destroy = texture_destroy,
    .descriptor_get_bindless_set = descriptor_get_bindless_set,
    .descriptor_write = descriptor_write,
    .get_scene_color_descriptor = get_scene_color_descriptor,
    .resize_scene_color_framebuffer = resize_scene_color_framebuffer,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_allocator_api = registry->get(FA_ALLOCATOR_API_NAME);
    fa_os_api = registry->get(FA_OS_API_NAME);
    fa_window_api = registry->get(FA_WINDOW_API_NAME);

    fa_add_or_remove_api(&vulkan_backend_api, FA_VULKAN_BACKEND_API_NAME, load);

    render_backend = (fa_render_backend_t*)registry->static_variable(FA_VULKAN_BACKEND_API_NAME, "vulkan_backend", sizeof(fa_render_backend_t));
}
