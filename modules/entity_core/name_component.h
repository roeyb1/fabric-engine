#pragma once

#include <core/core_types.h>

typedef struct fa_name_component_t
{
    fa_string_t name;
} fa_name_component_t;

#define FA_NAME_COMPONENT_NAME "fa_name_component"
