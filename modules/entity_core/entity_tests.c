#include "entity_api.h"
#include <core/stretchy_buffers.inl>
#include <core/unit_tests.h>

typedef struct fa_position_component_t
{
    vec3s pos;
} fa_position_component_t;
#define FA_POSITION_COMPONENT_NAME "position_component"

const static fa_position_component_t default_position = { 1, 1, 1 };

static struct fa_unit_test_api* fa_unit_test_api;

static fa_component_description_t position_component_description = {
    .name = FA_POSITION_COMPONENT_NAME,
    .prototype_data = &default_position,
    .bytes = sizeof(fa_position_component_t),
};

static void run_component_tests(void)
{
    const fa_component_type_t type_id = fa_entity_api->component_type_register(&position_component_description);

    const fa_component_type_t type_id2 = fa_entity_api->component_type_get(FA_POSITION_COMPONENT_NAME);

    FA_TEST_EXPECT(type_id.id != FA_INVALID_COMPONENT.id);
    FA_TEST_EXPECT(type_id2.id == type_id.id);
}

static void run_entity_tests(void)
{
    const fa_component_type_t type_id = fa_entity_api->component_type_register(&position_component_description);

    fa_entity_context_t* ctx = fa_entity_api->context_create("test context");

    // test basic entity creation and single component addition
    {
        FA_TEST_EXPECT(ctx != NULL);

        const fa_entity_t entity = fa_entity_api->entity_create_nameless(ctx);

        FA_TEST_EXPECT(entity.id != FA_INVALID_ENTITY.id);

        const fa_position_component_t* component_ptr = fa_entity_api->component_add(ctx, entity, type_id);

        FA_TEST_EXPECT(component_ptr != NULL);
        FA_TEST_EXPECT(component_ptr->pos.x == 1.f);
        FA_TEST_EXPECT(component_ptr->pos.y == 1.f);
        FA_TEST_EXPECT(component_ptr->pos.z == 1.f);

        const fa_position_component_t* retrieved_ptr = fa_entity_api->component_get(ctx, entity, type_id);

        FA_TEST_EXPECT(retrieved_ptr != NULL);

        FA_TEST_EXPECT(component_ptr == retrieved_ptr);
    }

    /* #todo: needs more testing
     * - Adding the same type of component to the same entity
     * - multiple components of different types to an entity
     * - no explicit default value for a component
     * - creating many entities
     * - creating many entities with components
     * - removing entities
     */

    fa_entity_api->context_destroy(ctx);
}

static void run_entity_filter_tests(void)
{
    const fa_component_type_t position_type_id = fa_entity_api->component_type_register(&position_component_description);

    fa_entity_context_t* ctx = fa_entity_api->context_create("test context");

    {
        FA_TEST_EXPECT(ctx != NULL);

        const fa_entity_t entity = fa_entity_api->entity_create_nameless(ctx);
        const fa_entity_t entity2 = fa_entity_api->entity_create_nameless(ctx);
        (void)entity2;

        FA_TEST_EXPECT(entity.id != FA_INVALID_ENTITY.id);

        const fa_position_component_t* component_ptr = fa_entity_api->component_add(ctx, entity, position_type_id);
        FA_TEST_EXPECT(component_ptr != NULL);

        const fa_entity_filter_t filter = fa_entity_api->entity_filter_create(ctx, (fa_entity_filter_desc_t){ .types = { FA_POSITION_COMPONENT_NAME } });

        const uint64_t num_entities_in_filter = fa_sbuff_num(filter.entities);
        FA_TEST_EXPECT(num_entities_in_filter == 1);
        FA_TEST_EXPECT(filter.entities[0].id == entity.id);

        FA_TEST_EXPECT(filter.type_count == 1);
        FA_TEST_EXPECT(filter.types[0].id == position_type_id.id);

        const fa_entity_filter_t filter_all = fa_entity_api->entity_filter_create(ctx, (fa_entity_filter_desc_t){ 0 });
        FA_TEST_EXPECT(fa_sbuff_num(filter_all.entities) == 2);
        FA_TEST_EXPECT(filter_all.type_count == 0);
    }

    fa_entity_api->context_destroy(ctx);
}

static void test_system_update(fa_entity_filter_t* filter)
{
    static int count = 0;
    if (count++ % 2 == 0)
    {
        FA_TEST_EXPECT(fa_sbuff_num(filter->entities) == 2);
    }
    else
    {
        FA_TEST_EXPECT(fa_sbuff_num(filter->entities) == 3);
    }
}

static void run_system_tests(void)
{
    const fa_component_type_t position_type_id = fa_entity_api->component_type_register(&position_component_description);
    const fa_entity_filter_desc_t position_type_filter_desc = { .types = { FA_POSITION_COMPONENT_NAME } };

    fa_entity_context_t* ctx = fa_entity_api->context_create("test context");

    const fa_entity_t entity = fa_entity_api->entity_create_nameless(ctx);
    const fa_entity_t entity2 = fa_entity_api->entity_create_nameless(ctx);

    const fa_position_component_t* component_ptr = fa_entity_api->component_add(ctx, entity, position_type_id);
    const fa_position_component_t* component_ptr_2 = fa_entity_api->component_add(ctx, entity2, position_type_id);
    FA_TEST_EXPECT(component_ptr != component_ptr_2);

    const fa_system_description_t test_system_description = { .filter_desc = position_type_filter_desc, .update = test_system_update };

    fa_entity_api->system_create(&test_system_description);

    fa_entity_api->run_all_systems(ctx);

    const fa_entity_t entity3 = fa_entity_api->entity_create_nameless(ctx);
    const fa_position_component_t* component_ptr_3 = fa_entity_api->component_add(ctx, entity3, position_type_id);
    (void)component_ptr_3;

    fa_entity_api->run_all_systems(ctx);

    fa_entity_api->context_destroy(ctx);
}

void register_entity_tests(struct fa_unit_test_api* unit_test_api, bool add)
{
    fa_unit_test_api = unit_test_api;
    fa_add_or_remove_test(unit_test_api, "entity", "component", run_component_tests, add);
    fa_add_or_remove_test(unit_test_api, "entity", "entity", run_entity_tests, add);
    fa_add_or_remove_test(unit_test_api, "entity", "system", run_system_tests, add);
    fa_add_or_remove_test(unit_test_api, "entity", "filter", run_entity_filter_tests, add);
}