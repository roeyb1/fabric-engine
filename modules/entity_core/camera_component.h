#pragma once

#include <core/core_defines.h>
#include <core/core_types.h>

typedef struct fa_camera_component_t
{
    // cached matrices
    mat4s view;
    mat4s proj;

    // camera origin
    vec3s pos;

    // camera basis vectors
    vec3s forward;
    vec3s up;
    vec3s right;

    float near_plane;
    float far_plane;

    float yaw;
    float pitch;
    float aspect;
} fa_camera_component_t;

#define FA_CAMERA_COMPONENT_NAME "fa_camera_component"

static inline void fa_recompute_camera_matrices(fa_camera_component_t* camera)
{
    camera->pitch = glm_clamp(camera->pitch, -89.f, 89.f);

    camera->forward.x = cosf(glm_rad(camera->yaw)) * cosf(glm_rad(camera->pitch));
    camera->forward.y = sinf(glm_rad(camera->pitch));
    camera->forward.z = sinf(glm_rad(camera->yaw)) * cosf(glm_rad(camera->pitch));
    camera->forward = glms_normalize(camera->forward);

    camera->right = (glms_normalize(glms_cross(camera->forward, WORLD_UP_VECTOR)));
    camera->up = glms_normalize(glms_cross(camera->right, camera->forward));

    camera->view = glms_look(camera->pos, camera->forward, camera->up);
    camera->proj = glms_perspective(glm_rad(45.f), camera->aspect, camera->near_plane, camera->far_plane);
}
