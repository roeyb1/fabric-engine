#pragma once

#include <core/core_types.h>

typedef struct fa_selected_component_t
{
    FA_PAD(1);
} fa_selected_component_t;

#define FA_SELECTED_COMPONENT_NAME "fa_selected_component"
