#pragma once

#include <core/core_types.h>

typedef struct fa_transform_component_t
{
    mat4s mat;

    vec3s pos;
    vec3s rot;
    vec3s scale;
} fa_transform_component_t;

static inline void fa_recompute_transform_matrix(fa_transform_component_t* transform_component)
{
    transform_component->mat = GLMS_MAT4_IDENTITY;
    transform_component->mat = glms_translate(transform_component->mat, transform_component->pos);
    transform_component->mat = glms_scale(transform_component->mat, transform_component->scale);
    transform_component->mat = glms_rotate(transform_component->mat, glm_rad(transform_component->rot.x), (vec3s){ 1.f, 0.f, 0.f });
    transform_component->mat = glms_rotate(transform_component->mat, glm_rad(transform_component->rot.y), (vec3s){ 0.f, 1.f, 0.f });
    transform_component->mat = glms_rotate(transform_component->mat, glm_rad(transform_component->rot.z), (vec3s){ 0.f, 0.f, 1.f });
}

#define FA_TRANSFORM_COMPONENT_NAME "fa_transform_component"