#pragma once

#include <core/core_types.h>

/**
 * Entity handle used to address a specific, unique entity within a single entity context.
 *
 * #todo: use generational IDs to allow reusing entity id numbers.
 */
typedef struct fa_entity_t
{
    uint64_t id;
} fa_entity_t;

#define FA_INVALID_ENTITY \
    ((fa_entity_t){ 0 })
