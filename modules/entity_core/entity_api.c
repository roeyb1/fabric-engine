#include "entity_api.h"
#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/array.h>
#include <core/atomic.h>
#include <core/core_defines.h>
#include <core/hash.inl>
#include <core/log.h>
#include <core/string_hash.inl>
#include <core/unit_tests.h>

#include "camera_component.h"
#include "name_component.h"
#include "selected_component.h"
#include "transform_component.h"

#include <string.h>
// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_logger_api* fa_logger_api;
struct fa_allocator_api* fa_allocator_api;
struct fa_string_api* fa_string_api;

// ------------------------------------------------------------------------------------------------------------------------------------------

/** The maximum number of different component types supported by the engine. */
#define MAX_COMPONENT_TYPES 1024

/**
 * Stores all the type information for every registered component type.
 */
struct fa_component_type_registry
{
    fa_component_description_t types[MAX_COMPONENT_TYPES];
    atomic_uint_least64_t next_free_type_index;

    // Maps hashes of component names to
    struct FA_HASH_T(fa_string_hash_t, uint64_t) component_name_map;
};

/**
 * Internal representation of the entity. Includes the handle as well as a list of components
 * which that entity has. Used for quicker searching than checking every component container.
 */
typedef struct
{
    fa_entity_t handle;

    /* sbuff */ fa_component_type_t* components;
} entity_t;

/**
 * Stores all the components of a specific component type for a given context.
 *
 * #todo: implement this using an archetype based approach. All component data should be stored within the archetype.
 **/
typedef struct
{
    /** maps entity ids to pointers into the component data array */
    struct FA_HASH_T(fa_entity_t, void*) component_map;
    struct FA_HASH_T(void*, fa_entity_t) reverse_component_map;

    fa_component_description_t* desc;

    /** Data buffer storing all the data for all the components data. */
    char* data;

    /** the number of components currently allocated in the buffer */
    uint64_t size;
    /** the total number of components which can fit in the current buffer */
    uint64_t capacity;

    /** Set to true when the container has been initialized */
    bool valid;
} fa_component_container_t;

/** Acts as the "world" object for the entity system. */
typedef struct fa_entity_context_t
{
    fa_string_t name;

    fa_entity_t* entities;
    atomic_uint_least64_t next_entity_id;

    fa_component_container_t component_containers[MAX_COMPONENT_TYPES];

} fa_entity_context_t;

/** Just a container for all the known entity contexts */
struct fa_entity_context_registry
{
    // Ideally this would use a chunked array to maintain pointer stability
    /* sbuff */ fa_entity_context_t** contexts;
};

struct fa_system_registry
{
    /* sbuff */ fa_system_description_t* systems;
};

static struct fa_component_type_registry* component_type_registry;
static struct fa_entity_context_registry* context_registry;
static struct fa_system_registry* system_registry;

// ------------------------------------------------------------------------------------------------------------------------------------------

static fa_entity_context_t* context_create(const char* name)
{
    // #todo: this method is not threadsafe. Sbuff pushes are not threadsafe
    fa_entity_context_t* new_ctx = fa_malloc(sizeof(fa_entity_context_t));
    memset(new_ctx, 0, sizeof(*new_ctx));

    fa_sbuff_push(context_registry->contexts, new_ctx);
    new_ctx->name = fa_string_api->create_from_c_str(name);

    return new_ctx;
}

static void context_destroy(fa_entity_context_t* ctx)
{
    fa_sbuff_free(ctx->entities);
    memset(ctx, 0, sizeof(*ctx));

    for (uint32_t i = 0; i < fa_sbuff_num(context_registry->contexts); ++i)
    {
        if (context_registry->contexts[i] == ctx)
        {
            context_registry->contexts[i] = NULL;
        }
    }

    fa_free(ctx);
    // #todo: remove the context from the main context buffer.
    // for now this is fine but it will continuously grow
}

static fa_entity_t entity_create_nameless(fa_entity_context_t* ctx)
{
    const uint64_t entity_id = atomic_fetch_add_uint64_t(&ctx->next_entity_id, 1) + 1;
    const fa_entity_t entity = { entity_id };
    fa_sbuff_push(ctx->entities, entity);

    return entity;
}

static fa_entity_t entity_create(fa_entity_context_t* ctx, const char* name)
{
    const fa_entity_t entity = entity_create_nameless(ctx);
    fa_name_component_t* name_component = fa_entity_api->component_add(ctx, entity, fa_entity_api->component_type_get(FA_NAME_COMPONENT_NAME));
    name_component->name = fa_string_api->create_from_c_str(name);

    return entity;
}

static void entity_destroy(fa_entity_context_t* ctx, fa_entity_t entity)
{
    for (uint32_t i = 0; i < fa_sbuff_num(ctx->entities); ++i)
    {
        if (ctx->entities[i].id == entity.id)
        {
            ctx->entities[i] = FA_INVALID_ENTITY;
        }
    }
}

static fa_component_type_t component_type_get(const char* name)
{
    const fa_string_hash_t name_hash = fa_string_hash(name);

    if (fa_hash_has(&component_type_registry->component_name_map, name_hash))
    {
        return (fa_component_type_t){ fa_hash_get(&component_type_registry->component_name_map, name_hash) };
    }
    return FA_INVALID_COMPONENT;
}

static fa_component_type_t component_type_register(fa_component_description_t* description)
{
    const fa_component_type_t existing_component_type = component_type_get(description->name);
    if (existing_component_type.id != 0)
    {
        component_type_registry->types[existing_component_type.id].prototype_data = description->prototype_data;
        return existing_component_type;
    }

    const uint64_t component_type_index = atomic_fetch_add_uint64_t(&component_type_registry->next_free_type_index, 1) + 1;
    check(component_type_index < MAX_COMPONENT_TYPES);

    memcpy(component_type_registry->types + component_type_index, description, sizeof(*description));
    const fa_string_hash_t name_hash = fa_string_hash(description->name);

    fa_hash_add(&component_type_registry->component_name_map, name_hash, component_type_index);

    return (fa_component_type_t){ component_type_index };
}

static fa_component_description_t const** component_type_list(void)
{
    fa_component_description_t const** component_types = NULL;
    const uint64_t last_index = component_type_registry->next_free_type_index;
    for (uint64_t i = 0; i < last_index; ++i)
    {
        if (component_type_registry->types[i].name != NULL)
        {
            fa_sbuff_push(component_types, component_type_registry->types + i);
        }
    }
    return component_types;
}

static void component_type_reset(void)
{
    component_type_registry->next_free_type_index = 0;
    fa_hash_free(&component_type_registry->component_name_map);
}

static inline fa_component_container_t* get_or_create_component_container_internal(fa_entity_context_t* ctx, fa_component_type_t type)
{
    if (ctx->component_containers[type.id].valid == false)
    {
        memset(ctx->component_containers + type.id, 0, sizeof(fa_component_container_t));
        ctx->component_containers[type.id].desc = component_type_registry->types + type.id;
        ctx->component_containers[type.id].valid = true;
    }
    return ctx->component_containers + type.id;
}

static inline void* get_component_internal(fa_component_container_t* container, fa_entity_t entity)
{
    if (fa_hash_has(&container->component_map, entity))
    {
        return fa_hash_get(&container->component_map, entity);
    }
    return NULL;
}

static void* component_get(fa_entity_context_t* ctx, fa_entity_t entity, fa_component_type_t component_type)
{
    fa_component_container_t* component_container = get_or_create_component_container_internal(ctx, component_type);
    return get_component_internal(component_container, entity);
}

static void* component_add(fa_entity_context_t* ctx, fa_entity_t entity, fa_component_type_t component_type)
{
    // #warning: not threadsafe

    fa_component_container_t* component_container = get_or_create_component_container_internal(ctx, component_type);

    // if the entity already has the component, just return the pointer to it. We don't support
    // multiple instances of the same component per-entity
    void* data = get_component_internal(component_container, entity);
    if (data != NULL)
        return data;

    const fa_component_description_t* desc = component_container->desc;

    // if the current buffer can't fit another component
    if (component_container->capacity - component_container->size == 0)
    {
        // we need to realloc the buffer
        const uint64_t new_capacity = component_container->capacity ? 2 * component_container->capacity : 16;
        const uint64_t new_bytes = new_capacity * desc->bytes;
        component_container->data = fa_realloc(component_container->data, new_bytes);
        component_container->capacity = new_capacity;
    }
    check(component_container->capacity - component_container->size > 0);

    // note: this assumes a perfectly packed data array
    const uint64_t offset = component_container->size * desc->bytes;
    data = component_container->data + offset;
    component_container->size++;

    // initialize the new component
    if (desc->prototype_data)
    {
        memcpy(data, desc->prototype_data, desc->bytes);
    }
    else
    {
        memset(data, 0, desc->bytes);
    }

    fa_hash_add(&component_container->component_map, entity, data);
    fa_hash_add(&component_container->reverse_component_map, data, entity);

    if (component_container->desc->on_component_added != NULL)
    {
        component_container->desc->on_component_added(data, entity);
    }

    return data;
}

static void component_remove(fa_entity_context_t* ctx, fa_entity_t entity, fa_component_type_t component_type)
{
    fa_component_container_t* component_container = get_or_create_component_container_internal(ctx, component_type);
    if (!fa_hash_has(&component_container->component_map, entity))
    {
        return;
    }

    void* ptr = fa_hash_get(&component_container->component_map, entity);

    if (component_container->desc->on_component_removed != NULL)
    {
        component_container->desc->on_component_removed(ptr, entity);
    }

    if (component_container->size == 1)
    {
        component_container->size = 0;
        fa_hash_remove(&component_container->component_map, entity);
        fa_hash_remove(&component_container->reverse_component_map, ptr);
    }
    else
    {
        // remove is handled by swapping the removed component with the last component in the map.
        const uint64_t elem_to_move = component_container->size - 1;
        const uint64_t elem_offset = elem_to_move * component_container->desc->bytes;
        void* end_ptr = component_container->data + elem_offset;
        const fa_entity_t end_entity = fa_hash_get(&component_container->reverse_component_map, end_ptr);

        void* remove_ptr = fa_hash_get(&component_container->component_map, entity);

        memmove(remove_ptr, end_ptr, component_container->desc->bytes);
        // replace the entry in the map for the old last entity with the new location
        fa_hash_add(&component_container->component_map, end_entity, remove_ptr);
        fa_hash_add(&component_container->reverse_component_map, remove_ptr, end_entity);

        --component_container->size;
    }
}

static fa_entity_filter_t entity_filter_create(fa_entity_context_t* ctx, fa_entity_filter_desc_t desc)
{
    fa_entity_filter_t filter = { 0 };
    for (uint32_t i = 0; i < FA_ARRAY_COUNT(desc.types); ++i)
    {
        if (desc.types[i] == 0)
        {
            break;
        }
        filter.types[i] = component_type_get(desc.types[i]);
        checkf(filter.types[i].id != 0, "Attempting to create a filter for a component type which doesn't exist (%s)", desc.types[i]);
        ++filter.type_count;
    }
    for (uint32_t i = 0; i < FA_ARRAY_COUNT(desc.type_ids); ++i)
    {
        if (desc.type_ids[i].id == 0)
        {
            break;
        }
        filter.types[i] = desc.type_ids[i];
        checkf(filter.types[i].id != 0, "Attempting to create a filter for a component type which doesn't exist (%s)", desc.types[i]);
        ++filter.type_count;
    }

    for (uint32_t i = 0; i < fa_sbuff_num(ctx->entities); ++i)
    {
        if (ctx->entities[i].id == FA_INVALID_ENTITY.id)
        {
            continue;
        }

        bool has_all_types = true;
        for (uint32_t component_type_index = 0; component_type_index < filter.type_count; ++component_type_index)
        {
            if (component_get(ctx, ctx->entities[i], filter.types[component_type_index]) == NULL)
            {
                has_all_types = false;
            }
        }
        if (has_all_types)
        {
            fa_sbuff_push(filter.entities, ctx->entities[i]);
        }
    }

    return filter;
}

static void system_create(const fa_system_description_t* system_description)
{
    // #todo: better way to prevent duplicate systems being added?
    for (uint32_t i = 0; i < fa_sbuff_num(system_registry->systems); ++i)
    {
        if (system_registry->systems[i].update == system_description->update)
        {
            return;
        }
    }
    fa_sbuff_push(system_registry->systems, *system_description);
}

static void run_systems_for_entity_ctx(fa_entity_context_t* ctx)
{
    for (uint32_t system_index = 0; system_index < fa_sbuff_num(system_registry->systems); ++system_index)
    {
        const fa_system_description_t* system = system_registry->systems + system_index;

        fa_entity_filter_t filter = entity_filter_create(ctx, system->filter_desc);
        system->update(&filter);
    }
}

static void run_all_systems(fa_entity_context_t* specified_ctx)
{
    // #todo: go wide per-context?
    if (specified_ctx == NULL)
    {
        for (uint32_t context_index = 0; context_index < fa_sbuff_num(context_registry->contexts); ++context_index)
        {
            if (context_registry->contexts[context_index] != NULL)
            {
                fa_entity_context_t* ctx = context_registry->contexts[context_index];

                run_systems_for_entity_ctx(ctx);
            }
        }
    }
    else
    {
        run_systems_for_entity_ctx(specified_ctx);
    }
}

// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_entity_api* fa_entity_api = &(struct fa_entity_api){
    .context_create = context_create,
    .context_destroy = context_destroy,
    .entity_create = entity_create,
    .entity_create_nameless = entity_create_nameless,
    .component_type_register = component_type_register,
    .component_type_get = component_type_get,
    .component_type_list = component_type_list,
    .component_type_reset = component_type_reset,
    .component_add = component_add,
    .component_remove = component_remove,
    .component_get = component_get,
    .entity_filter_create = entity_filter_create,
    .system_create = system_create,
    .run_all_systems = run_all_systems
};

extern void register_entity_tests(struct fa_unit_test_api* unit_test_api, bool add);

static void register_builtin_component_types(void)
{
    // #todo: maybe there's a better place to register component types
    {
        static fa_transform_component_t default_transform = {
            .mat = GLMS_MAT4_IDENTITY_INIT,
            .pos = GLMS_VEC3_ZERO_INIT,
            .rot = GLMS_VEC3_ZERO_INIT,
            .scale = GLMS_VEC3_ONE_INIT,
        };

        fa_component_description_t transform_desc = {
            .name = FA_TRANSFORM_COMPONENT_NAME,
            .display_name = "Transform",
            .bytes = sizeof(fa_transform_component_t),
            .prototype_data = &default_transform,
        };
        fa_entity_api->component_type_register(&transform_desc);
    }

    {
        static fa_camera_component_t default_camera = {
            .view = GLMS_MAT4_IDENTITY_INIT,
            .proj = GLMS_MAT4_IDENTITY_INIT,
        };

        fa_component_description_t camera_desc = {
            .name = FA_CAMERA_COMPONENT_NAME,
            .display_name = "Camera",
            .bytes = sizeof(fa_camera_component_t),
            .prototype_data = &default_camera,
        };
        fa_entity_api->component_type_register(&camera_desc);
    }

    fa_component_description_t name_desc = {
        .name = FA_NAME_COMPONENT_NAME,
        .display_name = NULL,
        .bytes = sizeof(fa_name_component_t),
        .prototype_data = NULL,
    };
    fa_entity_api->component_type_register(&name_desc);

    fa_component_description_t selected_desc = {
        .name = FA_SELECTED_COMPONENT_NAME,
        .display_name = NULL,
        .bytes = sizeof(fa_selected_component_t),
        .prototype_data = NULL,
    };
    fa_entity_api->component_type_register(&selected_desc);
}

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_allocator_api = fa_get_api(FA_ALLOCATOR_API_NAME);
    fa_logger_api = fa_get_api(FA_LOGGER_API_NAME);
    fa_string_api = fa_get_api(FA_STRING_API_NAME);
    fa_add_or_remove_api(fa_entity_api, FA_ENTITY_API_NAME, load);

    register_entity_tests(fa_get_api(FA_UNIT_TEST_API_NAME), load);

    component_type_registry = registry->static_variable(FA_ENTITY_API_NAME, "component_type_registry", sizeof(*component_type_registry));
    context_registry = registry->static_variable(FA_ENTITY_API_NAME, "context_registry", sizeof(*context_registry));
    system_registry = registry->static_variable(FA_ENTITY_API_NAME, "system_registry", sizeof(*system_registry));

    register_builtin_component_types();
}
