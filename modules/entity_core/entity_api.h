#pragma once

#include <core/core_types.h>
#include <modules/entity_core/entity_types.h>

/**
 * Component type handle maps to a specific component_description struct for that component.
 * Used to uniquely identify a specific component type and can be queried by name.
 */
typedef struct fa_component_type_t
{
    uint64_t id;
} fa_component_type_t;

#define FA_INVALID_COMPONENT \
    ((fa_component_type_t){ 0 })

/**
 * Implementation of an entity component.
 */
typedef struct fa_component_description_t
{
    /** The unique name of the component type. */
    const char* name;

    /** A shorter, human-readable name. */
    const char* display_name;

    /**
     * Pointer to a static instance of the component type which can be used to default-init future components.
     * The "prototype" of a component defines the default values for all instances of the component.
     * If any delta-serialization is required it will be based on this instance.
     *
     * If prototype_data is null, the components will be memset to 0 on creation.
     */
    const void* prototype_data;

    /** The size of a single instance of the component POD struct */
    uint32_t bytes;

    // #todo: function pointers for things like registering to change callbacks, property panel draws, debug draws, etc.
    // can be added to the end of this description struct.

    void (*on_component_added)(void* this, fa_entity_t owner);

    void (*on_component_removed)(void* this, fa_entity_t owner);
} fa_component_description_t;

/** The entity context is where all entities and components live */
typedef struct fa_entity_context_t fa_entity_context_t;

#define MAX_COMPONENTS_PER_FILTER 16

typedef struct fa_entity_filter_desc_t
{
    const char* types[MAX_COMPONENTS_PER_FILTER];
    fa_component_type_t type_ids[MAX_COMPONENTS_PER_FILTER];
} fa_entity_filter_desc_t;

typedef struct fa_entity_filter_t
{
    fa_entity_t* entities;

    fa_component_type_t types[2 * MAX_COMPONENTS_PER_FILTER];
    uint32_t type_count;
} fa_entity_filter_t;

typedef struct fa_system_description_t
{
    fa_string_t name;
    fa_entity_filter_desc_t filter_desc;

    void (*update)(fa_entity_filter_t* filter);
} fa_system_description_t;

struct fa_entity_api
{
    /** Creates a new entity context. */
    fa_entity_context_t* (*context_create)(const char* name);

    /** Destroys a context. */
    void (*context_destroy)(fa_entity_context_t* ctx);

    /** Creates a new entity in the entity context. */
    fa_entity_t (*entity_create)(fa_entity_context_t* ctx, const char* name);

    /** Creates an nameless entity. */
    fa_entity_t (*entity_create_nameless)(fa_entity_context_t* ctx);

    /** Registers a component type with the ECS. Components are registered globally, not by entity context */
    fa_component_type_t (*component_type_register)(fa_component_description_t* desc);

    /** Returns the ID of a component type by name */
    fa_component_type_t (*component_type_get)(const char* name);

    /** Get an sbuff of all the component types currently registered */
    fa_component_description_t const** (*component_type_list)(void);

    /** Resets all known component types. Mostly useful for tests. */
    void (*component_type_reset)(void);

    /** Creates a component on a given entity */
    void* (*component_add)(fa_entity_context_t* ctx, fa_entity_t entity, fa_component_type_t component_type);

    /** Removes a component from an entity */
    void (*component_remove)(fa_entity_context_t* ctx, fa_entity_t entity, fa_component_type_t component_type);

    /** Retrieves a pointer to a component on a given entity */
    void* (*component_get)(fa_entity_context_t* ctx, fa_entity_t entity, fa_component_type_t component_type);

    fa_entity_filter_t (*entity_filter_create)(fa_entity_context_t* ctx, fa_entity_filter_desc_t desc);

    void (*system_create)(const fa_system_description_t* description);

    // #todo: implement a multi phase run.
    /** Executes the update function for all registered systems. If the `specified_ctx` param is null, it will run for all entity contexts */
    void (*run_all_systems)(fa_entity_context_t* specified_ctx);
};

#define FA_ENTITY_API_NAME "FA_ENTITY_API"

#if defined(FA_LINKS_TO_ENTITY_CORE)
extern struct fa_allocator_api* fa_allocator_api;
extern struct fa_entity_api* fa_entity_api;
extern struct fa_logger_api* fa_logger_api;
#endif // defined(FA_LINKS_TO_ENTITY_CORE)