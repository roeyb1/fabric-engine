#include <core/api_registry.h>
#include <core/core_defines.h>
#include <core/core_types.h>

typedef void load_function(struct fa_api_registry*, bool);

extern load_function load_properties, load_scene_graph, load_log, load_asset_browser;

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    load_properties(registry, load);
    load_scene_graph(registry, load);
    load_log(registry, load);
    load_asset_browser(registry, load);
}