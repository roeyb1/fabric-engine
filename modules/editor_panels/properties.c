#include "properties.h"

#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/error.h>
#include <core/log.h>
#include <core/profiler.h>

#include <modules/entity_core/entity_api.h>
#include <modules/entity_core/name_component.h>
#include <modules/entity_core/selected_component.h>
#include <modules/entity_core/transform_component.h>
#include <modules/ui_widgets/ui_widgets.h>

#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>

#include <modules/ui/style_colors.inl>

#include <core/stretchy_buffers.inl>

#include <string.h>

// ------------------------------------------------------------------------------------------------------------------------------------------

static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_entity_api* fa_entity_api;
static struct fa_application_api* fa_application_api;
static struct fa_string_api* fa_string_api;
static struct fa_logger_api* fa_logger_api;

static char add_component_filter[64];
static void draw_add_component_list(void)
{
    fa_entity_context_t* entity_ctx = fa_application_api->get_active_entity_context();
    const fa_component_type_t selected_type = fa_entity_api->component_type_get(FA_SELECTED_COMPONENT_NAME);
    const fa_entity_filter_t selected_filter = fa_entity_api->entity_filter_create(entity_ctx, (fa_entity_filter_desc_t){ .type_ids = { selected_type } });
    // maybe every component should have a "base" entity with a special component type which can be queried via regular filters
    fa_component_description_t const** component_descriptions = fa_entity_api->component_type_list();

    if (igBeginPopupContextItem("##AddComponentList", ImGuiWindowFlags_None))
    {
        igInputTextWithHint("##ComponentFilter", "Search", add_component_filter, 64, 0, NULL, NULL);
        for (uint32_t i = 0; i < fa_sbuff_num(component_descriptions); ++i)
        {
            // skip over components without display names
            if (component_descriptions[i]->display_name == NULL)
            {
                continue;
            }

            if (add_component_filter[0] == 0 || strstr(component_descriptions[i]->display_name, add_component_filter) != NULL)
            {
                if (igMenuItem_Bool(component_descriptions[i]->display_name, NULL, false, true))
                {
                    const fa_component_type_t component_type = fa_entity_api->component_type_get(component_descriptions[i]->name);
                    fa_entity_api->component_add(entity_ctx, selected_filter.entities[0], component_type);
                }
            }
        }

        igEndPopup();
    }
}

static void draw(void)
{
    FA_PROFILER_ZONE_N(ctx, "draw_property_editor", true);
    const ImGuiStyle* style = igGetStyle();

    fa_entity_context_t* entity_ctx = fa_application_api->get_active_entity_context();
    const fa_component_type_t name_type = fa_entity_api->component_type_get(FA_NAME_COMPONENT_NAME);
    const fa_component_type_t selected_type = fa_entity_api->component_type_get(FA_SELECTED_COMPONENT_NAME);
    const fa_entity_filter_t name_filter = fa_entity_api->entity_filter_create(entity_ctx, (fa_entity_filter_desc_t){ .type_ids = { name_type, selected_type } });
    if (fa_sbuff_num(name_filter.entities) == 0)
    {
        igBegin(ICON_FA_INFO_CIRCLE " Properties", NULL, 0);
        igText("No selected entity");
        igEnd();
        FA_PROFILER_ZONE_END(ctx);
        return;
    }

    fa_name_component_t* name_component = fa_entity_api->component_get(entity_ctx, name_filter.entities[0], name_type);

    if (igBegin(ICON_FA_INFO_CIRCLE " Properties", NULL, 0))
    {
        const float y_offset = 0.5f * style->FramePadding.y;
        igSetCursorPosY(igGetCursorPosY() + y_offset);
        igText("Name:");

        igSameLine(0, 0);
        igSetCursorPosY(igGetCursorPosY() - y_offset);
        igSetCursorPosX(igGetCursorPosX() + style->ItemSpacing.x);
        ImVec2 content_region;
        igGetContentRegionAvail(&content_region);
        igSetNextItemWidth(content_region.x);

        ImVec2 main_viewport_center = igGetMainViewport()->Pos;
        const ImVec2 main_viewport_size = igGetMainViewport()->Size;
        main_viewport_center.x += main_viewport_size.x * 0.5f;
        main_viewport_center.y += main_viewport_size.y * 0.5f;
        igSetNextWindowPos(main_viewport_center, ImGuiCond_Appearing, (ImVec2){ 0.5f, 0.5f });

        const char* error_window_name = "Error##EntityNameEmptyWarning";
        // Warning modal for empty entity names
        if (igBeginPopupModal(error_window_name, NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            igTextColored(style_color_error, "Entities cannot have empty names!");

            igText("");
            if (igButton("Ok", (ImVec2){ 240.f, 0.f }))
            {
                igCloseCurrentPopup();
            }
            igEndPopup();
        }

        char buff[128];
        strncpy(buff, name_component->name.str, name_component->name.len + 1);
        if (igInputText("##PropertyEditorEntityName", buff, 128, 0, NULL, NULL))
        {
            if (buff[0] == 0)
            {
                igOpenPopup_Str(error_window_name, 0);
            }
            if (buff[0] != 0)
            {
                fa_string_api->copy(&name_component->name, buff);
            }
        }

        const ImGuiTableFlags table_flags = ImGuiTableFlags_Resizable;
        const ImGuiTreeNodeFlags treenode_flags = ImGuiTreeNodeFlags_DefaultOpen;

        const fa_component_type_t transform_type = fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME);
        fa_transform_component_t* transform_component = fa_entity_api->component_get(entity_ctx, name_filter.entities[0], transform_type);
        if (transform_component != NULL)
        {
            if (igCollapsingHeader_TreeNodeFlags("Transform", treenode_flags))
            {

                if (igBeginTable("Properties###Transform", 2, table_flags, IM_VEC2(0, 0), 0.f))
                {
                    if (fa_widgets_api->draw_transform(&transform_component->pos, &transform_component->rot, &transform_component->scale))
                    {
                        fa_recompute_transform_matrix(transform_component);
                    }
                    igEndTable();
                }
            }
        }

        if (igCollapsingHeader_TreeNodeFlags("Material", treenode_flags))
        {
            if (igBeginTable("Properties###Material", 2, table_flags, IM_VEC2(0, 0), 0.f))
            {
                igTableNextColumn();

                ImVec2 size;
                igGetContentRegionAvail(&size);
                const float column_width = size.x;
                igCalcTextSize(&size, "Shader", NULL, false, -1.f);
                igSetCursorPosX(igGetCursorPosX() + column_width - (size.x + style->ItemSpacing.x));

                igSetCursorPosY(igGetCursorPosY() + style->FramePadding.x);
                igText("Shader");

                igTableNextColumn();

                igSetCursorPosX(igGetCursorPosX() + style->FramePadding.x);

                static int current_item = 0;
                igSetNextItemWidth(-1.f);
                igCombo_Str("##ShaderCombo", &current_item, "Standard\0Flat", 4);
                igEndTable();
            }
        }

        // Add Component button
        {
            ImVec2 window_pos;
            igGetWindowPos(&window_pos);
            ImDrawList_AddRect(igGetWindowDrawList(), IM_VEC2(window_pos.x + igGetCursorPosX(), window_pos.y + igGetCursorPosY() + igGetTextLineHeightWithSpacing() / 2), IM_VEC2(window_pos.x + content_region.x, window_pos.y + igGetCursorPosY() + igGetTextLineHeightWithSpacing() / 2), CIM_COL32_BLACK, 5.f, 0, 1.f);
            igText("");

            ImVec2 text_size;
            const char* add_component_text = ICON_FA_PLUS " Add Component";
            igCalcTextSize(&text_size, add_component_text, NULL, false, -1.f);
            text_size.x += 8 * style->FramePadding.x;
            const float button_pos = (content_region.x - (text_size.x)) / 2;

            igSetCursorPosX(button_pos);

            draw_add_component_list();
            if (igButton(add_component_text, IM_VEC2(text_size.x, 0)))
            {
                add_component_filter[0] = 0;
                igOpenPopup_Str("##AddComponentList", 0);
            }
        }
    }

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

static void set_focused_entity(fa_entity_t entity)
{
}

// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_properties_panel_api* fa_properties_panel_api = &(struct fa_properties_panel_api){
    .draw = draw,
    .set_focused_entity = set_focused_entity
};

void load_properties(struct fa_api_registry* registry, bool load)
{
    fa_widgets_api = fa_get_api(FA_UI_WIDGETS_API_NAME);
    fa_application_api = fa_get_api(FA_APPLICATION_API_NAME);
    fa_entity_api = fa_get_api(FA_ENTITY_API_NAME);
    fa_string_api = fa_get_api(FA_STRING_API_NAME);
    fa_logger_api = fa_get_api(FA_LOGGER_API_NAME);
    fa_add_or_remove_api(fa_properties_panel_api, FA_PROPERTIES_PANEL_API_NAME, load);
}
