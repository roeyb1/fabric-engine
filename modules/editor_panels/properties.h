#pragma once
#include <modules/entity_core/entity_types.h>

struct fa_properties_panel_api
{
    void (*draw)(void);
    void (*set_focused_entity)(fa_entity_t entity);
};

#define FA_PROPERTIES_PANEL_API_NAME "FA_PROPERTIES_PANEL_API"

#if defined(FA_LINKS_TO_EDITOR_PANELS)
extern struct fa_properties_panel_api* fa_properties_panel_api;
#endif // defined(FA_LINKS_TO_EDITOR_PANELS)