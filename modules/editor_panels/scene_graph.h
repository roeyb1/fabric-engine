#pragma once

struct fa_scene_graph_panel_api
{
    void (*draw)(void);
};

#define FA_SCENE_GRAPH_PANEL_API_NAME "FA_SCENE_GRAPH_PANEL_API"

#if defined(FA_LINKS_TO_EDITOR_PANELS)
extern struct fa_scene_graph_panel_api* fa_scene_graph_panel_api;
#endif // defined(FA_LINKS_TO_EDITOR_PANELS)
