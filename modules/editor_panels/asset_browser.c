#include "asset_browser.h"

#include <core/api_registry.h>
#include <core/core_defines.h>
#include <core/profiler.h>

#include <modules/ui/ui.h>
#include <modules/ui_widgets/ui_widgets.h>

#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>

#include <modules/ui/style_colors.inl>

// ------------------------------------------------------------------------------------------------------------------------------------------

static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_ui_api* fa_ui_api;

static void draw(void)
{
    FA_PROFILER_ZONE_N(ctx, "draw_asset_panel", true);

    const ImGuiStyle* style = igGetStyle();

    igPushStyleColor_Vec4(ImGuiCol_Separator, style_color_surface);

    if (igBegin(ICON_FA_FOLDER_TREE " Assets", NULL, 0))
    {
        const ImGuiID dockspace = igGetID_Str("AssetDockspace");

        static ImGuiID center = 0, left = 0, right = 0;

        if (!igDockBuilderGetNode(dockspace))
        {
            igDockBuilderRemoveNode(dockspace);
            igDockBuilderAddNode(dockspace, ImGuiDockNodeFlags_None);
            igDockBuilderSplitNode(dockspace, ImGuiDir_Right, 0.8f, &right, &left);
            igDockBuilderDockWindow("AssetFiles", left);
            igDockBuilderDockWindow("AssetThumbnails", center);

            igDockBuilderFinish(dockspace);
        }

        igDockSpace(dockspace, (ImVec2){ 0, 0 }, ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar, NULL);

        static float item_size = 128.f;
        {
            if (igBegin("AssetFiles", NULL, 0))
            {
                if (igTreeNodeEx_Str(ICON_FA_FOLDER " Content", ImGuiTreeNodeFlags_DefaultOpen))
                {
                    if (igTreeNodeEx_Str(ICON_FA_FOLDER " Fonts", ImGuiTreeNodeFlags_Leaf))
                    {
                        igTreePop();
                    }
                    if (igTreeNodeEx_Str(ICON_FA_FOLDER " Shaders", ImGuiTreeNodeFlags_Leaf))
                    {
                        igTreePop();
                    }
                    if (igTreeNodeEx_Str(ICON_FA_FOLDER " Textures", ImGuiTreeNodeFlags_Leaf))
                    {
                        igTreePop();
                    }
                    igTreePop();
                }
            }
            igEnd();

            if (igBegin("AssetThumbnails", NULL, 0))
            {
                static char filter_buff[128];
                igText(ICON_FA_FILTER);
                igSameLine(0, 5);

                igSetNextItemWidth(256);
                igPushFont(fa_ui_api->get_small_font());
                igInputTextWithHint("##AssetFilter", "Search (Ctrl + F)", filter_buff, 64, 0, NULL, NULL);
                igPopFont();

                const float item_width = 128;
                ImVec2 content_region;
                igGetContentRegionAvail(&content_region);
                igSameLine(content_region.x - (item_width), 0);
                igPushItemWidth(item_width);
                igSliderFloat("##ItemSize", &item_size, 32.f, 256.f, "%1.f", 0);

                if (igBeginChild_Str("##AssetFilesTable", IM_VEC2(0, 0), false, 0))
                {
                    ImVec2 region;
                    igGetContentRegionAvail(&region);
                    const ImVec2 item_extent = { item_size, item_size };
                    const ImVec2 cell_size = {
                        .x = item_extent.x + style->ItemSpacing.x,
                        .y = item_extent.y + style->ItemSpacing.y,
                    };
                    const uint32_t num_columns = fa_min(fa_max((uint32_t)(region.x / cell_size.x), 1), 64);

                    if (igBeginTable("Assets", num_columns, ImGuiTableFlags_PadOuterX, IM_VEC2(0, 0), 10.f))
                    {
                        for (uint32_t i = 0; i < 500; ++i)
                        {
                            igTableNextColumn();
                            igButton(ICON_FA_FILE, item_extent);

                            ImVec2 text_size;
                            igCalcTextSize(&text_size, "file.png", NULL, false, -1.f);

                            igSetCursorPosX(igGetCursorPosX() + (item_extent.x - text_size.x) / 2);
                            igText("file.png");
                        }
                        igEndTable();
                    }
                }
                igEndChild();
            }
            igEnd();
        }
    }

    igEnd();
    igPopStyleColor(1);
    FA_PROFILER_ZONE_END(ctx);
}

// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_asset_browser_panel_api* fa_asset_browser_panel_api = &(struct fa_asset_browser_panel_api){
    .draw = draw,
};

void load_asset_browser(struct fa_api_registry* registry, bool load)
{
    fa_widgets_api = fa_get_api(FA_UI_WIDGETS_API_NAME);
    fa_ui_api = fa_get_api(FA_UI_API_NAME);
    fa_add_or_remove_api(fa_asset_browser_panel_api, FA_ASSET_BROWSER_PANEL_API_NAME, load);
}
