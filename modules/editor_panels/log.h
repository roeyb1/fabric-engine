#pragma once

struct fa_log_panel_api
{
    void (*draw)(void);
};

#define FA_LOG_PANEL_API_NAME "FA_LOG_PANEL_API"

#if defined(FA_LINKS_TO_EDITOR_PANELS)
extern struct fa_log_panel_api* fa_log_panel_api;
#endif // defined(FA_LINKS_TO_EDITOR_PANELS)
