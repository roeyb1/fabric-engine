#include "log.h"

#include "core/array.h"

#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/log.h>
#include <core/profiler.h>

#include <modules/ui/ui.h>
#include <modules/ui_widgets/ui_widgets.h>

#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>

#include <string.h>

#include <core/stretchy_buffers.inl>
#include <modules/ui/style_colors.inl>

// ------------------------------------------------------------------------------------------------------------------------------------------

static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_ui_api* fa_ui_api;
static struct fa_logger_api* fa_logger_api;
static struct fa_allocator_api* fa_allocator_api;

// ------------------------------------------------------------------------------------------------------------------------------------------

typedef struct log_message
{
    char* text;
    enum fa_log_severity severity;
} log_message;

struct fa_log_panel_data
{
    fa_logger_i logger;
    /* sbuff */ log_message* messages;

    bool autoscroll_disabled;
    bool verbose_output_enabled;
};

static const char* severity_prefix[] = { "[Verbose]", "[Info]", "[Warning]", "[Error]", "[Fatal]" };
static struct fa_log_panel_data* log_panel_data;
static bool needs_scroll = true;

static void log_panel_log(fa_logger_t* logger, enum fa_log_severity severity, const char* message)
{
    const uint64_t count = snprintf(NULL, 0, "%s", message);

    char* buff = fa_malloc(count + 1);
    snprintf(buff, count + 1, "%s", message);
    fa_sbuff_push(log_panel_data->messages, ((log_message){ .text = buff, .severity = severity }));
    needs_scroll = true;
}

static void clear_log(void)
{
    for (uint32_t i = 0; i < fa_sbuff_num(log_panel_data->messages); ++i)
    {
        fa_free(log_panel_data->messages[i].text);
    }
    fa_sbuff_resize(log_panel_data->messages, 0);
}

static void draw(void)
{
    FA_PROFILER_ZONE_N(ctx, "draw_log", true);
    if (igBegin(ICON_FA_COMMENT_ALT " Log", NULL, 0))
    {
        // adjust the filter icon position otherwise it doesn't center on the text input
        const float old_cursor_pos_y = igGetCursorPosY();
        igSetCursorPosY(old_cursor_pos_y + igGetStyle()->ItemInnerSpacing.y);
        igText(ICON_FA_FILTER " ");
        igSameLine(0, 0);
        static char filter_buff[128];
        igSetCursorPosY(old_cursor_pos_y);
        igPushFont(fa_ui_api->get_small_font());
        igInputTextWithHint(" ##LogFilter", "Search (Ctrl + F)", filter_buff, FA_ARRAY_COUNT(filter_buff), 0, NULL, NULL);

        igSameLine(0, 0);

        if (igButton("Clear", (ImVec2){ 0, 0 }))
        {
            clear_log();
        }

        igSameLine(0, 0);
        static bool autoscroll_enabled = true;
        autoscroll_enabled = !log_panel_data->autoscroll_disabled;
        igCheckbox("Autoscroll", &autoscroll_enabled);
        log_panel_data->autoscroll_disabled = !autoscroll_enabled;

        igSameLine(0, 0);
        if (igCheckbox("Verbose", &log_panel_data->verbose_output_enabled))
        {
            needs_scroll = true;
        }

        igPopFont();

        ImVec2 size;
        igGetContentRegionAvail(&size);
        size.y -= igGetTextLineHeight() + igGetStyle()->FramePadding.y;
        igPushStyleColor_Vec4(ImGuiCol_ChildBg, style_color_overlay_1);
        igPushStyleColor_Vec4(ImGuiCol_ScrollbarBg, style_color_overlay_1);
        igPushStyleColor_Vec4(ImGuiCol_Border, style_color_overlay_4);
        if (igBeginChild_Str("##LogInner", size, true, 0))
        {
            for (uint32_t i = 0; i < fa_sbuff_num(log_panel_data->messages); ++i)
            {
                if (log_panel_data->messages[i].severity == FA_LOG_SEVERITY_Verbose && !log_panel_data->verbose_output_enabled)
                {
                    continue;
                }
                // #todo: implement strcasestr (case insensitive substring)
                if (filter_buff[0] == 0 || strstr(log_panel_data->messages[i].text, filter_buff) != NULL)
                {
                    ImVec4 severity_color = IM_VEC4(1.f, 1.f, 1.f, 1.f);
                    if (log_panel_data->messages[i].severity == FA_LOG_SEVERITY_Error || log_panel_data->messages[i].severity == FA_LOG_SEVERITY_Fatal)
                    {
                        severity_color = style_color_error;
                    }
                    else if (log_panel_data->messages[i].severity == FA_LOG_SEVERITY_Verbose)
                    {
                        severity_color = style_color_disabled;
                    }
                    // igTextColored(severity_color, "%s: ", severity_prefix[log_panel_data->messages[i].severity], NULL);
                    // igSameLine(0, 0);
                    igTextColored(severity_color, "%s", log_panel_data->messages[i].text);
                }
            }

            if (needs_scroll && !log_panel_data->autoscroll_disabled)
            {
                igSetScrollHereY(1.f);
                needs_scroll = false;
            }
        }
        igPopStyleColor(3);
        igEndChild();
    }
    igEnd();

    FA_PROFILER_ZONE_END(ctx);
}

// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_log_panel_api* fa_log_panel_api = &(struct fa_log_panel_api){
    .draw = draw,
};

void load_log(struct fa_api_registry* registry, bool load)
{
    if (!load)
    {
        fa_logger_api->unregister_logger(&log_panel_data->logger);
    }

    fa_widgets_api = fa_get_api(FA_UI_WIDGETS_API_NAME);
    fa_ui_api = fa_get_api(FA_UI_API_NAME);
    fa_logger_api = fa_get_api(FA_LOGGER_API_NAME);
    fa_allocator_api = fa_get_api(FA_ALLOCATOR_API_NAME);
    fa_add_or_remove_api(fa_log_panel_api, FA_LOG_PANEL_API_NAME, load);

    log_panel_data = registry->static_variable(FA_LOG_PANEL_API_NAME, "log_panel_data", sizeof(struct fa_log_panel_data));

    // hook the logger into the main log callback system
    if (load)
    {
        log_panel_data->logger = (fa_logger_i){ .log = log_panel_log };
        fa_logger_api->register_logger(&log_panel_data->logger);
    }
}
