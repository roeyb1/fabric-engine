#include "scene_graph.h"

#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/profiler.h>
#include <core/stretchy_buffers.inl>

#include <modules/entity_core/entity_api.h>
#include <modules/entity_core/name_component.h>
#include <modules/entity_core/selected_component.h>
#include <modules/ui/ui.h>
#include <modules/ui_widgets/ui_widgets.h>

#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>

#include <modules/ui/style_colors.inl>

#include <string.h>

// ------------------------------------------------------------------------------------------------------------------------------------------

static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_ui_api* fa_ui_api;
static struct fa_application_api* fa_application_api;
static struct fa_entity_api* fa_entity_api;

static void draw(void)
{
    FA_PROFILER_ZONE_N(ctx, "draw_scene_graph", true);
    const ImGuiStyle* style = igGetStyle();

    igBegin(ICON_FA_CUBES " Scene Graph", NULL, ImGuiWindowFlags_MenuBar);
    igBeginMenuBar();
    // filter
    {
        igText(ICON_FA_FILTER);
        igSameLine(0, 8);
        static char buff[64];
        igPushFont(fa_ui_api->get_small_font());
        igInputTextWithHint("##SceneGraphSearch", "Search (Ctrl + F)", buff, 64, 0, NULL, NULL);
        igPopFont();
    }
    igEndMenuBar();

    fa_entity_context_t* entity_ctx = fa_application_api->get_active_entity_context();
    const fa_component_type_t name_type = fa_entity_api->component_type_get(FA_NAME_COMPONENT_NAME);
    const fa_component_type_t selected_type = fa_entity_api->component_type_get(FA_SELECTED_COMPONENT_NAME);
    const fa_entity_filter_t name_filter = fa_entity_api->entity_filter_create(entity_ctx, (fa_entity_filter_desc_t){ .type_ids = { name_type } });
    const fa_entity_filter_t selected_filter = fa_entity_api->entity_filter_create(entity_ctx, (fa_entity_filter_desc_t){ .type_ids = { selected_type } });

    bool clear_selection = false;
    fa_entity_t* entities_to_select = NULL;

    // Context menu
    {
        const char* scene_graph_context_menu_name = "##SceneGraphContext";

        igPushStyleColor_Vec4(ImGuiCol_Border, style_color_active);
        if (igBeginPopup(scene_graph_context_menu_name, 0))
        {
            ImVec2 new_spacing = style->ItemSpacing;
            new_spacing.y *= 2.f;
            igPushStyleVar_Vec2(ImGuiStyleVar_ItemSpacing, new_spacing);
            const char* add_entity_text = "Add Entity";

            // non-selection dependent operations
            if (igMenuItem_Bool(add_entity_text, "Space", false, true))
            {
                fa_entity_api->entity_create(entity_ctx, "New Entity");
            }

            // selection dependent operations
            if (selected_filter.entities != NULL)
            {
                igPushStyleColor_Vec4(ImGuiCol_Separator, style_color_active);
                igSeparator();
                igPopStyleColor(1);

                if (igMenuItem_Bool("Add Component", "Ctrl+Space", false, true))
                {
                }

                if (igMenuItem_Bool("Rename", "F2", false, true))
                {
                }
                if (igMenuItem_Bool("Copy", "Ctrl+C", false, true))
                {
                }
                if (igMenuItem_Bool("Cut", "Ctrl+X", false, true))
                {
                }
                if (igMenuItem_Bool("Paste", "Ctrl+V", false, true))
                {
                }
                if (igMenuItem_Bool("Delete", "Delete", false, true))
                {
                }
            }

            igPopStyleVar(1);
            igEndPopup();
        }
        igPopStyleColor(1);

        if (igIsWindowHovered(ImGuiHoveredFlags_RootAndChildWindows) && igIsMouseReleased(ImGuiMouseButton_Right))
        {
            igOpenPopup_Str(scene_graph_context_menu_name, 0);
        }
        if (igIsWindowHovered(ImGuiHoveredFlags_RootWindow) && igIsMouseReleased(ImGuiMouseButton_Left))
        {
            clear_selection = true;
        }
    }

    // Tree
    {
        const ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding;
        igPushStyleColor_Vec4(ImGuiCol_HeaderHovered, style_color_active);
        igPushStyleColor_Vec4(ImGuiCol_Header, style_color_primary);
        igPushStyleColor_Vec4(ImGuiCol_HeaderActive, style_color_primary);

        ImVec2 new_spacing = style->ItemSpacing;
        new_spacing.y = 0.f;
        igPushStyleVar_Vec2(ImGuiStyleVar_ItemSpacing, new_spacing);
        ImVec2 new_frame_padding = style->FramePadding;
        new_frame_padding.y /= 2.f;
        igPushStyleVar_Vec2(ImGuiStyleVar_FramePadding, new_frame_padding);
        if (igTreeNodeEx_Str(ICON_FA_GLOBE " Scene", ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding))
        {
            for (uint32_t i = 0; i < fa_sbuff_num(name_filter.entities); ++i)
            {
                const fa_name_component_t* name_component = fa_entity_api->component_get(entity_ctx, name_filter.entities[i], name_type);

                ImGuiTreeNodeFlags node_flags = base_flags;
                if (fa_entity_api->component_get(entity_ctx, name_filter.entities[i], selected_type) != NULL)
                {
                    node_flags |= ImGuiTreeNodeFlags_Selected;
                }

                const bool is_open = igTreeNodeEx_Ptr((void*)(intptr_t)i, node_flags, ICON_FA_CUBE " %s", name_component->name.str);
                if (igIsItemHovered(0) && (igIsMouseReleased(ImGuiMouseButton_Left) || igIsMouseReleased(ImGuiMouseButton_Right)))
                {
                    clear_selection = true;
                    fa_sbuff_push(entities_to_select, name_filter.entities[i]);
                }

                if (is_open)
                {
                    igTreePop();
                }
            }
            igTreePop();
        }
        igPopStyleColor(3);
        igPopStyleVar(2);
    }

    // #todo: support multiple selection eventually
    if (clear_selection)
    {
        for (uint32_t i = 0; i < fa_sbuff_num(selected_filter.entities); ++i)
        {
            fa_entity_api->component_remove(entity_ctx, selected_filter.entities[i], selected_type);
        }
    }
    for (uint32_t i = 0; i < fa_sbuff_num(entities_to_select); ++i)
    {
        fa_entity_api->component_add(entity_ctx, entities_to_select[i], selected_type);
    }
    if (entities_to_select != NULL)
    {
        fa_sbuff_free(entities_to_select);
    }

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_scene_graph_panel_api* fa_scene_graph_panel_api = &(struct fa_scene_graph_panel_api){
    .draw = draw,
};

void load_scene_graph(struct fa_api_registry* registry, bool load)
{
    fa_widgets_api = fa_get_api(FA_UI_WIDGETS_API_NAME);
    fa_ui_api = fa_get_api(FA_UI_API_NAME);
    fa_application_api = fa_get_api(FA_APPLICATION_API_NAME);
    fa_entity_api = fa_get_api(FA_ENTITY_API_NAME);
    fa_add_or_remove_api(fa_scene_graph_panel_api, FA_SCENE_GRAPH_PANEL_API_NAME, load);
}
