#pragma once

struct fa_asset_browser_panel_api
{
    void (*draw)(void);
};

#define FA_ASSET_BROWSER_PANEL_API_NAME "FA_ASSET_BROWSER_PANEL_API"

#if defined(FA_LINKS_TO_EDITOR_PANELS)
extern struct fa_asset_browser_panel_api* fa_asset_browser_panel_api;
#endif // defined(FA_LINKS_TO_EDITOR_PANELS)
