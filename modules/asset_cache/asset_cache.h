#pragma once

#include <core/core_types.h>

typedef uint64_t fa_asset_handle_t;

typedef enum {
    NONE = 0,
    FA_ASSET_TYPE_MESH = 1,
    FA_ASSET_TYPE_IMAGE = 2,
} fa_asset_type;

struct fa_asset_cache_api
{
    /**
     * Loads an asset into the cache.
     * Returns a handle to the asset.
     */
    fa_asset_handle_t (*load_asset)(const char* filename, fa_asset_type type);

    void (*release_asset)(fa_asset_handle_t handle);

    void* (*get_asset_data)(fa_asset_handle_t handle);
};

#define FA_ASSET_CACHE_API_NAME "FA_ASSET_CACHE_API"
