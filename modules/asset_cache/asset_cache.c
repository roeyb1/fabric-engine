#include "asset_cache.h"
#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/error.h>
#include <core/log.h>
#include <core/profiler.h>

#include <cglm/struct.h>

#include <modules/asset_import/asset_import.h>
#include <modules/renderer/mesh_component.h>

#include <string.h>

#define LOG_CATEGORY "Asset Cache"

static struct fa_logger_api* fa_logger_api;
static struct fa_asset_import_api* asset_import_api;
static struct fa_allocator_api* fa_allocator_api;

#include <core/hash.inl>
#include <core/stretchy_buffers.inl>

typedef struct FA_HASH_T(fa_asset_handle_t, void*) fa_hash_test_t;

typedef struct asset_cache_t
{
    fa_hash_test_t asset_map;
    fa_asset_handle_t next_handle;
} asset_cache_t;

static asset_cache_t* asset_cache;

static fa_asset_handle_t load_asset(const char* filename, fa_asset_type type)
{
    FA_PROFILER_ZONE(ctx, true);

    void* data = NULL;
    switch (type)
    {
    case FA_ASSET_TYPE_MESH:
        vertex_t* vertices = NULL;
        uint32_t* indices = NULL;
        const bool succeeded = asset_import_api->load_obj(filename, &vertices, &indices);
        if (!succeeded)
        {
            FA_PROFILER_ZONE_END(ctx);
            return FA_INVALID_HANDLE;
        }
        // #todo: there's probably a better way to store this
        fa_dynamic_mesh_component_t* mesh = fa_malloc(sizeof(fa_dynamic_mesh_component_t));
        mesh->indices = indices;
        mesh->vertices = vertices;
        mesh->material = NULL;

        data = mesh;
        break;
    case FA_ASSET_TYPE_IMAGE:
        break;
    case NONE:
    default:
        checkf(false, "Attempting to load unknown asset type");
        break;
    }

    const fa_asset_handle_t handle = ++asset_cache->next_handle;
    fa_hash_add(&asset_cache->asset_map, handle, data);

    FA_PROFILER_ZONE_END(ctx);
    return handle;
}

static void release_asset(fa_asset_handle_t handle)
{
    void* data = fa_hash_get(&asset_cache->asset_map, handle);
    if (data != NULL)
    {
        fa_hash_remove(&asset_cache->asset_map, handle);
        fa_free(data);
    }
}

static void* get_asset_data(fa_asset_handle_t handle)
{
    return fa_hash_get(&asset_cache->asset_map, handle);
}

struct fa_asset_cache_api asset_cache_api = {
    .load_asset = load_asset,
    .release_asset = release_asset,
    .get_asset_data = get_asset_data,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_allocator_api = registry->get(FA_ALLOCATOR_API_NAME);
    asset_import_api = registry->get(FA_ASSET_IMPORT_API_NAME);

    fa_add_or_remove_api(&asset_cache_api, FA_ASSET_CACHE_API_NAME, load);

    asset_cache = (asset_cache_t*)registry->static_variable(FA_ASSET_CACHE_API_NAME, "asset_cache", sizeof(asset_cache_t));
}
