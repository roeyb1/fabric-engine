#pragma once

typedef struct ImFont ImFont;

struct fa_ui_api
{
    void (*create)(void);

    void (*destroy)(void);

    ImFont* (*get_small_font)(void);
};

#define FA_UI_API_NAME "FA_UI_API"