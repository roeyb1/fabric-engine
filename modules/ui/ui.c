#include "ui.h"
#include <core/api_registry.h>
#include <core/core_defines.h>

#include <vulkan/vulkan.h>

#include <modules/vulkan_backend/vulkan_backend.h>
#include <modules/window/window_api.h>

#include <cimgui/cimgui.h>
#include <float.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>

#include "style_colors.inl"

static struct fa_window_api* fa_window_api;
static struct fa_vulkan_backend_api* fa_vulkan_backend_api;

struct ui_context_t
{
    ImGuiContext* ctx;
    ImFont* main_font;
    ImFont* small_font;
};

static struct ui_context_t* ui_ctx;

static void set_style_vars()
{
    ImGuiStyle* style = &ui_ctx->ctx->Style;
    igStyleColorsDark(style);

    // Setup style colors
    {
        style->Colors[ImGuiCol_WindowBg] = style_color_surface;
        style->Colors[ImGuiCol_ChildBg] = style_color_overlay_2;
        style->Colors[ImGuiCol_TitleBg] = style_color_surface;
        style->Colors[ImGuiCol_TitleBgActive] = (ImVec4){ 0.06f, 0.06f, 0.06f, 1.f };

        style->Colors[ImGuiCol_MenuBarBg] = style_color_overlay_2;
        style->Colors[ImGuiCol_PopupBg] = style_color_overlay_3;

        style->Colors[ImGuiCol_Tab] = style_color_surface;
        style->Colors[ImGuiCol_TabActive] = style_color_overlay_2;
        style->Colors[ImGuiCol_TabHovered] = style_color_overlay_2;
        style->Colors[ImGuiCol_TabUnfocused] = style_color_surface;
        style->Colors[ImGuiCol_TabUnfocusedActive] = style_color_overlay_2;

        style->Colors[ImGuiCol_FrameBg] = style_color_overlay_1;
        style->Colors[ImGuiCol_FrameBgHovered] = style_color_overlay_4;
        style->Colors[ImGuiCol_FrameBgActive] = style_color_active;

        style->Colors[ImGuiCol_Header] = style_color_overlay_4;
        style->Colors[ImGuiCol_HeaderActive] = style_color_active;
        style->Colors[ImGuiCol_HeaderHovered] = style_color_primary;

        style->Colors[ImGuiCol_Button] = style_color_overlay_4;
        style->Colors[ImGuiCol_ButtonHovered] = style_color_primary;
        style->Colors[ImGuiCol_ButtonActive] = style_color_primary_dark;

        style->Colors[ImGuiCol_Border] = style_color_overlay_4;
        style->Colors[ImGuiCol_Border].w = 0.8f;

        style->Colors[ImGuiCol_CheckMark] = style_color_primary;
        style->Colors[ImGuiCol_SliderGrab] = style_color_primary;
        style->Colors[ImGuiCol_SliderGrabActive] = style_color_primary_dark;

        style->Colors[ImGuiCol_DockingPreview] = style_color_primary;

        style->Colors[ImGuiCol_Separator] = style_color_active;
        style->Colors[ImGuiCol_Separator].w = 0.f;
        style->Colors[ImGuiCol_SeparatorActive] = style_color_primary_light;
        style->Colors[ImGuiCol_SeparatorHovered] = style_color_primary;

        style->Colors[ImGuiCol_ResizeGripActive] = style_color_primary_dark;
        style->Colors[ImGuiCol_ResizeGripHovered] = style_color_primary;
        style->Colors[ImGuiCol_ResizeGrip] = style_color_primary_dark;
    }

    // Setup sizes
    {
        style->TabRounding = 8;
        style->TabBorderSize = 0;

        style->FrameRounding = 6;
        style->FrameBorderSize = 1;
        style->FramePadding = IM_VEC2(6, 8);

        style->WindowRounding = 1;

        style->GrabMinSize = 6;
        style->GrabRounding = 3;

        style->PopupRounding = 3;

        style->WindowPadding = (ImVec2){ 8, 8 };
        style->IndentSpacing = 25;
        style->ScrollbarSize = 14;

        style->CircleTessellationMaxError = 0.1f;
    }

    {
        style->WindowMenuButtonPosition = ImGuiDir_None;
    }
}

static void create(void)
{
    ui_ctx->ctx = igCreateContext(NULL);
    igSetCurrentContext(ui_ctx->ctx);
    ImGuiIO* io = igGetIO();
    io->IniFilename = "content/ui_layout.ini";

    io->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io->ConfigFlags |= ImGuiConfigFlags_DockingEnable;

    // const float dpi_scale = fa_window_api->get_dpi_scale_factor((fa_window_t){ 0 });
    const float dpi_scale = 1.f;
    ImGuiStyle_ScaleAllSizes(&ui_ctx->ctx->Style, dpi_scale);

    set_style_vars();

    const float font_size = 18.f;

    ImFontConfig config = { 0 };
    config.FontDataOwnedByAtlas = true;
    config.OversampleH = 1;
    config.OversampleV = 1;
    config.RasterizerMultiply = 1;
    config.GlyphMaxAdvanceX = FLT_MAX;
    config.EllipsisChar = (ImWchar)-1;
    ImFontAtlas_AddFontFromFileTTF(io->Fonts, "content/fonts/Roboto-Regular.ttf", font_size * dpi_scale, &config, NULL);

    config.MergeMode = true;
    config.GlyphOffset.y = 0.5f;
    config.PixelSnapH = true;
    const ImWchar fa_icon_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
    ui_ctx->main_font = ImFontAtlas_AddFontFromFileTTF(io->Fonts, "content/fonts/" FONT_ICON_FILE_NAME_FAR, (font_size - 2.f) * dpi_scale, &config, fa_icon_ranges);

    config.MergeMode = false;
    config.GlyphOffset.y = 0.f;
    ui_ctx->small_font = ImFontAtlas_AddFontFromFileTTF(io->Fonts, "content/fonts/Roboto-Regular.ttf", (font_size - 4.f) * dpi_scale, &config, NULL);

    ImFontAtlas_Build(io->Fonts);

    fa_vulkan_backend_api->init_ui();
}

static void destroy(void)
{
    fa_vulkan_backend_api->destroy_ui();

    igDestroyContext(ui_ctx->ctx);
}

static ImFont* get_small_font(void)
{
    return ui_ctx ? ui_ctx->small_font : NULL;
}

static struct fa_ui_api ui_api = {
    .create = create,
    .destroy = destroy,
    .get_small_font = get_small_font,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_window_api = registry->get(FA_WINDOW_API_NAME);
    fa_vulkan_backend_api = registry->get(FA_VULKAN_BACKEND_API_NAME);

    fa_add_or_remove_api(&ui_api, FA_UI_API_NAME, load);

    ui_ctx = registry->static_variable(FA_UI_API_NAME, "ctx", sizeof(*ui_ctx));
    // Reload styles when this module is hot-reloaded
    if (load && ui_ctx && ui_ctx->ctx)
    {
        set_style_vars();
    }
}