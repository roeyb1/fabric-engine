#pragma once

static const ImVec4 style_color_white = { 1.f, 1.f, 1.f, 1.f };

static const ImVec4 style_color_orange = { 0.819f, 0.278f, 0.007f, 1.f };
static const ImVec4 style_color_red = { 0.764f, 0.133f, 0.058f, 1.f };
static const ImVec4 style_color_red_inactive = { 0.764f, 0.133f, 0.058f, 0.7f };
static const ImVec4 style_color_blue = { 0.250f, 0.474f, 0.549f, 1.f };
static const ImVec4 style_color_blue_inactive = { 0.250f, 0.474f, 0.549f, 0.7f };
static const ImVec4 style_color_green = { 0.596f, 0.592f, 0.101f, 1.f };
static const ImVec4 style_color_green_inactive = { 0.596f, 0.592f, 0.101f, 0.7f };

static const ImVec4 style_color_primary = { 0.250f, 0.474f, 0.549f, 1.f };
static const ImVec4 style_color_primary_light = { 0.439f, 0.662f, 0.631f, 1.f };
static const ImVec4 style_color_primary_dark = { 0.066f, 0.294f, 0.372f, 1.f };

static const ImVec4 style_color_surface = { 0.059f, 0.059f, 0.059f, 1.f };
static const ImVec4 style_color_overlay_1 = { 0.07f, 0.07f, 0.07f, 1.f };
static const ImVec4 style_color_overlay_2 = { 0.117f, 0.117f, 0.117f, 1.f };
static const ImVec4 style_color_overlay_3 = { 0.133f, 0.133f, 0.133f, 1.f };
static const ImVec4 style_color_overlay_4 = { 0.177f, 0.177f, 0.177f, 1.f };
static const ImVec4 style_color_active = { 0.3f, 0.3f, 0.3f, 1.f };

static const ImVec4 style_color_error = { 0.8f, 0.141f, 0.113f, 1.f };
static const ImVec4 style_color_disabled = { 1.f, 1.f, 1.f, 0.7f };
