#pragma once

#include <core/core_types.h>

typedef struct vertex_t vertex_t;

struct fa_asset_import_api
{
    bool (*load_obj)(const char* filename, vertex_t** out_vertices, uint32_t** out_indices);
};

#define FA_ASSET_IMPORT_API_NAME "FA_ASSET_IMPORT_API"
