#include "asset_import.h"
#include <core/api_registry.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/log.h>
#include <core/stretchy_buffers.inl>

#include <cglm/struct.h>
#include <stdio.h>
#include <string.h>

#include <modules/renderer/mesh_component.h>

#define LOG_CATEGORY "Asset Import"

static struct fa_logger_api* fa_logger_api;

// #todo [roey]: this algorithm is very primitive and does not take any advantage of the index buffer. It should be rewritten
static bool load_obj(const char* filename, vertex_t** out_vertices, uint32_t** out_indices)
{
    FILE* file = fopen(filename, "r");
    if (file == NULL)
    {
        FA_LOG_CATEGORY(Warning, "Failed to import asset %s", filename);
        return false;
    }

    // #todo [roey]: allocate these sbuffs from a temp allocator.
    vec3s* positions = NULL;
    vec3s* normals = NULL;
    vec2s* uvs = NULL;

    uint32_t* p_indices = NULL;
    uint32_t* n_indices = NULL;
    uint32_t* uv_indices = NULL;

    while (true)
    {
        char line[128];
        const int32_t result = fscanf(file, "%s", line);
        if (result == EOF)
        {
            break;
        }

        // Vertex pos
        if (strcmp(line, "v") == 0)
        {
            vec3s vertex = { 0 };
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
            fa_sbuff_push(positions, vertex);
        }
        // Vertex normal
        else if (strcmp(line, "vn") == 0)
        {
            vec3s normal = { 0 };
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
            fa_sbuff_push(normals, normal);
        }
        // vertex uv
        else if (strcmp(line, "vt") == 0)
        {
            vec2s uv = { 0 };
            fscanf(file, "%f %f\n", &uv.x, &uv.y);
            fa_sbuff_push(uvs, uv);
        }
        // faces, describes indices
        else if (strcmp(line, "f") == 0)
        {
            uint32_t pos_is[3], uv_is[3], normal_is[3];
            const uint32_t num = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &pos_is[0], &uv_is[0], &normal_is[0], &pos_is[1], &uv_is[1], &normal_is[1], &pos_is[2], &uv_is[2], &normal_is[2]);
            if (num != 9)
            {
                FA_LOG_CATEGORY(Error, "Failed to import .obj file (%s). Vertex layout not supported. Ensure mesh is exported with triangular faces", filename);
                return false;
            }

            fa_sbuff_push_array(p_indices, pos_is, 3);
            fa_sbuff_push_array(n_indices, normal_is, 3);
            fa_sbuff_push_array(uv_indices, uv_is, 3);
        }
    }

    fclose(file);

    const uint32_t num_indices = (uint32_t)fa_sbuff_num(p_indices);
    vertex_t* vertices = NULL;
    uint32_t* indices = NULL;
    fa_sbuff_set_capacity(vertices, num_indices);
    fa_sbuff_set_capacity(indices, num_indices);

    for (uint32_t i = 0; i < num_indices; ++i)
    {
        // OBJ format indexes from 1 so we need to adjust
        const uint32_t pos_index = p_indices[i] - 1;
        const uint32_t normal_index = n_indices[i] - 1;
        const uint32_t uv_index = uv_indices[i] - 1;

        vertex_t vertex;
        vertex.position = positions[pos_index];
        vertex.normal = normals[normal_index];
        vertex.uv = uvs[uv_index];
        // Flip the imported object's uvs
        vertex.uv.x = 1 - vertex.uv.x;

        fa_sbuff_push(vertices, vertex);
        fa_sbuff_push(indices, i);
    }

    *out_vertices = vertices;
    *out_indices = indices;
    return true;
}

static struct fa_asset_import_api import_api = {
    .load_obj = load_obj,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);

    fa_add_or_remove_api(&import_api, FA_ASSET_IMPORT_API_NAME, load);
}