#include "ui_widgets.h"

#include <core/api_registry.h>
#include <core/core_defines.h>

#include <modules/ui/ui.h>
#include <vendor/cimgui/cimgui.h>

#include <modules/ui/style_colors.inl>

static struct fa_ui_api* fa_ui_api;

static bool draw_vec3(const char* label, vec3s* v, vec3s default_val)
{
    const ImGuiStyle* style = igGetStyle();
    bool res = false;

    igTableNextColumn();

    ImVec2 size;
    igPushID_Str(label);
    igSetCursorPosY(igGetCursorPosY() + style->FramePadding.x);
    igGetContentRegionAvail(&size);
    const float column_width = size.x;
    igCalcTextSize(&size, label, NULL, false, -1.f);
    igSetCursorPosX(igGetCursorPosX() + column_width - (size.x + style->ItemSpacing.x));
    igText(label);

    igTableNextColumn();

    igSetCursorPosX(igGetCursorPosX() + style->FramePadding.x);
    ImVec2 avail_size;
    igGetContentRegionAvail(&avail_size);

    ImVec2 button_size;
    igCalcTextSize(&button_size, "X", NULL, false, -1.f);
    button_size.y += 2 * style->FramePadding.y;
    button_size.x = button_size.y;

    const float slider_width = (avail_size.x - (3 * (button_size.x) + style->ItemSpacing.x)) / 3;

    igPushStyleColor_Vec4(ImGuiCol_Button, style_color_red);
    igPushStyleColor_Vec4(ImGuiCol_ButtonHovered, style_color_red_inactive);
    igPushStyleColor_Vec4(ImGuiCol_ButtonActive, style_color_red);
    igPushStyleColor_Vec4(ImGuiCol_Border, style_color_red);
    if (igButton("x", button_size))
    {
        v->x = default_val.x;
        res |= true;
    }
    igPopStyleColor(4);
    igSameLine(0, 0);
    igSetNextItemWidth(slider_width);
    res |= igDragFloat("##Vec3X", &v->x, 0.1f, -FLT_MAX, FLT_MAX, "%.1f", 0);
    igSameLine(0, style->ItemSpacing.x / 2);

    igPushStyleColor_Vec4(ImGuiCol_Button, style_color_green);
    igPushStyleColor_Vec4(ImGuiCol_ButtonActive, style_color_green);
    igPushStyleColor_Vec4(ImGuiCol_ButtonHovered, style_color_green_inactive);
    igPushStyleColor_Vec4(ImGuiCol_Border, style_color_green);
    if (igButton("y", button_size))
    {
        v->y = default_val.y;
        res |= true;
    }
    igPopStyleColor(4);
    igSameLine(0, 0);
    igSetNextItemWidth(slider_width);
    res |= igDragFloat("##Vec3Y", &v->y, 0.1f, -FLT_MAX, FLT_MAX, "%.1f", 0);
    igSameLine(0, style->ItemSpacing.x / 2);

    igPushStyleColor_Vec4(ImGuiCol_Button, style_color_blue);
    igPushStyleColor_Vec4(ImGuiCol_ButtonActive, style_color_blue);
    igPushStyleColor_Vec4(ImGuiCol_ButtonHovered, style_color_blue_inactive);
    igPushStyleColor_Vec4(ImGuiCol_Border, style_color_blue);
    if (igButton("z", button_size))
    {
        v->z = default_val.z;
        res |= true;
    }
    igPopStyleColor(4);
    igSameLine(0, 0);
    igSetNextItemWidth(slider_width);
    res |= igDragFloat("##Vec3Z", &v->z, 0.1f, -FLT_MAX, FLT_MAX, "%.1f", 0);

    igPopID();
    return res;
}

static bool draw_transform(vec3s* position, vec3s* rotation, vec3s* scale)
{
    bool res = false;
    res |= draw_vec3("Position", position, GLMS_VEC3_ZERO);
    res |= draw_vec3("Rotation", rotation, GLMS_VEC3_ZERO);
    res |= draw_vec3("Scale", scale, GLMS_VEC3_ONE);
    return res;
}

static struct fa_ui_widgets_api widgets_api = {
    .draw_transform = draw_transform,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_ui_api = registry->get(FA_UI_API_NAME);
    fa_add_or_remove_api(&widgets_api, FA_UI_WIDGETS_API_NAME, load);
}
