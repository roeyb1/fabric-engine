#pragma once

#include <core/core_types.h>

struct fa_ui_widgets_api
{
    /** Draws a transform widgets. Depends on being in a 2 column table. */
    bool (*draw_transform)(vec3s* position, vec3s* rotation, vec3s* scale);
};

#define FA_UI_WIDGETS_API_NAME "FA_UI_WIDGETS_API"
