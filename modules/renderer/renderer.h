#pragma once

#include <core/core_types.h>

typedef uint32_t fa_texture_t;

// #todo: move/remove these
typedef struct fa_dynamic_mesh_component_t fa_dynamic_mesh_component_t;
typedef struct fa_transform_component_t fa_transform_component_t;
typedef struct vertex_t vertex_t;

typedef struct fa_material_t
{
    FA_ALIGN(4)
    fa_texture_t albedo;
    FA_ALIGN(4)
    fa_texture_t metallic;
    FA_ALIGN(4)
    fa_texture_t roughness;
    FA_ALIGN(4)
    fa_texture_t normal;
    FA_ALIGN(4)
    fa_texture_t ao;
} fa_material_t;

typedef struct fa_view_info_t
{
    mat4s camera_view;
    mat4s camera_proj;
    vec3s camera_pos;
    float near_plane;
    float far_plane;
} fa_view_info_t;

typedef struct fa_point_light_t
{
    FA_ALIGN(16)
    vec3s pos;
    FA_PAD(4);

    FA_ALIGN(16)
    vec3s color;
} fa_point_light_t;

struct fa_renderer_api
{
    void (*create)(void);
    void (*destroy)(void);

    void (*frame_begin)(void);
    void (*frame_draw)(fa_view_info_t* view_info);

    fa_material_t* (*material_create)(void);

    fa_texture_t (*texture_create)(const char* filename);

    fa_point_light_t* (*light_point_create)(void);
};

#define FA_RENDERER_API_NAME "FA_RENDERER_API"