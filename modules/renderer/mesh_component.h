#pragma once

#include <core/core_types.h>

typedef struct fa_material_t fa_material_t;

typedef struct vertex_t
{
    vec3s position;
    vec3s normal;
    vec2s uv;
} vertex_t;

/**
 * Specific mesh type which is redrawn every frame.
 * This is the slow path for rendering. Nothing is cached between frames.
 * There is currently no alternative but there will be a cached static mesh one day.
 */
typedef struct fa_dynamic_mesh_component_t
{
    /* sbuff */ vertex_t* vertices;
    /* sbuff */ uint32_t* indices;
    fa_material_t* material;
} fa_dynamic_mesh_component_t;

#define FA_DYNAMIC_MESH_COMPONENT_NAME "fa_dynamic_mesh_component"