#include <cglm/struct.h>

#include "renderer.h"

#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/array.h>
#include <core/core_defines.h>
#include <core/error.h>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/profiler.h>
#include <core/stretchy_buffers.inl>

#include <string.h>
#include <vulkan/vulkan.h>

#include "modules/entity_core/entity_api.h"
#include <modules/entity_core/transform_component.h>
#include <modules/renderer/mesh_component.h>
#include <modules/vulkan_backend/vulkan_backend.h>

#define STB_IMAGE_IMPLEMENTATION

#include <vendor/stb/stb_image.h>

#define LOG_CATEGORY "Renderer"

static struct fa_vulkan_backend_api* fa_vulkan_backend_api;
static struct fa_logger_api* fa_logger_api;
static struct fa_allocator_api* fa_allocator_api;
static struct fa_job_scheduler_api* fa_job_scheduler_api;
static struct fa_entity_api* fa_entity_api;
static struct fa_application_api* fa_application_api;

typedef struct scene_t
{
    fa_view_info_t view_info;

    /* sbuff */ vertex_t* vertices;
    /* sbuff */ uint32_t* indices;
    /* sbuff */ fa_draw_data_t* draw_data;
    /* sbuff */ VkDrawIndexedIndirectCommand* draw_command_data;

    /* sbuff */ mat4s* transforms;
    /* sbuff */ fa_material_t* materials;
    /* sbuff */ fa_vulkan_texture_t* textures;

    /* sbuff */ fa_point_light_t* point_lights;
} scene_t;

typedef struct lighting_gpu_scene_t
{
    fa_vulkan_buffer_t global_point_light_buffer[MAX_SWAPCHAIN_IMAGES];
} lighting_gpu_scene_t;

typedef struct renderer_t
{
    scene_t scene;
    fa_gpu_scene_t gpu_scene;
    lighting_gpu_scene_t lighting_gpu_scene;

    uint32_t frame_index;
} renderer_t;

static fa_dynamic_mesh_component_t null_mesh = { 0 };
static renderer_t* renderer;

// ------------------------------------------------------------------------------------------------------------------------------------------

static void frame_begin(void)
{
    FA_PROFILER_ZONE(ctx, true);

    renderer->frame_index = fa_vulkan_backend_api->begin_frame();

    FA_PROFILER_ZONE_END(ctx);
}

static inline uint32_t find_material_index(const fa_material_t* material)
{
    return (uint32_t)(material != NULL && fa_sbuff_end(renderer->scene.materials) >= material ? material - renderer->scene.materials : 0);
}

typedef struct prepare_mesh_job_data_t
{
    const fa_dynamic_mesh_component_t* mesh;
    const fa_transform_component_t* transform;
    uint32_t mesh_index;
    uint32_t vertex_offset;
    uint32_t index_offset;
} prepare_mesh_job_data_t;

static void prepare_mesh_job(void* data)
{
    FA_PROFILER_ZONE(ctx, true);
    const prepare_mesh_job_data_t* job_data = data;
    const fa_dynamic_mesh_component_t* mesh = job_data->mesh;
    const uint32_t mesh_index = job_data->mesh_index;

    memcpy(renderer->scene.vertices + job_data->vertex_offset, mesh->vertices, fa_sbuff_num(mesh->vertices) * sizeof(*mesh->vertices));
    memcpy(renderer->scene.indices + job_data->index_offset, mesh->indices, fa_sbuff_num(mesh->indices) * sizeof(*mesh->indices));

    fa_draw_data_t draw_data = { 0 };
    draw_data.material_index = find_material_index(mesh->material);
    *(renderer->scene.draw_data + mesh_index) = draw_data;

    VkDrawIndexedIndirectCommand draw_cmd;
    draw_cmd.firstIndex = job_data->index_offset;
    draw_cmd.indexCount = (uint32_t)fa_sbuff_num(mesh->indices);
    draw_cmd.vertexOffset = job_data->vertex_offset;
    draw_cmd.firstInstance = 0;
    draw_cmd.instanceCount = 1;
    *(renderer->scene.draw_command_data + mesh_index) = draw_cmd;
    renderer->scene.transforms[mesh_index] = job_data->transform->mat;
    FA_PROFILER_ZONE_END(ctx);
}

static void prepare_lights(void)
{
    // using a trick to store the number of point lights in the first component of the first point lights x positions
    // since this is a dead entry anyways
    renderer->scene.point_lights[0].pos.x = (float)fa_sbuff_num(renderer->scene.point_lights) - 1;
}

static void prepare_dynamic_meshes(void)
{
    FA_PROFILER_ZONE(ctx, true);
    FA_CREATE_TEMP_ALLOC(temp_alloc);

    // reset the per-scene buffers.
    // #todo [roey]: these should really be cached and data should persist between frames unless changed.
    scene_t* scene = &renderer->scene;
    fa_sbuff_resize(scene->vertices, 0);
    fa_sbuff_resize(scene->indices, 0);
    fa_sbuff_resize(scene->draw_data, 0);
    fa_sbuff_resize(scene->draw_command_data, 0);

    fa_entity_context_t* entity_ctx = fa_application_api->get_active_entity_context();
    const fa_component_type_t mesh_comp_type = fa_entity_api->component_type_get(FA_DYNAMIC_MESH_COMPONENT_NAME);
    const fa_component_type_t transform_comp_type = fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME);
    const fa_entity_filter_t entity_filter = fa_entity_api->entity_filter_create(entity_ctx, (fa_entity_filter_desc_t){ .type_ids = { mesh_comp_type, transform_comp_type } });

    const uint32_t mesh_count = (uint32_t)fa_sbuff_num(entity_filter.entities);
    fa_job_decl_t* job_decls = fa_temp_alloc(temp_alloc, mesh_count * sizeof(fa_job_decl_t));
    memset(job_decls, 0, mesh_count * sizeof(fa_job_decl_t));
    prepare_mesh_job_data_t* job_data = fa_temp_alloc(temp_alloc, mesh_count * sizeof(prepare_mesh_job_data_t));

    uint32_t num_vertices = 0;
    uint32_t num_indices = 0;
    for (uint32_t i = 0; i < mesh_count; ++i)
    {
        job_decls[i].func = prepare_mesh_job;

        job_data[i].mesh_index = i;
        job_data[i].mesh = fa_entity_api->component_get(entity_ctx, entity_filter.entities[i], mesh_comp_type);
        job_data[i].transform = fa_entity_api->component_get(entity_ctx, entity_filter.entities[i], transform_comp_type);
        job_data[i].vertex_offset = num_vertices;
        job_data[i].index_offset = num_indices;
        num_vertices += (uint32_t)fa_sbuff_num(job_data[i].mesh->vertices);
        num_indices += (uint32_t)fa_sbuff_num(job_data[i].mesh->indices);

        job_decls[i].user_data = job_data + i;
    }
    fa_sbuff_resize(renderer->scene.draw_data, mesh_count);
    fa_sbuff_resize(renderer->scene.draw_command_data, mesh_count);
    fa_sbuff_resize(renderer->scene.vertices, num_vertices);
    fa_sbuff_resize(renderer->scene.indices, num_indices);
    fa_sbuff_resize(renderer->scene.transforms, mesh_count);

#if defined(FA_OS_WINDOWS)
    fa_atomic_count_t* count = fa_job_scheduler_api->post_jobs(job_decls, mesh_count);
    fa_job_scheduler_api->wait_on_count_and_free(count);
#else
    for (uint32_t i = 0; i < mesh_count; ++i)
    {
        job_decls[i].func(job_decls[i].user_data);
    }
#endif

    FA_DESTROY_TEMP_ALLOC(temp_alloc);
    FA_PROFILER_ZONE_END(ctx);
}

static void upload_dynamic_mesh_data_to_gpu_scene(const scene_t* scene, fa_gpu_scene_t* gpu_scene, fa_vk_buffer_write_desc_t** out_buffer_writes)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint32_t frame_index = renderer->frame_index;

    const size_t num_transforms = fa_sbuff_num(scene->transforms);
    const size_t num_vertices = fa_sbuff_num(scene->vertices);
    const size_t num_indices = fa_sbuff_num(scene->indices);
    const size_t num_draw_datas = fa_sbuff_num(scene->draw_command_data);

    if (gpu_scene->global_transform_buffer[frame_index].size < (num_transforms * sizeof(*scene->transforms)))
    {
        fa_vulkan_backend_api->buffer_realloc(gpu_scene->global_transform_buffer + frame_index, num_transforms * sizeof(*scene->transforms));
    }
    if (gpu_scene->global_vertex_buffer[frame_index].size < (num_vertices * sizeof(*scene->vertices)))
    {
        fa_vulkan_backend_api->buffer_realloc(gpu_scene->global_vertex_buffer + frame_index, num_vertices * sizeof(*scene->vertices));
    }
    if (gpu_scene->global_index_buffer[frame_index].size < (num_indices * sizeof(*scene->indices)))
    {
        fa_vulkan_backend_api->buffer_realloc(gpu_scene->global_index_buffer + frame_index, num_indices * sizeof(*scene->indices));
    }
    if (gpu_scene->global_material_buffer[frame_index].size < fa_sbuff_size(scene->materials))
    {
        fa_vulkan_backend_api->buffer_realloc(gpu_scene->global_material_buffer + frame_index, fa_sbuff_size(scene->materials));
    }
    if (gpu_scene->global_draw_data_buffer[frame_index].size < fa_sbuff_size(scene->materials))
    {
        fa_vulkan_backend_api->buffer_realloc(gpu_scene->global_draw_data_buffer + frame_index, fa_sbuff_size(scene->draw_data));
    }
    if (gpu_scene->command_draw_data_buffer[frame_index].size < (num_draw_datas * sizeof(*scene->draw_command_data)))
    {
        fa_vulkan_backend_api->buffer_realloc(gpu_scene->command_draw_data_buffer + frame_index, num_draw_datas * sizeof(*scene->draw_command_data));
    }

    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->view_info_buffer[frame_index], &scene->view_info, 0, sizeof(scene->view_info) }));

    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->global_transform_buffer[frame_index], scene->transforms, 0, fa_sbuff_size(scene->transforms) }));
    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->global_vertex_buffer[frame_index], scene->vertices, 0, fa_sbuff_size(scene->vertices) }));
    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->global_index_buffer[frame_index], scene->indices, 0, fa_sbuff_size(scene->indices) }));
    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->global_material_buffer[frame_index], scene->materials, 0, fa_sbuff_size(scene->materials) }));
    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->global_draw_data_buffer[frame_index], scene->draw_data, 0, fa_sbuff_size(scene->draw_data) }));
    fa_sbuff_push(*out_buffer_writes, ((fa_vk_buffer_write_desc_t){ gpu_scene->command_draw_data_buffer[frame_index], scene->draw_command_data, 0, fa_sbuff_size(scene->draw_command_data) }));

    FA_PROFILER_ZONE_END(ctx);
}

static void write_scene_descriptors(const scene_t* scene, const fa_gpu_scene_t* gpu_scene, const lighting_gpu_scene_t* lighting_gpu_scene)
{
    const uint32_t frame_index = renderer->frame_index;

    VkDescriptorSet descriptor_set = fa_vulkan_backend_api->descriptor_get_bindless_set();

    VkDescriptorBufferInfo view_info_desc = {
        .buffer = gpu_scene->view_info_buffer[frame_index].buffer,
        .offset = 0,
        .range = sizeof(fa_view_info_t),
    };

    const VkWriteDescriptorSet view_info_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = descriptor_set,
        .dstBinding = 4,
        .dstArrayElement = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .pBufferInfo = &view_info_desc,
        .pImageInfo = NULL,
        .pTexelBufferView = NULL,
    };

    VkDescriptorBufferInfo transform_info = {
        .buffer = gpu_scene->global_transform_buffer[frame_index].buffer,
        .offset = 0,
        .range = fa_sbuff_size(scene->transforms),
    };

    const VkWriteDescriptorSet transform_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = descriptor_set,
        .dstBinding = 0,
        .dstArrayElement = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .pBufferInfo = &transform_info,
        .pImageInfo = NULL,
        .pTexelBufferView = NULL,
    };

    VkDescriptorBufferInfo draw_data_info = {
        .buffer = gpu_scene->global_draw_data_buffer[frame_index].buffer,
        .offset = 0,
        .range = fa_sbuff_size(scene->draw_data),
    };

    const VkWriteDescriptorSet draw_data_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = descriptor_set,
        .dstBinding = 0,
        .dstArrayElement = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .pBufferInfo = &draw_data_info,
        .pImageInfo = NULL,
        .pTexelBufferView = NULL,
    };

    VkDescriptorBufferInfo material_data_info = {
        .buffer = gpu_scene->global_material_buffer[frame_index].buffer,
        .offset = 0,
        .range = fa_sbuff_size(scene->materials),
    };

    const VkWriteDescriptorSet material_data_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = descriptor_set,
        .dstBinding = 0,
        .dstArrayElement = 2,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .pBufferInfo = &material_data_info,
        .pImageInfo = NULL,
        .pTexelBufferView = NULL,
    };

    VkDescriptorBufferInfo point_lights_info = {
        .buffer = lighting_gpu_scene->global_point_light_buffer[frame_index].buffer,
        .offset = 0,
        .range = fa_sbuff_size(scene->point_lights),
    };

    const VkWriteDescriptorSet point_lights_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = descriptor_set,
        .dstBinding = 0,
        .dstArrayElement = 3,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = 1,
        .pBufferInfo = &point_lights_info,
        .pImageInfo = NULL,
        .pTexelBufferView = NULL,
    };

    // #todo [roey]: push textures

    const VkWriteDescriptorSet descriptor_writes[] = { view_info_write, transform_write, draw_data_write, material_data_write, point_lights_write };
    fa_vulkan_backend_api->descriptor_write(FA_ARRAY_COUNT(descriptor_writes), descriptor_writes);
}

static void frame_draw(fa_view_info_t* view_info)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint32_t frame_index = renderer->frame_index;

    scene_t* scene = &renderer->scene;
    fa_gpu_scene_t* gpu_scene = &renderer->gpu_scene;
    lighting_gpu_scene_t* lighting_gpu_scene = &renderer->lighting_gpu_scene;

    scene->view_info = *view_info;

    // Collect all buffer writes and issue them as a single batch
    /* sbuff */ fa_vk_buffer_write_desc_t* buffer_writes = NULL;

    // prepare scene lighting data
    {
        // #todo: cache lighting data for non-dynamic lights
        prepare_lights();
        if (lighting_gpu_scene->global_point_light_buffer[frame_index].size < fa_sbuff_size(scene->point_lights))
        {
            fa_vulkan_backend_api->buffer_realloc(lighting_gpu_scene->global_point_light_buffer + frame_index, fa_sbuff_size(scene->point_lights));
        }
        fa_sbuff_push(buffer_writes, ((fa_vk_buffer_write_desc_t){ lighting_gpu_scene->global_point_light_buffer[frame_index], scene->point_lights, 0, fa_sbuff_size(scene->point_lights) }));
    }

    // prepare dynamic mesh data
    {
        prepare_dynamic_meshes();
        upload_dynamic_mesh_data_to_gpu_scene(scene, gpu_scene, &buffer_writes);
    }

    fa_vulkan_backend_api->batch_buffer_set_data(buffer_writes);

    const size_t num_draw_data = fa_sbuff_num(scene->draw_data);
    gpu_scene->draw_count = (uint32_t)num_draw_data;

    write_scene_descriptors(scene, gpu_scene, lighting_gpu_scene);

    fa_vulkan_backend_api->submit_scene(&renderer->gpu_scene);

    FA_PROFILER_ZONE_END(ctx);
}

static fa_material_t* material_create(void)
{
    const fa_material_t mat = { 0 };
    return fa_sbuff_push(renderer->scene.materials, mat);
}

static fa_texture_t texture_create(const char* filename)
{
    FA_PROFILER_ZONE(ctx, true);

    int width, height, nchannels;
    stbi_uc* pixel_data = NULL;
    {
        FA_PROFILER_ZONE_N(ctx1, "stbi_load", true);
        pixel_data = stbi_load(filename, &width, &height, &nchannels, STBI_rgb_alpha);
        FA_PROFILER_ZONE_END(ctx1);
    }
    if (!pixel_data)
    {
        FA_LOG_CATEGORY(Error, "failed to load texture file %s", filename);
        FA_PROFILER_ZONE_END(ctx);
        return (fa_texture_t){ 0 };
    }

    const fa_vulkan_texture_t vk_texture = fa_vulkan_backend_api->texture_create(width, height, pixel_data);

    stbi_image_free(pixel_data);

    fa_sbuff_push(renderer->scene.textures, vk_texture);

    FA_PROFILER_ZONE_END(ctx);
    return vk_texture.binding_slot;
}

static fa_point_light_t* light_point_create(void)
{
    const fa_point_light_t point_light = { 0 };
    return fa_sbuff_push(renderer->scene.point_lights, point_light);
}

static void allocate_scene_buffers(void)
{
    fa_gpu_scene_t* gpu_scene = &renderer->gpu_scene;
    lighting_gpu_scene_t* lighting_gpu_scene = &renderer->lighting_gpu_scene;
    for (uint32_t frame_index = 0; frame_index < MAX_SWAPCHAIN_IMAGES; ++frame_index)
    {
        gpu_scene->view_info_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(fa_view_info_t), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "ViewInfoUniform");
        gpu_scene->global_transform_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GTransformBuffer");
        gpu_scene->global_vertex_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GVertexBuffer");
        gpu_scene->global_index_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GIndexBuffer");
        gpu_scene->global_material_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GMaterialBuffer");
        gpu_scene->global_draw_data_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GDrawDataBuffer");
        gpu_scene->command_draw_data_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "CommandDrawData");
        lighting_gpu_scene->global_point_light_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(0, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GPointLightBuffer");
    }
}

static void create(void)
{
    // The first entry of each array is always empty. This allows us to always create and bind the buffers
    // even when there are no meshes/point lights without having to special case for when size is 0.
    fa_sbuff_push(renderer->scene.transforms, GLMS_MAT4_IDENTITY);
    const fa_point_light_t pl = { 0 };
    fa_sbuff_push(renderer->scene.point_lights, pl);
    const fa_material_t mat = { 0 };
    fa_sbuff_push(renderer->scene.materials, mat);

    allocate_scene_buffers();
}

static void destroy(void)
{
    for (uint32_t i = 0; i < MAX_SWAPCHAIN_IMAGES; ++i)
    {
        const fa_gpu_scene_t* gpu_scene = &renderer->gpu_scene;
        const lighting_gpu_scene_t* lighting_gpu_scene = &renderer->lighting_gpu_scene;
        fa_vulkan_backend_api->buffer_free(gpu_scene->view_info_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->global_transform_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->global_vertex_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->global_index_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->global_material_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->global_draw_data_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->command_draw_data_buffer[i]);
        fa_vulkan_backend_api->buffer_free(lighting_gpu_scene->global_point_light_buffer[i]);
    }

    for (uint32_t i = 0; i < fa_sbuff_num(renderer->scene.textures); ++i)
    {
        fa_vulkan_backend_api->texture_destroy(renderer->scene.textures[i]);
    }

    scene_t* scene = &renderer->scene;
    fa_sbuff_free(scene->draw_data);
    fa_sbuff_free(scene->indices);
    fa_sbuff_free(scene->vertices);
    fa_sbuff_free(scene->transforms);
    fa_sbuff_free(scene->point_lights);
    fa_sbuff_free(scene->materials);
    fa_sbuff_free(scene->textures);
}

// ------------------------------------------------------------------------------------------------------------------------------------------

static struct fa_renderer_api renderer_api = {
    .create = create,
    .destroy = destroy,
    .frame_begin = frame_begin,
    .frame_draw = frame_draw,
    .material_create = material_create,
    .texture_create = texture_create,
    .light_point_create = light_point_create,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = fa_get_api(FA_LOGGER_API_NAME);
    fa_allocator_api = fa_get_api(FA_ALLOCATOR_API_NAME);
    fa_vulkan_backend_api = fa_get_api(FA_VULKAN_BACKEND_API_NAME);
    fa_job_scheduler_api = fa_get_api(FA_JOB_API_NAME);
    fa_application_api = fa_get_api(FA_APPLICATION_API_NAME);
    fa_entity_api = fa_get_api(FA_ENTITY_API_NAME);

    fa_add_or_remove_api(&renderer_api, FA_RENDERER_API_NAME, load);

    renderer = (renderer_t*)registry->static_variable(FA_RENDERER_API_NAME, "vulkan_backend", sizeof(renderer_t));

    {
        fa_component_description_t mesh_desc = {
            .name = FA_DYNAMIC_MESH_COMPONENT_NAME,
            .display_name = "Mesh",
            .bytes = sizeof(fa_dynamic_mesh_component_t),
            .prototype_data = NULL,
        };
        fa_entity_api->component_type_register(&mesh_desc);
    }
}
