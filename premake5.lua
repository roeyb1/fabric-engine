-- Make sure incluedirs() also calls sysincludedirs()
builtin_includedirs = includedirs
function includedirs(dirs)
    builtin_includedirs(dirs)
    sysincludedirs(dirs)
end

function copy_to_modules(from)
    return 'XCOPY "' .. '..\\..\\..\\' .. from:gsub("/", "\\") .. '" "..\\..\\..\\bin\\%{cfg.buildcfg}\\modules" /Q/Y/I/C '
end

function link_to_vulkan()
	filter "platforms:Win64"
		links {"$(VULKAN_SDK)/lib/vulkan-1"}
		includedirs{"$(VULKAN_SDK)/include"}
	filter "platforms:MacOSX or Linux"
		links {"vulkan"}
	filter{}
end

function link_to_glfw()
	includedirs{"lib/glfw/include"}
	links{"glfw3"}
	dependson {"glfw"}
end

function link_to_freetype()
	includedirs{"lib/freetype2/include"}
	links{"freetype"}
	dependson {"freetype2"}
end

function snake_case(s)
	return string.gsub(s, "-", "_");
end
function folder(t)
    if type(t) ~= "table" then t = {t} end
    for _,f in ipairs(t) do
        files {f .. "/**.h",  f .. "/**.c", f .. "/**.inl", f .. "/**.cpp" }
    end
end

-- basic fabric project setup
function fabric_project(name)
	local snake_cased_name = snake_case(name) -- snake case name required for #DEFINES
    project(name)
        language "C++"
		location("build/" .. snake_cased_name)
		targetdir "bin/%{cfg.buildcfg}"
		-- temporarily allow including vulkan everywhere since we are reusing the vulkan structs
        includedirs { "", "vendor/cglm/include", "$(VULKAN_SDK)/include" }
		defines {"FA_LINKS_TO_" .. string.upper(snake_cased_name)}
		-- link to the profiler dll
		dependson {"tracy"}
		includedirs{"vendor/tracy"}
		defines{"TRACY_IMPORTS", "TRACY_FIBERS"}
		links{"tracy"}
end

-- specialize project for modules
function fabric_module(name)
	local snake_cased_name = snake_case(name)
    fabric_project(name)
        location("build/modules/" .. name)
        kind "SharedLib"
        targetdir "bin/%{cfg.buildcfg}/modules"
        targetname("fa_" .. snake_cased_name)
        dependson("core")
        folder {"modules/" .. name}
		includedirs{"vendor/"}
end

function main_dll(t)
	fabric_project(t.name)
        location("build/" .. t.name .. "_dll")
        targetname(t.name)
        kind "SharedLib"
        language "C++"
        targetdir "bin/%{cfg.buildcfg}"
        dependson {"core"}
		dependson {"renderer", "window", "vulkan_backend", "ui", "ui_widgets", "ecs"}
		
		folder{t.dir}
        removefiles {t.dir .. "/main.c"}
end

function main_exe(t)
	fabric_project(t.name)
        location("build/" .. t.name .. "_exe")
        targetname(t.name)
        kind "ConsoleApp"
        language "C++"
        targetdir "bin/%{cfg.buildcfg}"
        dependson {"core"}
		defines {"FA_LINKS_TO_CORE"}
        links {"core"}
		
		files{t.dir .. "/main.c"}
end

		
workspace "Fabric"
    configurations {"Debug", "Release", "Profile"}
    language "C++"
    cppdialect "C++20"
    flags 
	{
		"FatalWarnings",
		"MultiProcessorCompile"
	}
    warnings "Extra"
    inlining "Auto"
    editandcontinue "Off"
	exceptionhandling "Off"
	rtti "Off"
    -- Enable this to test compile with strict aliasing.
    -- strictaliasing "Level3"
    -- Enable this to report build times, see: http://aras-p.info/blog/2019/01/21/Another-cool-MSVC-flag-d1reportTime/
    -- buildoptions { "/Bt+", "/d2cgsummary", "/d1reportTime" }
	debugdir "bin/%{cfg.buildcfg}"
	startproject "fabric_editor"
    libdirs{"./lib/**"}

	
filter { "system:windows" }
    platforms { "Win64" }

filter { "system:macosx" }
    platforms { "MacOSX" }

filter {"system:linux"}
    platforms { "Linux" }
	
-- Windows platform settings
filter { "platforms:Win64" }
    defines { "FA_OS_WINDOWS", "_CRT_SECURE_NO_WARNINGS" }
    staticruntime "On"
    architecture "x64"
    buildoptions 
	{
		--"/we4121", -- Padding was added to align structure member on boundary. #todo
        --"/we4820", -- Padding was added to end of struct. #todo
        "/utf-8",	-- Source encoding is UTF-8.
    }
    disablewarnings 
	{
        "4100", -- Unused formal parameter.
		"4152", -- Conversion from function pointer to void *.
		"4201", -- Nameless struct/union.
		"4706", -- assignment within conditional
		"4206", -- translation unit is empty. May be #ifdef'd out
    }
    defines {'_ITERATOR_DEBUG_LEVEL=0'}

-- MacOS platform settings
filter { "platforms:MacOSX" }
    defines { "FA_OS_MACOSX", "FA_OS_POSIX" }
    architecture "x64"
    buildoptions
	{
        "-fms-extensions",
        "-mavx",
        "-mfma",
    }
    enablewarnings 
	{
		"shadow",
		--"padded", #todo
    }
    disablewarnings
	{
		"unused-parameter",
		"unused-function",
		"missing-field-initializers",
		"missing-braces",
    }

filter {"platforms:Linux"}
    defines { "FA_OS_LINUX", "FA_OS_POSIX" }
    architecture "x64"
    toolset "clang"
    buildoptions
	{
    }
    enablewarnings
	{
    }
    disablewarnings
	{
		"unused-parameter",
		"unused-function",
		"missing-field-initializers",
		"missing-braces",
    }

filter "configurations:Debug"
    defines { "FA_DEBUG", "DEBUG" }
    symbols "On"
	--optimize "On"

filter "configurations:Release"
    defines { "FA_RELEASE", "NDEBUG" }
    optimize "On"

filter "configurations:Profile"
	defines { "FA_RELEASE", "NDEBUG", "FA_PROFILE", "TRACY_ENABLE"}
	symbols "On"
	optimize "On"
filter {}

group "vendor"

	 project "tracy"
        language "C++"
        location("build/vendor/tracy")
        kind "SharedLib"
        targetdir "bin/%{cfg.buildcfg}"
        targetname("tracy")
		defines {"TRACY_EXPORTS", "TRACY_FIBERS"}
        files
		{
			"vendor/tracy/TracyClient.cpp",
			"vendor/tracy/TracyC.h",
			"vendor/tracy/Tracy.hpp",
		}
		
		filter "system:windows"
			disablewarnings
			{
				"4244", -- 'conversion' conversion from 'type1' to 'type2', possible loss of data.
				"4505", -- unreferenced function with internal linkage has been removed
			}
			
		filter "system:macosx or linux"
			disablewarnings
			{
				"shadow",
			}
		
		filter "system:linux"
			linkoptions {"-ldl", "-pthread" }
		filter {}


group "modules"
    project "cimgui"
        language "C++"
        cppdialect "C++17"
        location("build/vendor/cimgui")
        kind "SharedLib"
        targetname "cimgui"
        targetdir "bin/%{cfg.buildcfg}/modules"
        link_to_vulkan()
        link_to_glfw()
        link_to_freetype()

        includedirs
        {
             "vendor/freetype2/include",
             "vendor/cimgui/imgui",
        }
        links { "freetype2"}
        defines{"CIMGUI_SHOULD_EXPORT", "CIMGUI_FREETYPE", "IMGUI_ENABLE_FREETYPE"}
        files
        {
             "vendor/cimgui/cimgui.cpp",
             "vendor/cimgui/cimgui.h",
             "vendor/cimgui/cimguizmo.cpp",
             "vendor/cimgui/cimguizmo.h",
             "vendor/cimgui/imgui/imgui.cpp",
             "vendor/cimgui/imgui/imgui_tables.cpp",
             "vendor/cimgui/imgui/imgui_widgets.cpp",
             "vendor/cimgui/imgui/imgui_draw.cpp",
             "vendor/cimgui/imgui/imgui_demo.cpp",
             "vendor/cimgui/imgui/misc/freetype/imgui_freetype.cpp",
             "vendor/cimgui/imgui/imgui.h",
             "vendor/cimgui/imgui/imgui_internal.h",
             "vendor/cimgui/imgui/misc/freetype/imgui_freetype.h",
             "vendor/cimgui/imgui/imconfig.h",
             "vendor/cimgui/imgui/backends/imgui_impl_vulkan.cpp",
             "vendor/cimgui/imgui/backends/imgui_impl_vulkan.h",
             "vendor/cimgui/imgui/backends/imgui_impl_glfw.cpp",
             "vendor/cimgui/imgui/backends/imgui_impl_glfw.h",
             "vendor/cimgui/imguizmo/**.cpp",
             "vendor/cimgui/imguizmo/**.h",
        }

    fabric_module "glfw"
         kind("Utility")
         postbuildcommands { copy_to_modules("lib/glfw/lib/*.dll") }

    fabric_module "freetype2"
         kind("Utility")
         postbuildcommands { copy_to_modules("lib/freetype2/lib/*.dll") }

    fabric_module "window"
         link_to_glfw()
         
    fabric_module "vulkan_backend"
         link_to_glfw()
         link_to_vulkan()
         links {"cimgui"}
             
    fabric_module "renderer"
         files{"content/shaders/*.vert", "content/shaders/*.frag", "content/shaders/*.h"}

    fabric_module "asset_import"

    fabric_module "asset_cache"
    
    fabric_module "entity_core"
    
    fabric_module "editor_panels"
         links {"cimgui"}

    fabric_module "ui"
         links{"cimgui"}
         
    fabric_module "ui_widgets"
         links{"cimgui"}

group "samples"
	main_dll {name="simple-3d-dll", dir="samples/simple-3d"}
        links{"cimgui"}
	main_exe {name="simple-3d", dir="samples/simple-3d"}

group ""
	fabric_project "core"
		kind "StaticLib"
		folder{"core"}
		
	main_exe{name="unit_test", dir="unit_test"}

	main_dll{name="fabric-editor-dll", dir="fabric_editor"}
        links{"cimgui"}
		
	main_exe{name="fabric-editor", dir="fabric_editor"}
		filter { "platforms:Win64" }
			postbuildcommands
			{
				'{MKDIR} ../../bin/%{cfg.buildcfg}/content/',
				'{COPY} ../../content/ ../../bin/%{cfg.buildcfg}/content/'
			}
		filter { "platforms:MacOSX or Linux" }
			postbuildcommands
			{
				'{MKDIR} ../../bin/%{cfg.buildcfg}/content/',
				'{COPY} ../../content/ ../../bin/%{cfg.buildcfg}/'
			}
		filter {}

		project "scripts"
			kind "SharedItems"
			files {"scripts/**", "premake5.lua"}
