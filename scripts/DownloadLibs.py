import os
import subprocess
import shutil
from pathlib import Path

import Utils

class GlfwConfig:

    platformName = ""
    glfwVersion = "3.3.6"
    
    @classmethod
    def Install(cls):

        archiveType = ""
        platformAlias = ""
        if cls.platformName == "windows":
            archiveType = "zip"
            platformAlias = "WIN64"
        else:
            archiveType = "tar.gz"

        glfwExists = Path(f"lib/glfw-{cls.glfwVersion}.bin.{platformAlias}.{archiveType}").exists();
        if glfwExists:
            return True

        cls.glfwZipUrl= f"https://github.com/glfw/glfw/releases/download/{cls.glfwVersion}/glfw-{cls.glfwVersion}.bin.{platformAlias}.{archiveType}"
        glfwZipFile = f"lib/glfw-{cls.glfwVersion}.bin.{platformAlias}.{archiveType}"

        print("Downloading {0:s} to {1:s}".format(cls.glfwZipUrl, glfwZipFile))
        Utils.DownloadFile(cls.glfwZipUrl, glfwZipFile)
        print("Extracting", glfwZipFile)
        if cls.platformName == "windows":
            Utils.UnzipFile(glfwZipFile, deleteZipFile=False)
        else:
            subprocess.call(["tar", "xzf", f"{glfwZipFile}"])
            os.remove(f"{premakeFile}")


        os.rename(f"lib/glfw-{cls.glfwVersion}.bin.{platformAlias}", "lib/glfw")
        os.rename("lib/glfw/lib-vc2022", "lib/glfw/lib")
        os.remove("lib/glfw/lib/glfw3.lib")
        os.remove("lib/glfw/lib/glfw3_mt.lib")
        os.rename("lib/glfw/lib/glfw3dll.lib", "lib/glfw/lib/glfw3.lib")
        shutil.rmtree("lib/glfw/lib-vc2012")
        shutil.rmtree("lib/glfw/lib-vc2013")
        shutil.rmtree("lib/glfw/lib-vc2015")
        shutil.rmtree("lib/glfw/lib-vc2017")
        shutil.rmtree("lib/glfw/lib-vc2019")
        shutil.rmtree("lib/glfw/lib-static-ucrt")
        shutil.rmtree("lib/glfw/lib-mingw-w64")
        shutil.rmtree("lib/glfw/docs")

        print(f"GLFW {cls.glfwVersion} has been installed to lib/glfw")

        return True

class Freetype2Config:

    platformName = ""
    version = "2.11.1"
    
    @classmethod
    def Install(cls):

        archiveType = ""
        platformAlias = ""
        if cls.platformName == "windows":
            archiveType = "zip"
        else:
            archiveType = "tar.gz"

        zipFile = f"lib/freetype.v{cls.version}.{archiveType}"
        zipExists = Path(zipFile).exists();
        if zipExists:
            return True

        if cls.platformName == "windows":
            cls.zipUrl= f"https://github.com/ubawurinna/freetype-windows-binaries/archive/refs/tags/v{cls.version}.{archiveType}"

            print("Downloading {0:s} to {1:s}".format(cls.zipUrl, zipFile))
            Utils.DownloadFile(cls.zipUrl, zipFile)
            print("Extracting", zipFile)
            if cls.platformName == "windows":
                Utils.UnzipFile(zipFile, deleteZipFile=False)
            else:
                subprocess.call(["tar", "xzf", f"{zipFile}"])
                os.remove(f"{premakeFile}")


            os.rename(f"lib/freetype-windows-binaries-{cls.version}/", "lib/freetype2/")
            os.rename(f"lib/freetype2/release dll/win64", "lib/freetype2/lib")
            shutil.rmtree("lib/freetype2/demos")
            shutil.rmtree("lib/freetype2/release dll")
            shutil.rmtree("lib/freetype2/release static")
            os.remove("lib/freetype2/ChangeLog.txt")
            os.remove("lib/freetype2/FTL.TXT")
            os.remove("lib/freetype2/GPLv2.TXT")
            os.remove("lib/freetype2/README.md")

        print(f"Freetype v{cls.version} has been installed to lib/freetype")

        return True
