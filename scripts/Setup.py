import os
import subprocess
import platform

from SetupPremake import PremakeConfig 
from DownloadLibs import *

platformName = platform.system().lower()
if platformName == "darwin":
    platformName = "macosx"

PremakeConfig.platformName = platformName
premakeInstalled = PremakeConfig.Install()

# Go to root directory
os.chdir("./../") 

GlfwConfig.platformName = platformName
glfwInstalled = GlfwConfig.Install()

Freetype2Config.platformName = platformName
freetypeInstalled = Freetype2Config.Install()

if (premakeInstalled and glfwInstalled and freetypeInstalled):
    print("\nRunning Premake...")
    if platformName == "windows":
        subprocess.call([os.path.abspath("./scripts/GenerateProjects.bat"), "nopause"])
    else:
        subprocess.call([os.path.abspath("./scripts/GenerateProjects.sh")])
else:
    print("Failed to complete setup. Premake not present")

