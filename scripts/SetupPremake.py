import os
import subprocess
from pathlib import Path

import Utils

class PremakeConfig:
    platformName = ""
    premakeVersion = "5.0.0-beta1"
    premakeLicenseUrl = "https://raw.githubusercontent.com/premake/premake-core/master/LICENSE.txt"
    premakeDirectory = "."
    
    @classmethod
    def Install(cls):
        # todo: fix for linux and mac as well
        premakeExeName = ""
        if cls.platformName == "windows":
            premakeExeName = "premake5.exe"
        else:
            premakeExeName = "premake5"
        premakeExists = Path(f"{cls.premakeDirectory}/{premakeExeName}").exists();
        if premakeExists:
            return True

        premakeArchiveType = ""
        if cls.platformName == "windows":
            premakeArchiveType = "zip"
        else:
            premakeArchiveType = "tar.gz"

        cls.premakeZipUrl = f"https://github.com/premake/premake-core/releases/download/v{cls.premakeVersion}/premake-{cls.premakeVersion}-{cls.platformName}.{premakeArchiveType}"
        premakeFile = f"{cls.premakeDirectory}/premake-{cls.premakeVersion}-{cls.platformName}.{premakeArchiveType}"
        print("Downloading {0:s} to {1:s}".format(cls.premakeZipUrl, premakeFile))
        Utils.DownloadFile(cls.premakeZipUrl, premakeFile)
        print("Extracting", premakeFile)
        if cls.platformName == "windows":
            Utils.UnzipFile(premakeFile, deleteZipFile=True)
        else:
            subprocess.call(["tar", "xzf", f"{premakeFile}"])
            os.remove(f"{premakeFile}")
        print(f"Premake {cls.premakeVersion} has been downloaded to '{cls.premakeDirectory}'")

        premakeLicensePath = f"{cls.premakeDirectory}/LICENSE.txt"
        print("Downloading {0:s} to {1:s}".format(cls.premakeLicenseUrl, premakeLicensePath))
        Utils.DownloadFile(cls.premakeLicenseUrl, premakeLicensePath)
        print(f"Premake License file has been downloaded to '{cls.premakeDirectory}'")
    
        return True