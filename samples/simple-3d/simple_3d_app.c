#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>

#include <modules/entity_core/camera_component.h>
#include <modules/entity_core/entity_api.h>
#include <modules/entity_core/transform_component.h>
#include <modules/renderer/mesh_component.h>
#include <modules/ui/ui.h>
#include <modules/window/window_api.h>

#include <cglm/struct.h>
#include <string.h>
#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>
#include <vulkan/vulkan.h>

#include <modules/asset_cache/asset_cache.h>
#include <modules/asset_import/asset_import.h>
#include <modules/renderer/renderer.h>
#include <modules/ui/style_colors.inl>
#include <modules/ui_widgets/ui_widgets.h>
#include <modules/vulkan_backend/vulkan_backend.h>

// #todo
static struct fa_logger_api* fa_logger_api;
static struct fa_os_api* fa_os_api;
static struct fa_window_api* fa_window_api;
static struct fa_vulkan_backend_api* fa_vulkan_backend_api;
static struct fa_renderer_api* fa_renderer_api;
static struct fa_ui_api* fa_ui_api;
static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_asset_cache_api* fa_asset_cache_api;
static struct fa_entity_api* fa_entity_api;

// Uncomment this line to enable conservative viewport resizing (only resizes the framebuffer when
// the manual resize operation finished
#define DISABLE_CONSERVATIVE_RESIZE 1

typedef struct fa_application_t
{
    // #todo: multiple windows
    /** Store a handle to the main editor window */
    fa_window_t window_handle;

    fa_render_backend_t* render_backend;

    fa_clock_t clock;
    double time;
    double delta_time;

    fa_entity_context_t* entity_ctx;
    fa_entity_t main_camera_entity;
} fa_application_t;

static fa_application_t* application;

static bool scene_color_needs_resize = false;
static vec2s scene_framebuffer_size = { { 1280, 720 } };

static void draw_scene_viewport(void)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint64_t scene_color_descriptor = fa_vulkan_backend_api->get_scene_color_descriptor();

    igBegin(ICON_FA_HASHTAG " Scene", NULL, 0);

    ImVec2 current_scene_panel_size;
    igGetContentRegionAvail(&current_scene_panel_size);

    // Resize when our framebuffer size is not equal to the current framebuffer size.
    // Wait until the mouse is released before triggered the resize so we don't end up
    // resizing every frame along the way.
    if ((current_scene_panel_size.x > 0 && current_scene_panel_size.y > 0) &&
        (scene_framebuffer_size.x != current_scene_panel_size.x ||
         scene_framebuffer_size.y != current_scene_panel_size.y))
    {
        if (FA_IS_DEFINED(DISABLE_CONSERVATIVE_RESIZE) || !igIsMouseDown(ImGuiMouseButton_Left))
        {
            scene_color_needs_resize = true;
            scene_framebuffer_size.x = current_scene_panel_size.x;
            scene_framebuffer_size.y = current_scene_panel_size.y;
        }
    }

    igImage((ImTextureID)scene_color_descriptor,
            current_scene_panel_size,
            (ImVec2){ 0.f, 0.f },
            (ImVec2){ 1.f, 1.f },
            (ImVec4){ 1.f, 1.f, 1.f, 1.f },
            (ImVec4){ 0.f, 0.f, 0.f, 0.f });

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

#pragma region Application Api Implementation

static bool app_tick(fa_application_t* app)
{
    const fa_window_t window = app->window_handle;

    // Update frame time and delta time
    const fa_clock_t now = fa_os_api->time->now();
    const double delta_time = fa_os_api->time->time_sub(now, app->clock);

    app->clock = now;
    app->delta_time = delta_time;
    app->time += delta_time;

    fa_window_api->update_window(window);
    fa_renderer_api->frame_begin();

    const fa_component_type_t camera_type = fa_entity_api->component_type_get(FA_CAMERA_COMPONENT_NAME);
    fa_camera_component_t* camera_component = fa_entity_api->component_get(app->entity_ctx, app->main_camera_entity, camera_type);

    {
        FA_PROFILER_ZONE_N(ctx1, "draw ui", true);
        draw_scene_viewport();
        FA_PROFILER_ZONE_END(ctx1);
    }

    fa_view_info_t view_info;
    view_info.camera_pos = camera_component->pos;
    view_info.camera_proj = camera_component->proj;
    view_info.camera_view = camera_component->view;
    view_info.near_plane = camera_component->near_plane;
    view_info.far_plane = camera_component->far_plane;
    fa_renderer_api->frame_draw(&view_info);

    if (scene_color_needs_resize)
    {
        fa_vulkan_backend_api->resize_scene_color_framebuffer((uint32_t)scene_framebuffer_size.x, (uint32_t)scene_framebuffer_size.y);
        scene_color_needs_resize = false;
    }

    return !fa_window_api->window_wants_close(window);
}

static void app_destroy(fa_application_t* app)
{
    fa_window_api->destroy_window(app->window_handle);
}

static fa_application_t* app_create(void)
{
    FA_PROFILER_ZONE(ctx, true);

    application->entity_ctx = fa_entity_api->context_create("Editor");

    {
        application->main_camera_entity = fa_entity_api->entity_create(application->entity_ctx, "Editor Camera");
        const fa_component_type_t camera_type = fa_entity_api->component_type_get(FA_CAMERA_COMPONENT_NAME);
        fa_camera_component_t* camera_component = fa_entity_api->component_add(application->entity_ctx, application->main_camera_entity, camera_type);
        camera_component->pos = (vec3s){ -1.f, 2.f, 5.f };
        camera_component->near_plane = 0.001f;
        camera_component->far_plane = 100.f;
        camera_component->view = glms_lookat(camera_component->pos, (vec3s){ 0.f, 0.f, 0.f }, (vec3s){ 0.f, 1.f, 0.f });
        camera_component->proj = glms_perspective(glm_rad(45.f), scene_framebuffer_size.x / scene_framebuffer_size.y, camera_component->near_plane, camera_component->far_plane);
    }

    const fa_entity_t sphere_entity = fa_entity_api->entity_create(application->entity_ctx, "Sphere");
    fa_dynamic_mesh_component_t* sphere_mesh = fa_entity_api->component_add(application->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_DYNAMIC_MESH_COMPONENT_NAME));
    fa_transform_component_t* transform = fa_entity_api->component_add(application->entity_ctx, sphere_entity, fa_entity_api->component_type_get(FA_TRANSFORM_COMPONENT_NAME));

    const fa_asset_handle_t sphere_mesh_handle = fa_asset_cache_api->load_asset("content/models/sphere.obj", FA_ASSET_TYPE_MESH);
    memcpy(sphere_mesh, fa_asset_cache_api->get_asset_data(sphere_mesh_handle), sizeof(fa_dynamic_mesh_component_t));

    fa_material_t* red_plastic = fa_renderer_api->material_create();
    sphere_mesh->material = red_plastic;
    red_plastic->albedo = fa_renderer_api->texture_create("content/textures/red-plastic/albedo.png");
    red_plastic->metallic = fa_renderer_api->texture_create("content/textures/red-plastic/metalness.png");
    red_plastic->roughness = fa_renderer_api->texture_create("content/textures/red-plastic/roughness.png");
    red_plastic->normal = fa_renderer_api->texture_create("content/textures/red-plastic/normal.png");
    red_plastic->ao = fa_renderer_api->texture_create("content/textures/white.png");

    transform->mat = GLMS_MAT4_IDENTITY;
    transform->mat = glms_rotate(transform->mat, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
    transform->mat = glms_translate(transform->mat, (vec3s){ 0.f, 0.f, 0.f });
    sphere_mesh->material = red_plastic;

    // fa_material_t* rustediron_material = fa_renderer_api->material_create();
    // rustediron_material->albedo = fa_renderer_api->texture_create("content/textures/rustediron/albedo.png");
    // rustediron_material->metallic = fa_renderer_api->texture_create("content/textures/rustediron/metalness.png");
    // rustediron_material->roughness = fa_renderer_api->texture_create("content/textures/rustediron/roughness.png");
    // rustediron_material->normal = fa_renderer_api->texture_create("content/textures/rustediron/normal.png");
    // rustediron_material->ao = fa_renderer_api->texture_create("content/textures/white.png");

    // fa_material_t* red_plastic = fa_renderer_api->material_create();
    // red_plastic->albedo = fa_renderer_api->texture_create("content/textures/red-plastic/albedo.png");
    // red_plastic->metallic = fa_renderer_api->texture_create("content/textures/red-plastic/metalness.png");
    // red_plastic->roughness = fa_renderer_api->texture_create("content/textures/red-plastic/roughness.png");
    // red_plastic->normal = fa_renderer_api->texture_create("content/textures/red-plastic/normal.png");
    // red_plastic->ao = fa_renderer_api->texture_create("content/textures/white.png");

    // fa_material_t* bamboo_wood = fa_renderer_api->material_create();
    // bamboo_wood->albedo = fa_renderer_api->texture_create("content/textures/bamboo-wood/albedo.png");
    // bamboo_wood->metallic = fa_renderer_api->texture_create("content/textures/bamboo-wood/metalness.png");
    // bamboo_wood->roughness = fa_renderer_api->texture_create("content/textures/bamboo-wood/roughness.png");
    // bamboo_wood->normal = fa_renderer_api->texture_create("content/textures/bamboo-wood/normal.png");
    // bamboo_wood->ao = fa_renderer_api->texture_create("content/textures/bamboo-wood/ao.png");
    {
        // sphere_mesh->transform = glms_rotate(sphere_mesh->transform, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
        // sphere_mesh->transform = glms_translate(sphere_mesh->transform, (vec3s){ 0.0f, 0.f, 0.f });
        // sphere_mesh->material = rustediron_material;
        //  fa_mesh_t* sphere_mesh2 = fa_renderer_api->mesh_create(sphere_vertices, sphere_indices);
        //  sphere_mesh2->transform = glms_rotate(sphere_mesh2->transform, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
        //  sphere_mesh2->transform = glms_translate(sphere_mesh2->transform, (vec3s){ 2.5f, 0.f, 0.f });
        //  sphere_mesh2->material = red_plastic;
        //  fa_mesh_t* sphere_mesh3 = fa_renderer_api->mesh_create(sphere_vertices, sphere_indices);
        //  sphere_mesh3->transform = glms_rotate(sphere_mesh3->transform, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
        //  sphere_mesh3->transform = glms_translate(sphere_mesh3->transform, (vec3s){ -2.5f, 0.f, 0.f });
        //  sphere_mesh3->material = bamboo_wood;
    }

    {
        fa_point_light_t* pl = fa_renderer_api->light_point_create();
        pl->pos = (vec3s){ -5.f, 4.f, 1.f };
        pl->color = (vec3s){ 200.f, 200.f, 200.f };

        fa_point_light_t* pl2 = fa_renderer_api->light_point_create();
        pl2->pos = (vec3s){ 0.f, 0.f, 4.f };
        pl2->color = (vec3s){ 150.f, 150.f, 150.f };
    }

    application->window_handle = fa_window_api->get_window();

    FA_PROFILER_ZONE_END(ctx);
    return (fa_application_t*)&application;
}

static fa_application_t* app_get(void)
{
    return (fa_application_t*)application;
}

static void app_exit(fa_application_t* app)
{
    fa_ui_api->destroy();

    fa_window_api->close_window(app->window_handle);
}

static fa_entity_context_t* get_entity_context(void)
{
    return application->entity_ctx;
}

#pragma endregion

#pragma region Module

static struct fa_application_api application_api = {
    .create = app_create,
    .get = app_get,
    .destroy = app_destroy,
    .exit = app_exit,
    .tick = app_tick,
    .get_active_entity_context = get_entity_context,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_window_api = registry->get(FA_WINDOW_API_NAME);
    fa_vulkan_backend_api = registry->get(FA_VULKAN_BACKEND_API_NAME);
    fa_renderer_api = registry->get(FA_RENDERER_API_NAME);
    fa_ui_api = registry->get(FA_UI_API_NAME);
    fa_widgets_api = registry->get(FA_UI_WIDGETS_API_NAME);
    fa_os_api = registry->get(FA_OS_API_NAME);
    fa_asset_cache_api = registry->get(FA_ASSET_CACHE_API_NAME);
    fa_entity_api = registry->get(FA_ENTITY_API_NAME);

    fa_add_or_remove_api(&application_api, FA_APPLICATION_API_NAME, load);

    application = registry->static_variable(FA_APPLICATION_API_NAME, "Simple 3D", sizeof(fa_application_t));
}

#pragma endregion