#version 450
#extension GL_EXT_nonuniform_qualifier: require
#extension GL_ARB_shader_draw_parameters: require

layout(set=0,binding=4) uniform view_info_t {
	mat4 view;
	mat4 proj;
	vec3 pos;
} view_info;

vec3 editor_grid_plane[6] = vec3[](
	vec3(1,1,0), vec3(-1,-1,0), vec3(-1,1,0),
	vec3(-1,-1,0), vec3(1,1,0), vec3(1,-1,0)
);

vec3 unproj_point(float x, float y, float z, mat4 view, mat4 proj)
{
	mat4 view_inv = inverse(view);
	mat4 proj_inv = inverse(proj);
	vec4 unproj_point = view_inv * proj_inv * vec4(x, y, z, 1.0);
	return unproj_point.xyz / unproj_point.w;
}

layout(location = 0) out vec3 near_point;
layout(location = 1) out vec3 far_point;

void main()
{
	vec3 point = editor_grid_plane[gl_VertexIndex].xyz;
	
	near_point = unproj_point(point.x, point.y, 0.0, view_info.view, view_info.proj).xyz;
	far_point = unproj_point(point.x, point.y, 1.0, view_info.view, view_info.proj).xyz;

    gl_Position = vec4(point, 1.0);
}