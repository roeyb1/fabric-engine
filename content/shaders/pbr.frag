#version 450
#extension GL_EXT_nonuniform_qualifier: require

#include "common.h"

layout(location = 0) in vec3 frag_world_pos;
layout(location = 1) in vec3 frag_normal;
layout(location = 2) in vec2 frag_uv;
layout(location = 3) in flat uint draw_id;

layout(location = 0) out vec4 out_color;

struct point_light
{
	vec3 pos;
	vec3 color;
};

layout(set = 0, binding = 0, std430) readonly buffer SSBOs0
{ 
	point_light point_lights[];
} ssbos0[];

struct material_data
{
	uint albedo;
	uint metallic;
	uint roughness;
	uint normal;
	uint ao;
};

layout(set = 0, binding = 0, std430) readonly buffer SSBOs1
{ 
	material_data mat_data[];
} ssbos1[];

struct draw_data
{
	uint material_index;
};

layout(set = 0, binding = 0, std430) readonly buffer SSBOs2
{ 
	draw_data dds[];
} ssbos2[];

layout(set = 0, binding = 1) uniform texture2D textures[];
layout(set = 0, binding = 3) uniform sampler samplers[];

layout(set = 0, binding = 4) uniform view_info_t {
	mat4 view;
	mat4 proj;
	vec3 pos;
} view_info;

vec3 get_normal_from_map(uint normal_index)
{
	vec3 tangent_normal = texture(sampler2D(textures[normal_index], samplers[BINDING_GLOBAL_REPEAT_SAMPLER]), frag_uv).rgb;
	
	vec3 q1 = dFdx(frag_world_pos);
	vec3 q2 = dFdy(frag_world_pos);
	vec2 st1 = dFdx(frag_uv);
	vec2 st2 = dFdy(frag_uv);
	
	vec3 n = normalize(frag_normal);
	vec3 t = normalize(q1 * st2.t - q2 * st1.t);
	vec3 b = -normalize(cross(n, t));
	mat3 tbn = mat3(t, b, n);
	
	return normalize(tbn * tangent_normal);
}

vec3 fresnel_schlick(float cos_theta, vec3 f0)
{
	return f0 + (1 - f0) * pow(clamp(1.0 - cos_theta, 0.0, 1.0), 5.0);
}

float distribution_ggx(vec3 n, vec3 h, float roughness)
{
	float a = roughness * roughness;
	float a2 = a * a;
	float n_dot_h = max(dot(n, h), 0.0);
	float n_dot_h2 = n_dot_h * n_dot_h;
	
	float num = a2;
	float denom = (n_dot_h2 * (a2 - 1.0) + 1.0);
	denom = PI * denom * denom;
	
	return num / denom;
}

float geometry_schlick_ggx(float n_dot_v, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r * r) / 8.0;
	
	float num = n_dot_v;
	float denom = n_dot_v * (1.0 - k) + k;
	
	return num / denom;
}

float geometry_smith(vec3 n, vec3 v, vec3 l, float roughness)
{
	float n_dot_v = max(dot(n, v), 0.0);
	float n_dot_l = max(dot(n, l), 0.0);
	float ggx2 = geometry_schlick_ggx(n_dot_v, roughness);
	float ggx1 = geometry_schlick_ggx(n_dot_l, roughness);
	
	return ggx1 * ggx2;
}

void main()
{
	uint num_point_lights = uint(ssbos0[BINDING_GLOBAL_POINT_LIGHTS].point_lights[0].pos.x);

	uint mat_index = ssbos2[BINDING_GLOBAL_DRAW_DATA].dds[draw_id].material_index;
	material_data mat = ssbos1[BINDING_GLOBAL_MATERIAL_DATA].mat_data[mat_index];

	vec3 n = get_normal_from_map(mat.normal);
	vec3 v = normalize(view_info.pos - frag_world_pos);

	vec3 albedo = pow(texture(sampler2D(textures[mat.albedo], samplers[BINDING_GLOBAL_REPEAT_SAMPLER]), frag_uv).rgb, vec3(2.2));
	float metallic = texture(sampler2D(textures[mat.metallic], samplers[BINDING_GLOBAL_REPEAT_SAMPLER]), frag_uv).r;
	float roughness = texture(sampler2D(textures[mat.roughness], samplers[BINDING_GLOBAL_REPEAT_SAMPLER]), frag_uv).r;
	float ao = texture(sampler2D(textures[mat.ao], samplers[BINDING_GLOBAL_REPEAT_SAMPLER]), frag_uv).r;

	vec3 l0 = vec3(0.0);
	
	for (int i = 1; i <= num_point_lights; ++i)
	{
		point_light light = ssbos0[BINDING_GLOBAL_POINT_LIGHTS].point_lights[i];
	
		vec3 l = normalize(light.pos - frag_world_pos);
		vec3 h = normalize(l + v);
		
		float distance = length(light.pos - frag_world_pos);
		float attenuation = 1.0 / (distance * distance);
		vec3 radiance = light.color * attenuation;
		
		vec3 f0 = vec3(0.04);
		f0 = mix(f0, albedo, metallic);
		vec3 f = fresnel_schlick(max(dot(h,v), 0.0), f0);
		
		float ndf = distribution_ggx(n, h, roughness);
		float g = geometry_smith(n, v, l, roughness);
		
		vec3 numerator = ndf * g * f;
		float denominator = 4.0 * max(dot(n, v), 0.0) * max(dot(n, l), 0.0) + 0.0001;
		vec3 specular = numerator / denominator;
		
		vec3 ks = f;
		vec3 kd = vec3(1.0) - ks;
		
		kd *= 1.0 - metallic;
		
		float n_dot_l = max(dot(n, l), 0.0);
		l0 += (kd * albedo / PI + specular) * radiance * n_dot_l;
	}

	vec3 ambient = vec3(0.03) * albedo * ao;
	vec3 color = ambient + l0;

	// gamma correct
	color = color / (color + vec3(1.0));
	color = pow(color, vec3(1.0 / 2.2));

	out_color = vec4(color, 1.0);
}