#version 450
#extension GL_EXT_nonuniform_qualifier: require
#extension GL_ARB_shader_draw_parameters: require

layout(location = 0) in vec3 near_point;
layout(location = 1) in vec3 far_point;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 4) uniform view_info_t {
	mat4 view;
	mat4 proj;
	vec3 pos;
	float near_plane;
	float far_plane;
} view_info;



vec4 grid(vec3 frag_pos_3d, float scale, bool drawAxis) {
    vec2 coord = frag_pos_3d.xz * scale;
    vec2 derivative = fwidth(coord);
    vec2 grid = abs(fract(coord - 0.5) - 0.5) / derivative;
    float line = min(grid.x, grid.y);
    float minimumz = min(derivative.y, 1);
    float minimumx = min(derivative.x, 1);
    vec4 color = vec4(0.2, 0.2, 0.2, 1.0 - min(line, 1.0));
    // z axis
    if(frag_pos_3d.x > -0.1 * minimumx && frag_pos_3d.x < 0.1 * minimumx)
        color.z = 1.0;
    // x axis
    if(frag_pos_3d.z > -0.1 * minimumz && frag_pos_3d.z < 0.1 * minimumz)
        color.x = 1.0;
    return color;
}

float compute_depth(vec3 pos) {
    vec4 clip_space_pos = view_info.proj * view_info.view * vec4(pos.xyz, 1.0);
    return (clip_space_pos.z / clip_space_pos.w);
}
float compute_linear_depth(vec3 pos) {
    vec4 clip_space_pos = view_info.proj * view_info.view * vec4(pos.xyz, 1.0);
    float clip_space_depth = (clip_space_pos.z / clip_space_pos.w) * 2.0 - 1.0; // put back between -1 and 1
    float linear_depth = (2.0 * view_info.near_plane * view_info.far_plane) / (view_info.far_plane + view_info.near_plane - clip_space_depth * (view_info.far_plane - view_info.near_plane)); // get linear value between 0.01 and 100
    return linear_depth / view_info.far_plane; // normalize
}

void main() {
    float t = -near_point.y / (far_point.y - near_point.y);
    vec3 frag_pos_3d = near_point + t * (far_point - near_point);

	gl_FragDepth = compute_depth(frag_pos_3d);

    float linear_depth = compute_linear_depth(frag_pos_3d);
    float fading = max(0, (0.5 - linear_depth));

    outColor = (grid(frag_pos_3d, 10, true) + grid(frag_pos_3d, 1, true))* float(t > 0); // adding multiple resolution for the grid
    outColor.a *= fading;
}