echo Compiling Shaders...
glslc.exe -g shader.vert -o vert.spv
glslc.exe -g pbr.frag -o pbr.frag.spv

glslc.exe -g editor_grid.vert -o editor_grid.vert.spv
glslc.exe -g editor_grid.frag -o editor_grid.frag.spv
pause