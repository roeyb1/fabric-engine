#version 450
#extension GL_EXT_nonuniform_qualifier: require
#extension GL_ARB_shader_draw_parameters: require

#include "global_bindings.h"

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inUV;

layout(location = 0) out vec3 fragPos;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec2 fragUV;
layout(location = 3) out uint draw_id;

layout(set = 0, binding = 0, std430) readonly buffer SSBOs
{ 
	mat4 mats[];
} ssbos[];

layout(set=0,binding=4) uniform view_info_t {
	mat4 view;
	mat4 proj;
	vec3 pos;
} view_info;

void main() {
	mat4 model = ssbos[BINDING_GLOBAL_TRANSFORM].mats[gl_DrawIDARB];
    gl_Position = view_info.proj * view_info.view * model * vec4(inPosition, 1.0);
	fragPos = vec3(model * vec4(inPosition, 1.0));
    fragNormal = mat3(transpose(inverse(model))) * inNormal;
	fragUV = inUV;
	draw_id = gl_DrawIDARB;
}