#pragma once

// if we're on windows, define some equivalents to the <atomics.h> c11 library
#if defined(FA_OS_WINDOWS) && !defined(__clang__)
#include <stdint.h>
#include <windows.h>

typedef uint64_t atomic_uint_least64_t;
typedef uint32_t atomic_uint_least32_t;
typedef char atomic_flag;

static inline uint64_t atomic_fetch_add_uint64_t(volatile atomic_uint_least64_t* atomic, uint64_t val)
{
    return InterlockedExchangeAdd64((volatile LONG64*)atomic, val);
}
static inline uint32_t atomic_fetch_add_uint32_t(volatile atomic_uint_least32_t* atomic, uint32_t val)
{
    return InterlockedExchangeAdd((volatile long*)atomic, val);
}
static inline uint64_t atomic_fetch_sub_uint64_t(volatile atomic_uint_least64_t* atomic, uint64_t val)
{
    return InterlockedExchangeAdd64((volatile LONG64*)atomic, -(int64_t)val);
}
static inline uint32_t atomic_fetch_sub_uint32_t(volatile atomic_uint_least32_t* atomic, uint32_t val)
{
    return InterlockedExchangeAdd((volatile long*)atomic, -(int32_t)val);
}
#else

#include <stdatomic.h>

#define atomic_fetch_add_uint64_t atomic_fetch_add
#define atomic_fetch_add_uint32_t atomic_fetch_add

#define atomic_fetch_sub_uint64_t atomic_fetch_sub
#define atomic_fetch_sub_uint32_t atomic_fetch_sub

#define atomic_exchange_pointer atomic_exchange
#define atomic_compare_exchange_weak_pointer atomic_compare_exchange_weak

#define atomic_compare_exchange_weak_uint32_t atomic_compare_exchange_weak
#define atomic_exchange_uint32_t atomic_exchange

#endif