#pragma once

#include <assert.h>

#if defined(FA_OS_WINDOWS)
#define DEBUG_BREAK() __debugbreak()
#else
#define DEBUG_BREAK()
#endif

#if defined(FA_DEBUG)
/**
 * Checks if a predicate evaluates to false, if so will break in the debugger. Note: only valid for debug builds.
 * If runtime assertion is required use the FA_ASSERT() macro.
 */
#define check(pred)        \
    {                      \
        if (!(pred))       \
        {                  \
            DEBUG_BREAK(); \
        }                  \
    }                      \
    (void)(pred)

#define checkf(pred, message, ...)                                     \
    {                                                                  \
        if (!(pred))                                                   \
        {                                                              \
            FA_LOG(Error, "check failed: %s", message, ##__VA_ARGS__); \
            DEBUG_BREAK();                                             \
        }                                                              \
    }                                                                  \
    (void)(pred)
#else
/**
 * Checks if a predicate evaluates to false, if so will break in the debugger. Note: only valid for debug builds.
 * If runtime assertion is required use the FA_ASSERT() macro.
 */
#define check(pred) (void)(pred)
#define checkf(pred, message, ...) (void)(pred)
#endif // FA_DEBUG

#define FA_ASSERT(pred) assert(pred)
