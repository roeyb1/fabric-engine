#pragma once

#include "core_types.h"

/**
 * The module api allows dynamic loading and linking of shared libraries at any point during the runtime of the app.
 * Simple interface which just requires passing the filename of the dll and will load all required symbols and apis.
 *
 * Future plans:
 *      - Each frame we should be able to poll the status of each module which has hot-reload enabled and hot
 *        reload the dll file, replacing any registered apis with the new api struct.
 */

struct fa_api_registry;

/** Template for any module's `fa_load_module` function. */
typedef void (*fa_load_module_func)(struct fa_api_registry* reg, bool load);

struct fa_module_api
{
    /**
     * Loads a module by filename and calls its `fa_load_module` function. Note the module name must map to a
     * .dll/.so/.dynlib file depending on the platform at the path you pass as the filename parameter.
     */
    bool (*load_module)(const char* filename);

    /** Frees and unloads a module. */
    bool (*unload_module)(const char* filename);

    void (*check_hotreload)(void);
};

#if defined(FA_LINKS_TO_CORE)
/** Pointer to the global module_api struct. */
extern struct fa_module_api* fa_module_api;
#endif