#if defined(FA_OS_WINDOWS)
#include "os.h"
#include "allocator.h"
#include "atomic.h"
#include "error.h"
#include "log.h"

#include <direct.h>
#include <windows.h>

#pragma region Helpers

static wchar_t* get_error_message(DWORD error_code)
{
    wchar_t* str = NULL;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, error_code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&str, 0, NULL);
    return str;
}

#pragma endregion

#pragma region SharedLib

static fa_os_shared_lib_t load_lib(const char* filename)
{
    FA_CREATE_TEMP_ALLOC(ta);
    DWORD len = GetFullPathNameA(filename, 0, 0, 0);
    char* full_path = fa_temp_alloc(ta, len * sizeof(char));
    GetFullPathNameA(filename, len, full_path, 0);

    const HMODULE handle = LoadLibraryExA(full_path, NULL, LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR | LOAD_LIBRARY_SEARCH_SYSTEM32 | LOAD_LIBRARY_SEARCH_DEFAULT_DIRS);
    if (handle == NULL)
    {
        const DWORD error_code = GetLastError();
        wchar_t* error_message = get_error_message(error_code);
        FA_LOG(Error, "LoadLibrary failed. Error loading %s: %ls (%d)", filename, error_message, error_code);
        LocalFree(error_message);
    }

    FA_DESTROY_TEMP_ALLOC(ta);
    return (fa_os_shared_lib_t){
        .handle = (uint64_t)handle,
        .valid = handle != NULL,
    };
}

static void* get_symbol(fa_os_shared_lib_t lib, const char* symbol_name)
{
    if (lib.valid)
    {
        return (void*)GetProcAddress((HMODULE)lib.handle, symbol_name);
    }
    return NULL;
}

static void close_lib(fa_os_shared_lib_t lib)
{
    if (lib.valid)
    {
        FreeLibrary((HMODULE)lib.handle);
    }
}

static struct fa_os_shared_lib_api shared_lib = {
    .load_lib = load_lib,
    .get_symbol = get_symbol,
    .close_lib = close_lib,
};

#pragma endregion

#pragma region Thread

typedef struct thread_ctx_t
{
    fa_thread_entry entry_func;
    void* user_data;
} thread_ctx_t;

static DWORD __stdcall thread_proc(void* ctx)
{
    thread_ctx_t* ctx_ptr = (thread_ctx_t*)ctx;
    ctx_ptr->entry_func(ctx_ptr->user_data);
    fa_free(ctx_ptr);
    return 0;
}

typedef HRESULT WINAPI SetThreadDescriptionFunc(HANDLE hThread, PCWSTR lpThreadDescription);

static fa_os_thread_t thread_create(fa_thread_entry entry, void* user_data, uint32_t stack_size, const char* debug_name)
{
    thread_ctx_t* ctx = fa_malloc(sizeof(thread_ctx_t));
    ctx->entry_func = entry;
    ctx->user_data = user_data;

    SECURITY_ATTRIBUTES attribs = {
        .bInheritHandle = true,
        .lpSecurityDescriptor = 0,
        .nLength = sizeof(attribs)
    };

    DWORD thread_id = 0;
    const HANDLE handle = CreateThread(&attribs, stack_size, thread_proc, ctx, CREATE_SUSPENDED, &thread_id);
    check(handle != NULL);

    if (debug_name)
    {
        SetThreadDescriptionFunc* SetThreadDescription = (SetThreadDescriptionFunc*)GetProcAddress(GetModuleHandle(L"Kernel32.dll"), "SetThreadDescription");
        if (SetThreadDescription)
        {
            // #todo_core: convert debug_name to wide char string
            SetThreadDescription(handle, L"Worker");
        }
    }
    ResumeThread(handle);

    return (fa_os_thread_t){
        .handle = (uint64_t)handle,
        .thread_id = (uint64_t)thread_id,
    };
}

static void thread_join(fa_os_thread_t thread)
{
    WaitForSingleObject((HANDLE)thread.handle, INFINITE);
}

static void fa_yield_thread(void)
{
    YieldProcessor();
}

static uint32_t fa_get_thread_id(void)
{
    return GetCurrentThreadId();
}

static void sleep_thread(float time)
{
    Sleep((DWORD)(time * 1000));
}

typedef struct fiber_context_t
{
    fa_fiber_entry entry;
    void* user_data;
} fiber_context_t;

enum { MAX_FIBERS = 2048 };
static fiber_context_t fiber_pool[MAX_FIBERS];
static atomic_uint_least32_t next_fiber_index = 0;

static void __stdcall fiber_proc(void* ctx)
{
    const fiber_context_t* ctx_ptr = (fiber_context_t*)ctx;
    ctx_ptr->entry(ctx_ptr->user_data);
}

static fa_os_fiber_t fa_create_fiber(fa_fiber_entry entry, void* user_data, uint32_t stack_size)
{
    fiber_context_t* ctx = &fiber_pool[next_fiber_index];
    atomic_fetch_add_uint32_t(&next_fiber_index, 1);
    ctx->entry = entry;
    ctx->user_data = user_data;

    void* handle = CreateFiberEx(stack_size, stack_size, FIBER_FLAG_FLOAT_SWITCH, fiber_proc, ctx);
    return (fa_os_fiber_t){
        .handle = (uint64_t)handle,
    };
}

static void fa_destroy_fiber(fa_os_fiber_t fiber)
{
    DeleteFiber((void*)fiber.handle);
}

static void fa_switch_to_fiber(fa_os_fiber_t fiber)
{
    SwitchToFiber((void*)fiber.handle);
}

static fa_os_fiber_t fa_convert_thread_to_fiber(void* user_data)
{
    fiber_context_t* ctx = &fiber_pool[next_fiber_index];
    atomic_fetch_add_uint32_t(&next_fiber_index, 1);
    ctx->entry = NULL;
    ctx->user_data = user_data;

    void* handle = ConvertThreadToFiberEx(ctx, FIBER_FLAG_FLOAT_SWITCH);
    return (fa_os_fiber_t){
        .handle = (uint64_t)handle,
    };
}

static void fa_convert_fiber_to_thread(void)
{
    ConvertFiberToThread();
}

static void* fa_get_fiber_user_data(void)
{
    if (IsThreadAFiber())
    {
        const fiber_context_t* ctx = (fiber_context_t*)GetFiberData();
        return ctx->user_data;
    }
    return NULL;
}

static fa_semaphore_t fa_semaphore_create(uint32_t initial_count)
{
    fa_semaphore_t sem;

    HANDLE handle = CreateSemaphoreExW(NULL, initial_count, LONG_MAX, NULL, 0, SYNCHRONIZE | SEMAPHORE_MODIFY_STATE);
    if (handle == NULL)
    {
        DWORD error_code = GetLastError();
        wchar_t* error_message = get_error_message(error_code);
        FA_LOG(Error, "semaphore_create failed: %ls", error_message);
        LocalFree(error_message);
    }
    sem.handle = (uint64_t)handle;
    return sem;
}

static void fa_semaphore_add(fa_semaphore_t sem, uint32_t count)
{
    ReleaseSemaphore((HANDLE)sem.handle, count, NULL);
}

static void fa_semaphore_wait(fa_semaphore_t sem)
{
    DWORD res = WaitForSingleObject((HANDLE)sem.handle, INFINITE);
    if (res == WAIT_FAILED)
    {
        DWORD error_code = GetLastError();
        wchar_t* error_message = get_error_message(error_code);
        FA_LOG(Error, "semaphore_wait failed: %ls", error_message);
        LocalFree(error_message);
    }

    return;
}

static void fa_semaphore_destroy(fa_semaphore_t sem)
{
    CloseHandle((HANDLE)sem.handle);
}

static struct fa_os_thread_api thread = {
    .thread_create = thread_create,
    .thread_join = thread_join,
    .yield_thread = fa_yield_thread,
    .get_thread_id = fa_get_thread_id,
    .sleep = sleep_thread,
    .create_fiber = fa_create_fiber,
    .destroy_fiber = fa_destroy_fiber,
    .switch_to_fiber = fa_switch_to_fiber,
    .convert_thread_to_fiber = fa_convert_thread_to_fiber,
    .convert_fiber_to_thread = fa_convert_fiber_to_thread,
    .get_fiber_user_data = fa_get_fiber_user_data,
    .semaphore_create = fa_semaphore_create,
    .semaphore_add = fa_semaphore_add,
    .semaphore_wait = fa_semaphore_wait,
    .semaphore_destroy = fa_semaphore_destroy,
};

#pragma endregion

#pragma region Info

static uint32_t get_num_logical_processors(void)
{
    SYSTEM_INFO sys_info;
    GetSystemInfo(&sys_info);
    return sys_info.dwNumberOfProcessors;
}

static bool is_debugger_attached(void)
{
    return IsDebuggerPresent();
}

static struct fa_os_info_api info = {
    .get_num_logical_processors = get_num_logical_processors,
    .is_debugger_attached = is_debugger_attached,
};

#pragma endregion

#pragma region File

static char* os_get_cwd(char* buffer, size_t size)
{
    return _getcwd(buffer, (int)size);
}

static fa_file_time_t get_last_modified_time(const char* filename)
{
    fa_file_time_t result = { 0 };

    WIN32_FILE_ATTRIBUTE_DATA attribute_data;
    const DWORD err = GetFileAttributesExA(filename, GetFileExInfoStandard, &attribute_data);
    if (err == 0)
    {
        return result;
    }

    result.handle = (((uint64_t)attribute_data.ftLastWriteTime.dwHighDateTime << 32) | attribute_data.ftLastWriteTime.dwLowDateTime);
    return result;
}

static void set_last_modified_time(const char* filename, fa_file_time_t file_time)
{
    FILETIME time;
    time.dwHighDateTime = file_time.handle >> 32;
    time.dwLowDateTime = file_time.handle & 0xffffffffULL;

    const HANDLE h = CreateFileA(filename, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

    SetFileTime(h, 0, 0, &time);

    CloseHandle(h);
}

static uint64_t get_file_size(const char* filename)
{
    WIN32_FILE_ATTRIBUTE_DATA attribute_data;
    const DWORD err = GetFileAttributesExA(filename, GetFileExInfoStandard, &attribute_data);
    if (err == 0)
    {
        return 0;
    }

    return ((uint64_t)attribute_data.nFileSizeHigh << 32) | attribute_data.nFileSizeLow;
}

static struct fa_os_file_api file = {
    .get_cwd = os_get_cwd,
    .get_last_modified_time = get_last_modified_time,
    .set_last_modified_time = set_last_modified_time,
    .get_file_size = get_file_size,
};

#pragma endregion

#pragma region Time

static fa_clock_t now(void)
{
    LARGE_INTEGER time;
    QueryPerformanceCounter(&time);
    return (fa_clock_t){ .ticks = time.QuadPart };
}

static uint64_t get_tick_freq(void)
{
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    return freq.QuadPart;
}

static double time_sub(fa_clock_t a, fa_clock_t b)
{
    const uint64_t delta = a.ticks - b.ticks;

    const uint64_t freq = get_tick_freq();

    return (double)delta / (double)freq;
}

static struct fa_os_time_api time = {
    .now = now,
    .get_tick_freq = get_tick_freq,
    .time_sub = time_sub,
};

#pragma endregion

static struct fa_os_api os = {
    .shared_lib = &shared_lib,
    .thread = &thread,
    .info = &info,
    .file = &file,
    .time = &time,
};

struct fa_os_api* fa_os_api = &os;

#endif

int foo;