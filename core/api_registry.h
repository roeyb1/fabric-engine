#pragma once

#include <core/core_types.h>

/**
 * The API registry is a centralized place where any api implementation can be stored and later queried by name.
 * An api is just a struct of function pointers which point to implementations of functions. This allows us to easily
 * hot-swap implementations of any api at any time by switching out the api implementation struct.
 */

struct fa_api_registry
{
    /** Returns a pointer to the api struct for a given api name. */
    void* (*get)(const char* api_name);

    /**
     * Register a new api with the registry.
     *
     * Note: the pointer to the api must be guaranteed by the caller to remain valid for the lifetime of the application
     *       or until it is removed.
     */
    void (*add)(void* api, const char* api_name, size_t size);

    /** Remove an api from the registry. */
    void (*remove)(const char* api_name);

    void* (*static_variable)(const char* api_name, const char* variable_name, size_t size);
};

#define fa_get_api(api_name) \
    registry->get((api_name))
#define fa_add_api(reg, api, api_name) \
    reg->add(api, api_name, sizeof(*api))

#define fa_add_or_remove_api(api, api_name, should_load) \
    {                                                    \
        if (should_load)                                 \
            registry->add(api, api_name, sizeof(*api));  \
        else                                             \
            registry->remove(api_name);                  \
    }

#if defined(FA_LINKS_TO_CORE)
/** Pointer to the global registry api interface. */
extern struct fa_api_registry* fa_api_registry;
#endif