#include "unit_tests.h"

#include "core/allocator.h"
#include "core/array.h"
#include "core/error.h"
#include "core/log.h"
#include "core/stretchy_buffers.inl"

#include <string.h>

// ------------------------------------------------------------------------------------------------------------------------------------------

#define MAX_UNIT_TESTS 1024

#define LOG_CATEGORY "UnitTest"

typedef struct unit_test_desc
{
    char test_name[64];
    char test_category[64];
    unit_test_func func;

    const char* failed_file;
    uint32_t failed_line;
} unit_test_desc;

static unit_test_desc tests[MAX_UNIT_TESTS];
static uint64_t num_unit_tests = 0;
static unit_test_desc* current_test = NULL;

// ------------------------------------------------------------------------------------------------------------------------------------------

static void register_unit_test(const char* test_category, const char* test_name, unit_test_func test_func)
{
    const uint64_t test_index = num_unit_tests++;
    check(test_index < MAX_UNIT_TESTS);

    unit_test_desc* desc = tests + test_index;
    desc->func = test_func;
    strncpy(desc->test_name, test_name, FA_ARRAY_COUNT(desc->test_name));
    strncpy(desc->test_category, test_category, FA_ARRAY_COUNT(desc->test_category));
}

static void unregister_unit_test(const char* test_category, const char* test_name)
{
    for (uint32_t i = 0; i < num_unit_tests; ++i)
    {
        if (strcmp(test_category, tests[i].test_category) == 0 && strcmp(test_name, tests[i].test_name) == 0)
        {
            memset(tests + i, 0, sizeof(unit_test_desc));
        }
    }
}

static void run_tests_all(void)
{
    uint64_t* failed_tests = NULL;

    FA_LOG_CATEGORY(Info, "Running all unit tests...");

    // #todo: use the job system to run these in parallel.
    for (uint32_t i = 0; i < num_unit_tests; ++i)
    {
        if (tests[i].func != NULL)
        {
            current_test = tests + i;
            tests[i].func();
            if (tests[i].failed_file != NULL)
            {
                fa_sbuff_push(failed_tests, i);
            }
            current_test = NULL;
        }
    }

    if (failed_tests != NULL)
    {
        FA_LOG_CATEGORY(Info, "\tFailed tests: ");
        for (uint32_t i = 0; i < fa_sbuff_num(failed_tests); ++i)
        {
            const uint64_t failed_test_index = failed_tests[i];
            const unit_test_desc* test_desc = tests + failed_test_index;

            FA_LOG_CATEGORY(Info, "\t\t%s:%s at %s:%u", test_desc->test_category, test_desc->test_name, test_desc->failed_file, test_desc->failed_line);
        }
    }
    else
    {
        FA_LOG_CATEGORY(Info, "\tAll tests passed!");
    }
}

static void mark_fail(const char* file, uint32_t line)
{
    check(current_test);
    current_test->failed_file = file;
    current_test->failed_line = line;
}

// ------------------------------------------------------------------------------------------------------------------------------------------

struct fa_unit_test_api* fa_unit_test_api = &(struct fa_unit_test_api){
    .register_unit_test = register_unit_test,
    .unregister_unit_test = unregister_unit_test,
    .run_tests_all = run_tests_all,
    .mark_fail = mark_fail
};