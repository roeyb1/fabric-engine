extern "C" {
#include "job_scheduler.h"
#include "core_defines.h"
#include "error.h"
#include "hash.inl"
#include "log.h"
#include "os.h"
#include "profiler.h"
}
#include <atomic>
#include <cstdio>

template <typename T, uint32_t MAX_SIZE>
class mpmc_bounded_queue
{
  public:
    mpmc_bounded_queue()
    {
        for (size_t i = 0; i != MAX_SIZE; i += 1)
            buffer_[i].sequence_.store(i, std::memory_order_relaxed);
        enqueue_pos_.store(0, std::memory_order_relaxed);
        dequeue_pos_.store(0, std::memory_order_relaxed);
    }

    void set_size(uint32_t size)
    {
        check(size >= 2 && (size & (size - 1)) == 0);
        buffer_mask_ = size - 1;
    }

    bool enqueue(T const& data)
    {
        cell_t* cell;
        size_t pos = enqueue_pos_.load(std::memory_order_relaxed);
        for (;;)
        {
            cell = &buffer_[pos & buffer_mask_];
            const size_t seq = cell->sequence_.load(std::memory_order_acquire);
            const intptr_t dif = (intptr_t)seq - (intptr_t)pos;
            if (dif == 0)
            {
                if (enqueue_pos_.compare_exchange_weak(pos, pos + 1, std::memory_order_relaxed))
                    break;
            }
            else if (dif < 0)
                return false;
            else
                pos = enqueue_pos_.load(std::memory_order_relaxed);
        }

        cell->data_ = data;
        cell->sequence_.store(pos + 1, std::memory_order_release);

        return true;
    }

    bool dequeue(T& data)
    {
        cell_t* cell;
        size_t pos = dequeue_pos_.load(std::memory_order_relaxed);
        for (;;)
        {
            cell = &buffer_[pos & buffer_mask_];
            const size_t seq = cell->sequence_.load(std::memory_order_acquire);
            const intptr_t dif = (intptr_t)seq - (intptr_t)(pos + 1);
            if (dif == 0)
            {
                if (dequeue_pos_.compare_exchange_weak(pos, pos + 1, std::memory_order_relaxed))
                    break;
            }
            else if (dif < 0)
                return false;
            else
                pos = dequeue_pos_.load(std::memory_order_relaxed);
        }

        data = cell->data_;
        cell->sequence_.store(pos + buffer_mask_ + 1, std::memory_order_release);

        return true;
    }

  private:
    struct cell_t
    {
        std::atomic<size_t> sequence_;
        T data_;
    };

    static size_t const cacheline_size = 64;
    typedef char cacheline_pad_t[cacheline_size];

    cacheline_pad_t pad0_;
    cell_t buffer_[MAX_SIZE];
    size_t buffer_mask_;
    cacheline_pad_t pad1_;
    std::atomic<size_t> enqueue_pos_;
    cacheline_pad_t pad2_;
    std::atomic<size_t> dequeue_pos_;
    cacheline_pad_t pad3_;

    mpmc_bounded_queue(mpmc_bounded_queue const&) = delete;
    void operator=(mpmc_bounded_queue const&) = delete;
};

typedef struct fa_atomic_count_t
{
    std::atomic<uint32_t> counter;
    uint32_t counter_index;
} fa_atomic_count_t;

typedef struct job_t
{
    fa_job_decl_t job_decl;
    fa_atomic_count_t* counter;
    bool auto_free_counter;
} job_t;

typedef struct main_job_t
{
    job_func func;
    void* data;
    fa_atomic_count_t* counter;
} main_job_t;

struct fiber_t;

typedef struct sleeping_fiber_t
{
    uint32_t counter_index;
    struct fiber_t* fiber;
} sleeping_fiber_t;

typedef struct fiber_t
{
    uint32_t fiber_index;
    /* Handle of the thread to which this fiber's job is pinned */
    uint32_t pin_thread_handle;
    fa_os_fiber_t fiber_handle;
    sleeping_fiber_t fiber_to_sleep;

    char debug_name[64];
} fiber_t;

enum {
    MAX_WORKER_THREADS = 128,
    MAX_FIBERS = 2 * MAX_WORKER_THREADS,
    MAX_JOBS = 4096,
};

typedef struct job_scheduler_t
{
    struct fa_os_thread_api* thread_api;
    std::atomic<bool> running;

    uint32_t num_workers;
    fa_os_thread_t worker_threads[MAX_WORKER_THREADS];
    uint32_t worker_thread_ids[MAX_WORKER_THREADS];

    uint32_t num_fibers;
    fiber_t fibers[MAX_FIBERS];
    mpmc_bounded_queue<uint32_t, MAX_FIBERS> free_fiber_indices;

    mpmc_bounded_queue<sleeping_fiber_t, MAX_FIBERS> sleeping_fibers;

    mpmc_bounded_queue<job_t, MAX_JOBS> jobs;

    fa_atomic_count_t counters[MAX_JOBS];
    mpmc_bounded_queue<uint32_t, MAX_JOBS> free_counter_indices;

    fa_semaphore_t semaphores[MAX_WORKER_THREADS];
    fa_hash32_t semaphore_map;

    std::atomic_uint next_thread_to_wake;

    mpmc_bounded_queue<main_job_t, MAX_JOBS> main_thread_jobs;
    fa_semaphore_t main_thread_job_semaphore;
} job_scheduler_t;

static job_scheduler_t job_scheduler;

static void fiber_proc(void* data)
{
    while (!(job_scheduler.running.load(std::memory_order_acquire)))
    {
        job_scheduler.thread_api->yield_thread();
    }

    sleeping_fiber_t sleeping_fiber;
    job_t job;
    while (job_scheduler.running)
    {
        // if we have a fiber to sleep, make sure to sleep it first
        fiber_t* f = (fiber_t*)job_scheduler.thread_api->get_fiber_user_data();
        if (f->fiber_to_sleep.fiber != NULL)
        {
            job_scheduler.sleeping_fibers.enqueue(f->fiber_to_sleep);
            f->fiber_to_sleep.fiber = NULL;
        }

        const bool has_sleeping_fiber = job_scheduler.sleeping_fibers.dequeue(sleeping_fiber);
        if (has_sleeping_fiber)
        {
            // if the fiber's counter is ready to be picked up
            if (job_scheduler.counters[sleeping_fiber.counter_index].counter.load(std::memory_order_acquire) == 0)
            {
                bool accept = sleeping_fiber.fiber->pin_thread_handle == 0 || sleeping_fiber.fiber->pin_thread_handle == job_scheduler.thread_api->get_thread_id();
                if (accept)
                {
                    const fiber_t* current_fiber = (fiber_t*)job_scheduler.thread_api->get_fiber_user_data();
                    // mark this fiber as being free before switching to the new fiber
                    job_scheduler.free_fiber_indices.enqueue(current_fiber->fiber_index);

                    FA_PROFILER_FIBER_ENTER(sleeping_fiber.fiber->debug_name);
                    job_scheduler.thread_api->switch_to_fiber(sleeping_fiber.fiber->fiber_handle);
                    FA_PROFILER_FIBER_LEAVE;
                    FA_PROFILER_FIBER_ENTER(current_fiber->debug_name);

                    // continue so that after switching back we can pick up a new sleeping fiber before grabbing a job
                    continue;
                }
                else
                {
                    // the sleeping fiber was pinned to a thread and this thread is not it.
                    job_scheduler.sleeping_fibers.enqueue(sleeping_fiber);

                    const uint64_t thread_to_wake = sleeping_fiber.fiber->pin_thread_handle;
                    uint64_t temp;
                    job_scheduler.thread_api->semaphore_add(job_scheduler.semaphores[fa_hash_get_ts(&job_scheduler.semaphore_map, thread_to_wake, temp)], 1);
                }
            }
            else
            {
                job_scheduler.sleeping_fibers.enqueue(sleeping_fiber);

                if (sleeping_fiber.fiber->pin_thread_handle != 0)
                {
                    const uint64_t thread_to_wake = sleeping_fiber.fiber->pin_thread_handle;
                    uint64_t temp;
                    job_scheduler.thread_api->semaphore_add(job_scheduler.semaphores[fa_hash_get_ts(&job_scheduler.semaphore_map, thread_to_wake, temp)], 1);
                }
            }
        }

        if (job_scheduler.jobs.dequeue(job))
        {
            // only accept the job if the job doesn't require pinning to a specific thread or if the thread matches the pinned thread id
            const bool accept_job = job.job_decl.pin_thread_handle == 0 || job.job_decl.pin_thread_handle == job_scheduler.thread_api->get_thread_id();
            if (accept_job)
            {
                // pin the current fiber to the thread of the job
                f->pin_thread_handle = job.job_decl.pin_thread_handle;
                if (job.job_decl.func)
                {
                    job.job_decl.func(job.job_decl.user_data);
                }

                if (job.job_decl.pin_thread_handle != 0)
                {
                    fiber_t* current_fiber = (fiber_t*)job_scheduler.thread_api->get_fiber_user_data();
                    current_fiber->pin_thread_handle = 0;
                }

                job.counter->counter.fetch_sub(1);
                if (job.auto_free_counter && job.counter->counter == 0)
                {
                    job_scheduler.free_counter_indices.enqueue(job.counter->counter_index);
                }
            }
            else
            {
                job_scheduler.jobs.enqueue(job);

                const uint64_t thread_to_wake = job.job_decl.pin_thread_handle;
                uint64_t temp;
                job_scheduler.thread_api->semaphore_add(job_scheduler.semaphores[fa_hash_get_ts(&job_scheduler.semaphore_map, thread_to_wake, temp)], 1);
            }
        }
        else if (!has_sleeping_fiber)
        {
            const uint64_t thread_to_wake = job_scheduler.thread_api->get_thread_id();
            uint64_t temp;
            FA_PROFILER_FIBER_LEAVE;
            job_scheduler.thread_api->semaphore_wait(job_scheduler.semaphores[fa_hash_get_ts(&job_scheduler.semaphore_map, thread_to_wake, temp)]);
            FA_PROFILER_FIBER_ENTER(f->debug_name);
        }
    }

    // Shutdown the thread by finding the fiber this thread created originally and then destroying it.

    const uint32_t thread_id = job_scheduler.thread_api->get_thread_id();
    uint32_t thread_index = 0;
    for (uint32_t i = 0; i < job_scheduler.num_workers; ++i)
    {
        if (job_scheduler.worker_thread_ids[i] == thread_id)
        {
            thread_index = i;
            break;
        }
    }

    const fiber_t* fiber = (fiber_t*)job_scheduler.thread_api->get_fiber_user_data();
    if (fiber != &job_scheduler.fibers[thread_index])
    {
        FA_PROFILER_FIBER_LEAVE;
        FA_PROFILER_FIBER_ENTER(job_scheduler.fibers[thread_index].debug_name);
        job_scheduler.thread_api->switch_to_fiber(job_scheduler.fibers[thread_index].fiber_handle);
    }

    job_scheduler.thread_api->convert_fiber_to_thread();
}

typedef struct worker_data_t
{
    uint32_t worker_thread_index;
    std::atomic<uint32_t>* startup_counter;
} worker_data_t;

static void worker_entry(void* data)
{
    const worker_data_t* worker_data = (worker_data_t*)data;
    const uint32_t worker_id = worker_data->worker_thread_index;
    job_scheduler.worker_thread_ids[worker_id] = job_scheduler.thread_api->get_thread_id();
    fiber_t* fiber = &job_scheduler.fibers[worker_id];
    fiber->fiber_to_sleep.fiber = NULL;
    fiber->fiber_index = worker_id;
    snprintf(fiber->debug_name, 64, "Fiber: %d", worker_id);

#if FA_PROFILE
    // prepare the thread name for the profiler
    char name[64];
    snprintf(name, 64, "Job Worker #%d", worker_id);
    FA_PROFILER_SET_THREAD_NAME(name);
#endif

    FA_PROFILER_FIBER_ENTER(job_scheduler.fibers[worker_id].debug_name);
    fiber->fiber_handle = job_scheduler.thread_api->convert_thread_to_fiber(&job_scheduler.fibers[worker_id]);

    worker_data->startup_counter->fetch_sub(1);

    fiber_proc(&job_scheduler.fibers[worker_id]);
    FA_PROFILER_FIBER_LEAVE;
}

static fa_atomic_count_t* post_jobs_impl(fa_job_decl_t* job_decls, uint32_t num_jobs, bool auto_free)
{
    check(num_jobs < MAX_JOBS);

    uint32_t free_counter_index;
    while (!job_scheduler.free_counter_indices.dequeue(free_counter_index))
    {
    }

    fa_atomic_count_t* counter = &job_scheduler.counters[free_counter_index];
    counter->counter.store(num_jobs);

    job_t job;
    job.counter = counter;
    job.auto_free_counter = auto_free;

    // if any job is pinned to a specific threads we need to wake up all threads since there isn't yet a way to wake up a specific thread
    for (uint32_t i = 0; i < num_jobs; ++i)
    {
        job.job_decl = job_decls[i];
        job_scheduler.jobs.enqueue(job);

        if (job_decls[i].pin_thread_handle != 0)
        {
            uint64_t temp;
            const uint64_t semaphore_index = fa_hash_get_ts(&job_scheduler.semaphore_map, job_decls[i].pin_thread_handle, temp);
            job_scheduler.thread_api->semaphore_add(job_scheduler.semaphores[semaphore_index], 1);
        }
        else
        {
            uint32_t thread_to_wake_index = job_scheduler.next_thread_to_wake.fetch_add(1);
            const uint64_t this_thread_id = job_scheduler.thread_api->get_thread_id();

            uint64_t temp;
            // prevent waking up the current thread and potentially deadlocking
            if (job_scheduler.worker_thread_ids[(thread_to_wake_index % job_scheduler.num_workers)] == this_thread_id)
            {
                thread_to_wake_index = job_scheduler.next_thread_to_wake.fetch_add(1);
            }
            thread_to_wake_index = thread_to_wake_index % job_scheduler.num_workers;
            job_scheduler.thread_api->semaphore_add(job_scheduler.semaphores[fa_hash_get_ts(&job_scheduler.semaphore_map, thread_to_wake_index, temp)], 1);
        }
    }

    return counter;
}

static fa_atomic_count_t* post_jobs(fa_job_decl_t* job_decls, uint32_t num_jobs)
{
    return post_jobs_impl(job_decls, num_jobs, false);
}

static void post_jobs_and_free_count(fa_job_decl_t* job_decls, uint32_t num_jobs)
{
    post_jobs_impl(job_decls, num_jobs, true);
}

static void wait_on_count(fa_atomic_count_t* counter)
{
    if (counter->counter.load(std::memory_order_acquire) != 0)
    {
        uint32_t free_fiber_index;
        while (!job_scheduler.free_fiber_indices.dequeue(free_fiber_index))
        {
        }

        fiber_t* next_fiber = &job_scheduler.fibers[free_fiber_index];

        fiber_t* cur_fiber = (fiber_t*)job_scheduler.thread_api->get_fiber_user_data();

        if (cur_fiber == NULL)
        {
            FA_LOG(Fatal, "wait_on_counter() not called from a fiber");
        }

        sleeping_fiber_t sleeping_fiber{};
        sleeping_fiber.counter_index = counter->counter_index,
        sleeping_fiber.fiber = cur_fiber,

        next_fiber->fiber_to_sleep = sleeping_fiber;

        FA_PROFILER_FIBER_ENTER(next_fiber->debug_name);
        job_scheduler.thread_api->switch_to_fiber(next_fiber->fiber_handle);
        FA_PROFILER_FIBER_LEAVE;
        FA_PROFILER_FIBER_ENTER(cur_fiber->debug_name);
    }
}

static void wait_on_count_and_free(fa_atomic_count_t* counter)
{
    wait_on_count(counter);
    job_scheduler.free_counter_indices.enqueue(counter->counter_index);
}

static void wait_on_count_no_fiber(fa_atomic_count_t* counter)
{
    while (counter->counter.load(std::memory_order_acquire) != 0)
    {
        job_scheduler.thread_api->yield_thread();
    }

    job_scheduler.free_counter_indices.enqueue(counter->counter_index);
}

static uint32_t pin_thread_handle(uint32_t worker_index)
{
    return job_scheduler.worker_thread_ids[worker_index];
}

static fa_atomic_count_t* post_task_for_main_thread(job_func func, void* data)
{
    uint32_t free_counter_index;
    while (!job_scheduler.free_counter_indices.dequeue(free_counter_index))
    {
    }

    fa_atomic_count_t* counter = &job_scheduler.counters[free_counter_index];
    counter->counter.store(1);

    job_scheduler.main_thread_jobs.enqueue({ func, data, counter });
    job_scheduler.thread_api->semaphore_add(job_scheduler.main_thread_job_semaphore, 1);

    return counter;
}

static fa_job_scheduler_api job_api = {
    post_jobs,
    post_jobs_and_free_count,
    wait_on_count,
    wait_on_count_and_free,
    wait_on_count_no_fiber,
    pin_thread_handle,
    post_task_for_main_thread,
};

fa_job_scheduler_api* fa_init_job_scheduler(struct fa_os_thread_api* thread_api, uint32_t num_workers,
                                            uint32_t num_fibers, uint32_t fiber_stack_size)
{
    // assert that we haven't already been initialized
    FA_ASSERT(job_scheduler.thread_api == NULL);
    FA_ASSERT(num_workers < MAX_WORKER_THREADS);
    // num fibers has to be greater than 2, greater than the number of workers, and a power of two
    FA_ASSERT(num_fibers < MAX_FIBERS && num_fibers > 2 && (num_fibers & (num_fibers - 1)) == 0);

    job_scheduler.thread_api = thread_api;

    job_scheduler.num_fibers = num_fibers;
    job_scheduler.num_workers = num_workers;
    job_scheduler.sleeping_fibers.set_size(num_fibers);
    job_scheduler.free_fiber_indices.set_size(num_fibers);
    job_scheduler.next_thread_to_wake.store(0);
    job_scheduler.semaphore_map = { 0 };
    job_scheduler.main_thread_jobs.set_size(MAX_JOBS);
    job_scheduler.main_thread_job_semaphore = thread_api->semaphore_create(0);

    std::atomic<uint32_t> startup_counter;
    startup_counter.store(num_workers);
    worker_data_t worker_data[MAX_WORKER_THREADS];
    for (uint32_t i = 0; i < num_workers; ++i)
    {
        worker_data[i].startup_counter = &startup_counter;
        worker_data[i].worker_thread_index = i;
        char debug_name_buffer[128];
        snprintf(debug_name_buffer, 128, "Job Worker: %d", i);
        job_scheduler.worker_threads[i] = thread_api->thread_create(worker_entry, &worker_data[i], 0, debug_name_buffer);

        job_scheduler.semaphores[i] = thread_api->semaphore_create(0);
        fa_hash_add(&job_scheduler.semaphore_map, job_scheduler.worker_threads[i].thread_id, i);
    }

    // wait until thread bootup is complete since we stack allocated our init buffer
    while (startup_counter.load(std::memory_order_acquire) != 0)
    {
        thread_api->yield_thread();
    }

    job_scheduler.free_counter_indices.set_size(MAX_JOBS);
    job_scheduler.jobs.set_size(MAX_JOBS);

    for (uint32_t i = 0; i < MAX_JOBS; ++i)
    {
        job_scheduler.counters[i].counter_index = i;
        job_scheduler.free_counter_indices.enqueue(i);
    }

    // create remaining fibers
    fiber_t fiber{};
    for (uint32_t i = num_workers; i < num_fibers; ++i)
    {
        fiber.fiber_index = i;
        snprintf(fiber.debug_name, 64, "Fiber: %d", i);
        fiber.fiber_handle = thread_api->create_fiber(fiber_proc, &job_scheduler.fibers[i], fiber_stack_size);
        job_scheduler.fibers[i] = fiber;
        job_scheduler.free_fiber_indices.enqueue(i);
    }

    job_scheduler.running.store(true, std::memory_order_release);
    return &job_api;
}

void fa_shutdown_job_scheduler()
{
    job_scheduler.running.store(false);

    // wake up the main thread so it can clean up the rest of the engine
    job_scheduler.thread_api->semaphore_add(job_scheduler.main_thread_job_semaphore, MAX_JOBS);

    /* There is currently a bug when actually shutting down the fibers.
       Not a big deal since the OS can handle cleaning up the threads.

    job_scheduler.running.store(false);

    for (uint32_t i = 0; i < job_scheduler.num_workers; ++i)
    {
        uint64_t temp;
        job_scheduler.thread_api->semaphore_add(job_scheduler.semaphores[fa_hash_get_ts(&job_scheduler.semaphore_map, job_scheduler.worker_thread_ids[i], temp)], MAX_JOBS);
        fa_os_api->thread->thread_join(job_scheduler.worker_threads[i]);
    }
    */
}

fa_job_scheduler_api* fa_get_job_scheduler_api()
{
    return job_scheduler.thread_api ? &job_api : NULL;
}

void fa_main_thread_entry(void)
{
    while (job_scheduler.running)
    {
        job_scheduler.thread_api->semaphore_wait(job_scheduler.main_thread_job_semaphore);

        main_job_t job;
        if (job_scheduler.main_thread_jobs.dequeue(job))
        {
            FA_PROFILER_ZONE_N(ctx, "main thread task", true);
            job.func(job.data);
            job.counter->counter.fetch_sub(1);
            FA_PROFILER_ZONE_END(ctx);
        }
    }
}
