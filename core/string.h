#pragma once

#include <core/core_types.h>

/**
 * Dynamically allocated string object.
 * Prefer this over raw char buffers since it allows for reasoning about ownership and lifetimes.
 * Statically allocated strings and strings which aren't expected to persist longer than a function call will
 * take `const char*` whereas if lifetime management is required, passing `fa_string_t` is preferred.
 *
 * #todo: will contain a pointer to the source allocator as well.
 */
typedef struct fa_string_t
{
    char* str;
    uint64_t len;
} fa_string_t;

struct fa_string_api
{
    /** Constructs a string from a static string. */
    fa_string_t (*create_from_c_str)(const char* c_str);

    void (*copy)(fa_string_t* to, const char* from);
};

#define FA_STRING_API_NAME "fa_string_api"

#if defined(FA_LINKS_TO_CORE)
extern struct fa_string_api* fa_string_api;
#endif
