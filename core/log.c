#include "log.h"
#include "allocator.h"
#include "core_types.h"
#include "error.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(FA_OS_WINDOWS)
void OutputDebugStringA(const char* message);
#endif // defined(FA_OS_WINDOWS)

enum { MAX_LOGGERS = 32 };

static fa_logger_i loggers[MAX_LOGGERS];

static const char* severity_prefix[] = { "[Verbose]", "[Info]", "[Warning]", "[Error]", "[Fatal]" };

static void default_logger_log(fa_logger_t* logger, enum fa_log_severity severity, const char* message)
{
    printf("%s ", severity_prefix[severity]);
    printf("%s\n", message);
    fflush(stdout);

#if defined(FA_OS_WINDOWS)
    OutputDebugStringA(severity_prefix[severity]);
    OutputDebugStringA(": ");
    OutputDebugStringA(message);
    OutputDebugStringA("\n");
#endif // defined(_MSC_VER)
}

static fa_logger_i default_logger = {
    .log = default_logger_log,
};

static void register_logger(fa_logger_i* logger)
{
    uint32_t free_logger_index = MAX_LOGGERS;
    // find the first open logger slot
    for (uint32_t i = 0; i < MAX_LOGGERS; ++i)
    {
        if (loggers[i].log == NULL)
        {
            free_logger_index = i;
            break;
        }
    }

    FA_ASSERT(free_logger_index < MAX_LOGGERS);

    memcpy(&loggers[free_logger_index], logger, sizeof(fa_logger_i));
}

static void unregister_logger(fa_logger_i* logger)
{
    for (uint32_t i = 0; i < MAX_LOGGERS; ++i)
    {
        if (loggers[i].log == logger->log)
        {
            loggers[i].inst = NULL;
            loggers[i].log = NULL;
        }
    }
}

static void log_print(enum fa_log_severity severity, const char* message)
{
    default_logger.log(default_logger.inst, severity, message);

    for (uint32_t i = 0; i < MAX_LOGGERS; ++i)
    {
        if (loggers[i].log != NULL)
        {
            loggers[i].log(loggers[i].inst, severity, message);
        }
    }

    check(severity != FA_LOG_SEVERITY_Fatal);
}

static void log_printf(enum fa_log_severity severity, const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    va_list args2;
    va_copy(args2, args);
    const int ret = vsnprintf(NULL, 0, fmt, args2);
    va_end(args2);

    if (ret < 0)
    {
        return;
    }

    // if the buffer size required is >2048 we need to malloc
    if (ret >= 2048)
    {
        FA_CREATE_TEMP_ALLOC(temp_alloc);
        // extra byte for the null char
        const size_t buff_size = (size_t)ret + 1;
        char* buff = fa_temp_alloc(temp_alloc, buff_size);
        if (buff != NULL)
        {
            vsnprintf(buff, (int)buff_size, fmt, args);
            log_print(severity, buff);
        }
        FA_DESTROY_TEMP_ALLOC(temp_alloc);
    }
    // otherwise we can a stack-based buffer
    else
    {
        char buffer[2048];
        vsnprintf(buffer, sizeof(buffer), fmt, args);
        log_print(severity, buffer);
    }

    va_end(args);
}

struct fa_logger_api* fa_logger_api = &(struct fa_logger_api){
    .register_logger = register_logger,
    .unregister_logger = unregister_logger,
    .print = log_print,
    .printf = log_printf,
};