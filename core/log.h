﻿#pragma once

/**
 * Very simple logging api which allows for registering multiple different logger implementations.
 * Whenever there is a `log` request, we forward the request to all existing loggers.
 *
 * Example: we could have a logger registered which will forward requests to write to a file, one which logs to
 *          the debug output, one which prints to stdout, another which fowards across the network.
 *
 * Future ideas:
 *      - Logs can subscribe to specific categories and only be called if category matches.
 */

/** Log severity. If a log has FATAL severity we will terminate the program. */
enum fa_log_severity {
    FA_LOG_SEVERITY_Verbose,
    FA_LOG_SEVERITY_Info,
    FA_LOG_SEVERITY_Warning,
    FA_LOG_SEVERITY_Error,
    FA_LOG_SEVERITY_Fatal,
};

typedef struct fa_logger_t fa_logger_t;

/** Defines the logger instance. Points to a logger object and a function pointer to the log implementation. */
typedef struct fa_logger_i
{
    /** Essentially the `this` pointer. */
    fa_logger_t* inst;

    /** Called by the logger api and forwards the inst object to the function as if it was a `this` pointer. */
    void (*log)(fa_logger_t* logger, enum fa_log_severity severity, const char* message);
} fa_logger_i;

struct fa_logger_api
{
    /** Register a new logger instance. */
    void (*register_logger)(fa_logger_i* logger);

    /** Unregister a logger instance. */
    void (*unregister_logger)(fa_logger_i* logger);

    /** Simple print function which will forward the message to all loggers. */
    void (*print)(enum fa_log_severity severity, const char* message);

    /** Formats the message string and forwards the result to `print`. */
    void (*printf)(enum fa_log_severity severity, const char* fmt, ...);
};

/** Main logger function macro. Forwards the message to all registered loggers. */
#define FA_LOG(severity, format, ...) fa_logger_api->printf(FA_LOG_SEVERITY_##severity, "" format "", ##__VA_ARGS__)
#define FA_LOG_CATEGORY(severity, format, ...) FA_LOG(severity, "[" LOG_CATEGORY "]: " format, ##__VA_ARGS__)

#define FA_LOGGER_API_NAME "FA_LOGGER_API"

#if defined(FA_LINKS_TO_CORE)
/** Pointer to the global logger api. */
extern struct fa_logger_api* fa_logger_api;
#endif // defined(FA_LINKS_TO_CORE)