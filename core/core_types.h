#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef unsigned char byte_t;

#define _FA_MACRO_CONCAT(a, b) a##b
#define FA_MACRO_CONCAT(a, b) _FA_MACRO_CONCAT(a, b)

#define _FA_PADDING_NAME FA_MACRO_CONCAT(_padding, __LINE__)
#define FA_PAD(n) char _FA_PADDING_NAME[n]

#if defined(_MSC_VER)
#define FA_ALIGN(X) __declspec(align(X))
#else
#define FA_ALIGN(X) __attribute((aligned(X)))
#endif

#include <cglm/cglm.h>
#include <cglm/struct.h>

static const vec3s WORLD_RIGHT_VECTOR = { { 1, 0, 0 } };
static const vec3s WORLD_UP_VECTOR = { { 0, 1, 0 } };
static const vec3s WORLD_FORWARD_VECTOR = { { 0, 0, 1 } };

#include <core/string.h>