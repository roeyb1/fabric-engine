#pragma once

/**
 * The application api and application contexts are ways for a user application to define their own implementations
 * for the main engine control flow.
 *
 * Each application will define a single fa_application_api and will create a single fa_application_t context object.
 * Within the context object could be stored any application-specific data to be retrieved from any of the main functions
 * such as `tick`.
 */

/** Opaque struct which holds the application context */
typedef struct fa_application_t fa_application_t;

typedef struct fa_entity_context_t fa_entity_context_t;

struct fa_application_api
{
    /** Create the application context object. */
    fa_application_t* (*create)(void);

    /** Get a pointer to the application context. */
    fa_application_t* (*get)(void);

    /** Ticks the application one frame forward. Return false to signal to the engine that we want to exit. */
    bool (*tick)(fa_application_t* app);

    /** Signal to the application that we want to exit. */
    void (*exit)(fa_application_t* app);

    /** Destroy the application context after ticking is finished. */
    void (*destroy)(fa_application_t* app);

    fa_entity_context_t* (*get_active_entity_context)(void);
};

#define FA_APPLICATION_API_NAME "FA_APPLICATION_API"
