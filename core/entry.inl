#pragma once

#include "core/memory_stats.h"
#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>
#include <core/unit_tests.h>

#include <cglm/struct.h>
#include <vulkan/vulkan.h>

#include <modules/renderer/renderer.h>
#include <modules/ui/ui.h>
#include <modules/vulkan_backend/vulkan_backend.h>
#include <modules/window/window_api.h>

struct fa_window_api* window_api;
struct fa_vulkan_backend_api* vulkan_backend_api;
struct fa_renderer_api* renderer_api;
struct fa_job_scheduler_api* job_api;
struct fa_application_api* app_api;
struct fa_ui_api* ui_api;

// Initialize the core engine subsystems
static void init_engine()
{
#if !defined(FA_MAIN_DLL)
#error No main dll file defined! Ensure that FA_MAIN_DLL is defined in the exe's main.c file before including entry.inl
#endif

    FA_PROFILER_ZONE(ctx, true);
    // Initialize and register the core apis
    fa_add_api(fa_api_registry, fa_os_api, FA_OS_API_NAME);
    fa_add_api(fa_api_registry, fa_logger_api, FA_LOGGER_API_NAME);
    fa_add_api(fa_api_registry, fa_allocator_api, FA_ALLOCATOR_API_NAME);
    fa_add_api(fa_api_registry, fa_unit_test_api, FA_UNIT_TEST_API_NAME);
    fa_add_api(fa_api_registry, fa_memory_stats_api, FA_MEMORY_STATS_API_NAME);
    fa_add_api(fa_api_registry, fa_string_api, FA_STRING_API_NAME);

    // there currently isn't a fiber api for non-win32 platforms so we can't use the job system there
#if defined(FA_OS_WINDOWS)
    job_api = fa_init_job_scheduler(fa_os_api->thread, fa_os_api->info->get_num_logical_processors(), 128, 10 * 1024);
    fa_add_api(fa_api_registry, job_api, FA_JOB_API_NAME);
#endif

    fa_module_api->load_module("modules/fa_entity_core");
    fa_module_api->load_module("modules/fa_asset_import");
    fa_module_api->load_module("modules/fa_asset_cache");
    fa_module_api->load_module("modules/fa_editor_panels");
    fa_module_api->load_module("modules/fa_window");
    fa_module_api->load_module("modules/fa_vulkan_backend");
    fa_module_api->load_module("modules/fa_renderer");
    fa_module_api->load_module("modules/fa_ui");
    fa_module_api->load_module("modules/fa_ui_widgets");
    fa_module_api->load_module(FA_MAIN_DLL);

    // Load the core modules
    // #todo: figure out a way to have these all load in order and on their own.
    window_api = fa_api_registry->get(FA_WINDOW_API_NAME);
    window_api->init();
    const fa_window_t window = window_api->create_window(1280, 720, "Fabric");
    window_api->set_window_icon(window, "content/textures/logo_no_text.png");

    vulkan_backend_api = fa_api_registry->get(FA_VULKAN_BACKEND_API_NAME);
    vulkan_backend_api->create();

    renderer_api = fa_api_registry->get(FA_RENDERER_API_NAME);
    renderer_api->create();

    ui_api = fa_api_registry->get(FA_UI_API_NAME);
    ui_api->create();

    // Load the main application module now
    app_api = fa_api_registry->get(FA_APPLICATION_API_NAME);
    app_api->create();
    FA_PROFILER_ZONE_END(ctx);

#if defined(FA_DEBUG)
    fa_unit_test_api->run_tests_all();
#endif
}

// Shutdown the core engine subsystems and clean up
static void shutdown_engine()
{
    FA_PROFILER_ZONE(ctx, true);

    ui_api->destroy();
    renderer_api->destroy();
    vulkan_backend_api->destroy();
    window_api->shutdown();

    fa_module_api->unload_module(FA_MAIN_DLL);

    fa_module_api->unload_module("modules/fa_ui_widgets");

    fa_module_api->unload_module("modules/fa_ui");

    fa_module_api->unload_module("modules/fa_renderer");

    fa_module_api->unload_module("modules/fa_vulkan_backend");

    fa_module_api->unload_module("modules/fa_window");

    fa_module_api->unload_module("modules/fa_asset_import");

    fa_module_api->unload_module("modules/fa_entity_core");

    // print the memory stats when the engine is finished running so we can see if there were any leaks.
    fa_memory_stats_api->log_mem_stats();
    FA_PROFILER_ZONE_END(ctx);
}

static void run_main_loop(void* data)
{
    fa_application_t* app = app_api->get();

    while (app_api->tick(app))
    {
        FA_PROFILER_FRAME_MARK
        fa_module_api->check_hotreload();
    }

    app_api->destroy(app);

    fa_shutdown_job_scheduler();
}

int main(int argc, char** argv)
{
    init_engine();

    fa_job_decl_t main_app = {
        .func = run_main_loop,
        .user_data = NULL,
        .pin_thread_handle = 0,
    };

    fa_atomic_count_t* count = job_api->post_jobs(&main_app, 1);
    fa_main_thread_entry();
    job_api->wait_on_count_no_fiber(count);

    shutdown_engine();
    return 0;
}
