#pragma once

#if defined(FA_OS_WINDOWS) && !defined(__clang__)
#define FA_LIB_EXPORT __declspec(dllexport)
#else
#define FA_LIB_EXPORT __attribute__((visibility("default")))
#endif

#if defined(FA_OS_WINDOWS) && !defined(__clang__)
#define PRAGMA_DISABLE_OPTIMIZATIONS __pragma(optimize("", off))
#define PRAGMA_ENABLE_OPTIMIZATIONS __pragma(optimize("", on))
#else
#define PRAGMA_DISABLE_OPTIMIZATIONS _Pragma(optimize("", off))
#define PRAGMA_ENABLE_OPTIMIZATIONS _Pragma(optimize("", on))
#endif

#define FA_IS_DEFINED(macro) _FA_IS_DEFINED(macro)
#define _FA_IS_DEFINED(macro) (#macro[0] == 0 || #macro[0] == '1')

#define fa_max(a, b) ((a) > (b) ? (a) : (b))
#define fa_min(a, b) ((a) < (b) ? (a) : (b))
#define fa_clamp(min, v, max) fa_min(max, fa_max(min, v))

#define FA_INVALID_HANDLE 0
