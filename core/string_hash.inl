#pragma once

/** Encapsulate the string hash type in a struct for typesafety and readability */
typedef struct
{
    uint64_t hash;
} fa_string_hash_t;

// djb2 string hash algorithm by dan bernstein: http://www.cse.yorku.ca/~oz/hash.html
static inline fa_string_hash_t fa_string_hash(const char* str)
{
    uint64_t hash = 5381;
    int c;
    while ((c = *str++))
    {
        hash = ((hash << 5) + hash) + c;
    }
    return (fa_string_hash_t){ hash };
}
