#include "memory_stats.h"
#include <core/core_defines.h>
#include <core/log.h>
#include <core/profiler.h>

#define LOG_CATEGORY "MemStats"

static uint32_t num_active_allocs;

#define USE_MEMORY_TRACKER 0

static void alloc_track(void* ptr, size_t size, const char* file, size_t line)
{
    if (FA_IS_DEFINED(USE_MEMORY_TRACKER))
    {
        // #todo [roey]: implement more thorough tracking. Store alloc data in a hash map or something
        ++num_active_allocs;
        FA_PROFILER_TRACK_ALLOC(ptr, size);
    }
}

static void alloc_free(void* ptr, const char* file, size_t line)
{
    if (FA_IS_DEFINED(USE_MEMORY_TRACKER))
    {
        FA_PROFILER_FREE_ALLOC(ptr);
        --num_active_allocs;
    }
}

static void log_mem_stats(void)
{
    if (FA_IS_DEFINED(USE_MEMORY_TRACKER))
    {
        FA_LOG_CATEGORY(Info, "%d active allocations", num_active_allocs);
    }
}

struct fa_memory_stats_api* fa_memory_stats_api = &(struct fa_memory_stats_api){
    .alloc_track = alloc_track,
    .alloc_free = alloc_free,
    .log_mem_stats = log_mem_stats,
};