#pragma once

#include <core/core_types.h>
#include <core/error.h>

typedef void (*unit_test_func)(void);

struct fa_unit_test_api
{
    void (*register_unit_test)(const char* test_category, const char* test_name, unit_test_func test_func);
    void (*unregister_unit_test)(const char* test_category, const char* test_name);

    void (*run_tests_all)(void);

    void (*mark_fail)(const char* file, uint32_t line);
};

#define FA_TEST_EXPECT_IMPL(pred, file, line)        \
    {                                                \
        if (!(pred))                                 \
        {                                            \
            fa_unit_test_api->mark_fail(file, line); \
            return;                                  \
        }                                            \
    }

#define FA_TEST_EXPECT(pred)                      \
    FA_TEST_EXPECT_IMPL(pred, __FILE__, __LINE__) \
    check(pred)

#define fa_add_or_remove_test(test_api, category, name, func, load) \
    {                                                               \
        if (load)                                                   \
            (test_api)->register_unit_test(category, name, func);   \
        else                                                        \
            (test_api)->unregister_unit_test(category, name);       \
    }                                                               \
    (void)(test_api)

#define FA_UNIT_TEST_API_NAME "FA_UNIT_TEST_API"

#if defined(FA_LINKS_TO_CORE)
extern struct fa_unit_test_api* fa_unit_test_api;
#endif // defined(FA_LINKS_TO_CORE)