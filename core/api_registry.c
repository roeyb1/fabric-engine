#include "api_registry.h"
#include "allocator.h"
#include "core_types.h"
#include "error.h"
#include "string_hash.inl"

#include <stdlib.h>
#include <string.h>

enum { MAX_APIS = 1024 };
enum { MAX_API_FUNCTIONS = 128 };
enum { MAX_STATIC_VARIABLES = 128 };
enum { MAX_NAME_LENGTH = 64 };

typedef struct api_t
{
    char api_name[MAX_NAME_LENGTH];

    void* functions[MAX_API_FUNCTIONS];
} api_t;

typedef struct static_variable_t
{
    char variable_name[MAX_NAME_LENGTH];
    void* ptr;
} static_variable_t;

typedef struct api_static_variables_t
{
    static_variable_t variables[MAX_STATIC_VARIABLES];
    uint32_t num_variables;
} api_static_variables_t;

static api_t apis[MAX_APIS];
static api_static_variables_t variables[MAX_APIS];
static uint64_t name_hashes[MAX_APIS];
static uint32_t num_apis;

static uint32_t find_api_index(const char* api_name)
{
    // #todo: change this api to use the new hashmaps instead of this fake hashmap
    const uint64_t name_hash = fa_string_hash(api_name).hash;
    for (uint32_t i = 0; i < num_apis; ++i)
    {
        if (name_hashes[i] == name_hash && (strcmp(api_name, apis[i].api_name) == 0))
        {
            return i;
        }
    }

    return num_apis;
}

static api_t* get_or_add_empty_api(const char* api_name)
{
    FA_ASSERT(num_apis < MAX_APIS - 1);

    const uint32_t api_index = find_api_index(api_name);
    if (api_index < num_apis)
    {
        return apis + api_index;
    }

    const uint64_t name_hash = fa_string_hash(api_name).hash;

    api_t api = { 0 };
    strcpy(api.api_name, api_name);

    apis[api_index] = api;
    name_hashes[api_index] = name_hash;

    ++num_apis;

    return apis + api_index;
}

static void* get_api(const char* api_name)
{
    // if the api wasn't found, add an empty entry for it so that we can return the correct pointer
    // regardless in which order models are loaded.

    return &(get_or_add_empty_api(api_name)->functions);
}

static void add_api(void* api_impl, const char* api_name, size_t size)
{
    api_t* api = get_or_add_empty_api(api_name);

    memcpy(api->functions, api_impl, size);
}

static void remove_api(const char* api_name)
{
    const uint32_t api_index = find_api_index(api_name);

    if (api_index == num_apis)
    {
        return;
    }

    memset(apis[api_index].functions, 0, sizeof(void*) * MAX_API_FUNCTIONS);
}

static void* static_variable(const char* api_name, const char* variable_name, size_t size)
{
    const uint32_t api_index = find_api_index(api_name);

    if (api_index == num_apis)
    {
        return NULL;
    }

    for (uint32_t i = 0; i < variables[api_index].num_variables; ++i)
    {
        if (strcmp(variables[api_index].variables[i].variable_name, variable_name) == 0)
        {
            return variables[api_index].variables[i].ptr;
        }
    }

    // intentionally using default malloc because I know these memory allocations will be leaked at the end of the program
    // and we don't want to track the allocations
    void* result = malloc(size);
    memset(result, 0, size);
    variables[api_index].variables[variables[api_index].num_variables].ptr = result;
    strcpy(variables[api_index].variables[variables[api_index].num_variables++].variable_name, variable_name);

    return result;
}

struct fa_api_registry* fa_api_registry = &(struct fa_api_registry){
    .get = get_api,
    .add = add_api,
    .remove = remove_api,
    .static_variable = static_variable
};
