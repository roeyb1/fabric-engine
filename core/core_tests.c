#include "unit_tests.h"

#include <stdlib.h>

#include <core/atomic.h>
#include <core/core_types.h>
#include <core/hash.inl>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/os.h>
#include <core/unit_tests.h>

#include <core/profiler.h>

#pragma region Hash Tests

static void run_hash_tests(void)
{
    typedef struct test_struct
    {
        uint64_t val;
    } test_struct;

    typedef struct FA_HASH_T(uint64_t, test_struct) fa_hash_test_t;

    fa_hash_test_t hashmap = { 0 };
    for (uint64_t i = 0; i < 100; ++i)
    {
        test_struct v = { .val = i };
        fa_hash_add(&hashmap, i, v);
    }

    for (uint64_t i = 0; i < 100; ++i)
    {
        const test_struct val = fa_hash_get(&hashmap, i);

        FA_TEST_EXPECT(val.val == i);
    }

    const uint64_t id = 5;
    FA_TEST_EXPECT(fa_hash_remove(&hashmap, id));
    FA_TEST_EXPECT((fa_hash_get(&hashmap, id).val == 0));
    FA_TEST_EXPECT(!fa_hash_remove(&hashmap, id));

    fa_hash_add(&hashmap, id, (test_struct){ .val = 5 });
    FA_TEST_EXPECT(fa_hash_get(&hashmap, id).val == 5);

    fa_hash_free(&hashmap);
}

#pragma endregion

#pragma region Job Tests

static void job_test_func(void* data)
{
    FA_PROFILER_ZONE(ctx, true);
    atomic_uint_least32_t* data_ptr = (atomic_uint_least32_t*)data;
    atomic_fetch_sub_uint32_t(data_ptr, 1);
    FA_PROFILER_ZONE_END(ctx);
}

static void job_spawning_jobs(void* data)
{
    FA_PROFILER_ZONE(ctx, true);
    fa_job_decl_t job_decls[100];
    for (uint32_t i = 0; i < 100; ++i)
    {
        job_decls[i].func = job_test_func;
        job_decls[i].user_data = data;
        job_decls[i].pin_thread_handle = 0;
    }
    fa_atomic_count_t* count = fa_get_job_scheduler_api()->post_jobs(job_decls, 100);
    fa_get_job_scheduler_api()->wait_on_count_and_free(count);
    FA_PROFILER_ZONE_END(ctx);
}

static void run_job_tests(void)
{
    FA_PROFILER_ZONE(ctx, true);
    const struct fa_job_scheduler_api* job_api = fa_get_job_scheduler_api();

    FA_TEST_EXPECT(job_api != NULL);

    atomic_uint_least32_t test_counter = 2 * 100 * 10;
    fa_job_decl_t job_decls[10];
    for (uint32_t i = 0; i < 10; ++i)
    {
        job_decls[i].func = job_spawning_jobs;
        job_decls[i].user_data = &test_counter;
        job_decls[i].pin_thread_handle = 0;
    }
    {
        FA_PROFILER_ZONE(ctx1, true);

        fa_atomic_count_t* count = job_api->post_jobs(job_decls, 10);
        job_api->wait_on_count_no_fiber(count);
        FA_PROFILER_ZONE_END(ctx1);
    }
    {
        FA_PROFILER_ZONE(ctx1, true);
        fa_atomic_count_t* count = job_api->post_jobs(job_decls, 10);
        job_api->wait_on_count_no_fiber(count);

        FA_PROFILER_ZONE_END(ctx1);
    }

    FA_TEST_EXPECT(test_counter == 0);
    FA_PROFILER_ZONE_END(ctx);
}

#pragma endregion

void register_core_tests(struct fa_unit_test_api* unit_test_api)
{
    unit_test_api->register_unit_test("core", "jobs", run_job_tests);
    unit_test_api->register_unit_test("core", "hash_map", run_hash_tests);
}