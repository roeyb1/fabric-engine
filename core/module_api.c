#include "module_api.h"

#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/core_defines.h>
#include <core/error.h>
#include <core/log.h>
#include <core/os.h>
#include <core/profiler.h>
#include <core/unit_tests.h>

#include <stdio.h>
#include <string.h>

#include "string_hash.inl"

#define LOG_CATEGORY "Modules"

typedef struct module_t
{
    fa_os_shared_lib_t library_handle;
    fa_load_module_func loader_func;

    /** The last time this module's dll was modified */
    fa_file_time_t last_modified_time;

    char module_name[64];
} module_t;

enum { MAX_MODULES = 256 };

uint64_t module_name_hashes[MAX_MODULES];
module_t modules[MAX_MODULES];
uint32_t num_modules;
uint32_t hotreload_count;

static const char* get_module_extension(void)
{
#ifdef FA_OS_WINDOWS
    return "dll";
#elif defined(FA_OS_MACOSX)
    return "dylib";
#elif defined(FA_OS_LINUX)
    return "so";
#else
    FA_ASSERT(false && "Unknown platform");
    return NULL;
#endif
}

static const char* get_module_filename(fa_block_alloc_t* temp_alloc, const char* module_name)
{
    const char* prefix = FA_IS_DEFINED(FA_OS_POSIX) ? "lib" : "";

    const uint32_t filename_len = snprintf(NULL, 0, "%s%s.%s", prefix, module_name, get_module_extension()) + 1;
    char* filename = fa_temp_alloc(temp_alloc, filename_len * sizeof(char));
    snprintf(filename, filename_len, "%s%s.%s", prefix, module_name, get_module_extension());

    return filename;
}

// because of OS locks we need to copy the dll before loading it
static const char* copy_library_for_hotreload(fa_block_alloc_t* temp_alloc, const char* module_name)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint32_t num = snprintf(NULL, 0, "%s.hot-reload-%u.%s", module_name, hotreload_count, get_module_extension()) + 1;
    char* dest = fa_temp_alloc(temp_alloc, num * sizeof(char));
    snprintf(dest, num, "%s.hot-reload-%u.%s", module_name, hotreload_count++, get_module_extension());

    const char* source_file = get_module_filename(temp_alloc, module_name);

    FILE* sourcef = fopen(source_file, "rb");
    FILE* destf = fopen(dest, "wb");

    if (!sourcef)
    {
        FA_LOG_CATEGORY(Error, "Failed to copy module for hotreload (%s)", source_file);
        perror("\t");
        FA_PROFILER_ZONE_END(ctx);
        return NULL;
    }

    size_t n, m;
    unsigned char buffer[8192];

    do
    {
        n = fread(buffer, 1, sizeof(buffer), sourcef);
        if (n)
            m = fwrite(buffer, 1, n, destf);
        else
            m = 0;
    } while ((n > 0) && (n == m));

    fclose(destf);
    fclose(sourcef);

    fa_os_api->file->set_last_modified_time(dest, fa_os_api->file->get_last_modified_time(source_file));

    FA_PROFILER_ZONE_END(ctx);
    return m == 0 ? dest : NULL;
}

static uint32_t find_module_index(const char* module_name)
{
    // #todo: change this to the new hashmaps
    const uint64_t module_name_hash = fa_string_hash(module_name).hash;
    for (uint32_t i = 0; i < num_modules; ++i)
    {
        if (module_name_hashes[i] == module_name_hash && strcmp(modules[i].module_name, module_name) == 0)
        {
            return i;
        }
    }
    return num_modules;
}

static bool wait_till_readable(const char* filename)
{
    FA_PROFILER_ZONE(ctx, true);
    // Sometimes while compiling the file might not be fully written to before we notice that it has changed
    // so we need to wait until the OS finished writing the file. We also don't want to wait indefinitely
    // as that would freeze the application and we might be able to recover from it.
    const fa_clock_t start = fa_os_api->time->now();
    enum { MAX_WAIT_TIME = 2 };
    double elapsed = 0.f;
    while (elapsed < (double)MAX_WAIT_TIME)
    {
        FA_CREATE_TEMP_ALLOC(inner);
        elapsed = fa_os_api->time->time_sub(fa_os_api->time->now(), start);

        const uint64_t file_size = fa_os_api->file->get_file_size(filename);
        FILE* file = fopen(filename, "rb");
        if (!file)
        {
            fa_os_api->thread->sleep(0.01f);
            FA_DESTROY_TEMP_ALLOC(inner);
            continue;
        }
        void* buff = fa_temp_alloc(inner, file_size);
        const uint64_t read_size = fread(buff, 1, file_size, file);
        if (file)
            fclose(file);
        if (read_size != file_size)
        {
            fa_os_api->thread->sleep(0.01f);
            FA_DESTROY_TEMP_ALLOC(inner);
            continue;
        }

        FA_DESTROY_TEMP_ALLOC(inner);
        break;
    }

    FA_PROFILER_ZONE_END(ctx);
    return elapsed < (double)MAX_WAIT_TIME;
}

static bool load_module(const char* module_name)
{
    FA_PROFILER_ZONE(ctx, true);
    FA_ASSERT(num_modules < MAX_MODULES - 1);
    checkf(strlen(module_name) < 60, "Module filename exceeds maximum supported length (60 chars)");

    FA_CREATE_TEMP_ALLOC(temp_alloc);

    bool result = false;

    const char* module_filename = get_module_filename(temp_alloc, module_name);

    const char* copy_name = copy_library_for_hotreload(temp_alloc, module_name);
    if (copy_name == NULL)
    {
        result = false;
        goto ret;
    }

    if (!wait_till_readable(copy_name))
    {
        result = false;
        goto ret;
    }

    fa_os_shared_lib_t lib = fa_os_api->shared_lib->load_lib(copy_name);
    if (lib.valid)
    {
        const fa_load_module_func loader_func = fa_os_api->shared_lib->get_symbol(lib, "fa_load_module");
        if (loader_func != NULL)
        {
            const uint32_t module_index = find_module_index(module_name);
            fa_file_time_t last_mod_time = fa_os_api->file->get_last_modified_time(module_filename);

            // We hash based on the initial filename
            module_t new_module = {
                .library_handle = lib,
                .loader_func = loader_func,
                .last_modified_time = last_mod_time,
            };

            strcpy(new_module.module_name, module_name);

            memcpy(modules + module_index, &new_module, sizeof(module_t));

            const uint64_t module_name_hash = fa_string_hash(module_name).hash;
            module_name_hashes[module_index] = module_name_hash;

            ++num_modules;

            loader_func(fa_api_registry, true);

            result = true;
        }
    }

ret:
    FA_DESTROY_TEMP_ALLOC(temp_alloc);
    FA_PROFILER_ZONE_END(ctx);
    return result;
}

static bool unload_module(const char* filename)
{
    const uint32_t module_index = find_module_index(filename);
    if (module_index == num_modules)
    {
        FA_LOG_CATEGORY(Error, "Attempting to unload a module which doesn't exist");
        return false;
    }

    // prevent double unloading of a module
    if (!modules[module_index].library_handle.valid)
    {
        return false;
    }

    modules[module_index].loader_func(fa_api_registry, false);

    // #todo: maybe consider re-enabling this again?
    // had to disable actually closing the library because it can be pretty difficult to manage potentially unloaded memory
    // fa_os_api->shared_lib->close_lib(modules[module_index].library_handle);

    module_name_hashes[module_index] = 0;
    modules[module_index].library_handle.valid = false;
    modules[module_index].loader_func = NULL;

    --num_modules;

    module_name_hashes[module_index] = module_name_hashes[num_modules];
    memcpy(&modules[module_index], &modules[num_modules], sizeof(module_t));

    return true;
}

static bool reload_module(uint32_t module_index)
{
    char module_name[64];
    strcpy(module_name, modules[module_index].module_name);

    FA_LOG_CATEGORY(Info, "Reloading module %s...", module_name);

    unload_module(module_name);

    FA_CREATE_TEMP_ALLOC(temp_alloc);
    const char* filename = get_module_filename(temp_alloc, module_name);
    wait_till_readable(filename);
    FA_DESTROY_TEMP_ALLOC(temp_alloc);

    if (!load_module(module_name))
    {
        FA_LOG_CATEGORY(Warning, "Failed hot reload for %s", module_name);
        return false;
    }

    FA_LOG_CATEGORY(Info, "Hot reload for %s success", module_name);
    return true;
}

static void check_hotreload(void)
{
    // Attached debugger disables our ability to hot reload since the debugger will lock the .pdb file.
    // Prevent hot reloading completely while debugger is attached
    if (fa_os_api->info->is_debugger_attached())
    {
        return;
    }

    FA_PROFILER_ZONE(ctx, true);

    bool was_module_reloaded = false;
    for (uint32_t i = 0; i < num_modules; ++i)
    {
        FA_CREATE_TEMP_ALLOC(temp_alloc);
        const char* module_filename = get_module_filename(temp_alloc, modules[i].module_name);

        const fa_file_time_t modified_time = fa_os_api->file->get_last_modified_time(module_filename);
        if (modules[i].last_modified_time.handle != modified_time.handle)
        {
            reload_module(i);
            was_module_reloaded = true;
        }
        FA_DESTROY_TEMP_ALLOC(temp_alloc);
    }

    if (was_module_reloaded)
    {
        fa_unit_test_api->run_tests_all();
    }
    FA_PROFILER_ZONE_END(ctx);
}

struct fa_module_api* fa_module_api = &(struct fa_module_api){
    .load_module = load_module,
    .unload_module = unload_module,
    .check_hotreload = check_hotreload,
};
