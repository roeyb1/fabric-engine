#pragma once

#include "core_types.h"

/**
 * The job scheduler subsystem manages a pool of worker threads which all execute jobs from a central job queue.
 * However, instead of using regular OS-threads, the worker threads utilize 'fibers' to maximize efficiency.
 *
 * When a job goes to sleep because it is waiting for other jobs to finish, instead of locking it's current thread,
 * the fiber on which it is running will be swapped out for a free fiber and that fiber will be able to continue
 * processing jobs in the job queue. When a worker thread finishes executing a job, it will always check the list of
 * sleeping fibers before picking up a new job to see if any of them are ready to wake up. If so, it will wake up one
 * of the sleeping fibers and resume execution right where it left off.
 */

struct fa_os_thread_api;

/**
 * A thread-safe atomic counter which will be decremented for each job that finishes executing.
 * Once the value reaches 0, indicates that the jobs associated with the counter have finished executing.
 */
typedef struct fa_atomic_count_t fa_atomic_count_t;

/** Defines the template for a job function. */
typedef void (*job_func)(void* user_data);

/** Struct which defines a job for the scheduler. */
typedef struct fa_job_decl_t
{
    /** The function to execute. */
    job_func func;

    /** A pointer to user data that will be passed to the execution function. */
    void* user_data;

    /** The id of the thread on which this job *must* run */
    uint32_t pin_thread_handle;
} fa_job_decl_t;

struct fa_job_scheduler_api
{
    /**
     * Submits a new set of jobs to the job scheduler. Returns a pointer to an atomic counter on which we can wait
     * to determine when all our jobs are finished executing.
     */
    fa_atomic_count_t* (*post_jobs)(fa_job_decl_t* job_decls, uint32_t count);

    /** Submits a new set of jobs to the scheduler and returns the counter to the pool automatically once they finish. */
    void (*post_jobs_and_free_count)(fa_job_decl_t* job_decls, uint32_t count);

    /** Waits on a counter by switching the currently executing fiber out instead of locking the thread. */
    void (*wait_on_count)(fa_atomic_count_t* counter);

    /** Waits on a counter by switching the currently executing fiber out and frees the counter once it completes. */
    void (*wait_on_count_and_free)(fa_atomic_count_t* counter);

    /** Waits on a counter by spinlocking the current thread until it finishes executing. */
    void (*wait_on_count_no_fiber)(fa_atomic_count_t* counter);

    uint32_t (*pin_thread_handle)(uint32_t worker_index);

    /*
     * The Main-Thread-Task api allows arbitrary worker threads to submit short bursts of work to be executed on the main thread.
     * This is reserved for interacting with external libraries which explicitly require functions to be called specifically from
     * the main thread (being the thread from which main() was called.
     *
     * Work on this thread should be extremely short lived and expected to terminate almost immediately since it will block job
     * worker threads.
     */
    fa_atomic_count_t* (*post_task_for_main_thread)(job_func task, void* data);
};

#define FA_JOB_API_NAME "FA_JOB_API"

#if defined(FA_LINKS_TO_CORE)
/** Initializes the job scheduler. Should only be called once during the lifetime of the application! */
extern struct fa_job_scheduler_api* fa_init_job_scheduler(struct fa_os_thread_api* thread_api, uint32_t num_workers,
                                                          uint32_t num_fibers, uint32_t fiber_stack_size);

extern void fa_shutdown_job_scheduler(void);

/** Returns a pointer to the initialized job scheduler api interface. */
extern struct fa_job_scheduler_api* fa_get_job_scheduler_api(void);

extern void fa_main_thread_entry(void);
#endif