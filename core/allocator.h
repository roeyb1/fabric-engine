#pragma once

#include <core/core_types.h>

typedef struct fa_block_alloc_t fa_block_alloc_t;

enum { FA_TEMP_ALLOC_BUFFER_SIZE = 1024 };

typedef struct fa_temp_alloc_buffer_t
{
    byte_t buffer[FA_TEMP_ALLOC_BUFFER_SIZE];
} fa_temp_alloc_buffer_t;

struct fa_allocator_api
{
    void* (*malloc)(size_t size, const char* file, size_t line);
    void* (*realloc)(void* ptr, size_t size, const char* file, size_t line);
    void (*free)(void* ptr, const char* file, size_t line);

    /* Block allocators */
    fa_block_alloc_t* (*create_block_allocator_from_buffer)(byte_t* buffer, size_t size);
    fa_block_alloc_t* (*create_block_allocator)(size_t size);
    void (*destroy_block_alloc)(fa_block_alloc_t* allocator, const char* filename, size_t size);

    void* (*block_realloc)(struct fa_block_alloc_t* inst, void* ptr, size_t new_size, const char* file, size_t line);
};

#define FA_CREATE_TEMP_ALLOC(temp_alloc)                                             \
    /* will trigger an unused variable warning if we don't destroy the allocator; */ \
    uint32_t temp_alloc##_not_destroyed;                                             \
    fa_temp_alloc_buffer_t temp_alloc##_buffer;                                      \
    fa_block_alloc_t* temp_alloc = fa_allocator_api->create_block_allocator_from_buffer(temp_alloc##_buffer.buffer, sizeof(temp_alloc##_buffer.buffer))

#define FA_DESTROY_TEMP_ALLOC(temp_alloc)                                  \
    fa_allocator_api->destroy_block_alloc(temp_alloc, __FILE__, __LINE__); \
    (void)temp_alloc##_not_destroyed

#define fa_temp_alloc(alloc, size) fa_allocator_api->block_realloc(alloc, 0, size, __FILE__, __LINE__)

#define fa_malloc(size) fa_allocator_api->malloc(size, __FILE__, __LINE__)
#define fa_realloc(ptr, size) fa_allocator_api->realloc(ptr, size, __FILE__, __LINE__)
#define fa_free(ptr) fa_allocator_api->free(ptr, __FILE__, __LINE__)

#define FA_ALLOCATOR_API_NAME "FA_ALLOCATOR_API"

#if defined(FA_LINKS_TO_CORE)
extern struct fa_allocator_api* fa_allocator_api;
#endif