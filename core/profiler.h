#pragma once

// Currently this header file is just a wrapper around the Tracy macros
// In the future we might roll our own profiler so this will add a layer that we can abstract off

#if defined(FA_PROFILE)
#include <TracyC.h>

#define FA_PROFILER_ZONE(ctx, active)                                       \
    /* will trigger an unused variable warning if we don't end the zone. */ \
    const uint32_t ctx##_not_destroyed = 0;                                 \
    TracyCZone(ctx, active)

#define FA_PROFILER_ZONE_N(ctx, name, active) \
    uint32_t ctx##_not_destroyed;             \
    TracyCZoneN(ctx, name, active)

#define FA_PROFILER_ZONE_END(ctx) \
    (void)ctx##_not_destroyed;    \
    TracyCZoneEnd(ctx)

#define FA_PROFILER_TRACK_ALLOC(ptr, size) TracyCAlloc(ptr, size)
#define FA_PROFILER_FREE_ALLOC(ptr) TracyCFree(ptr)

#define FA_PROFILER_FRAME_MARK TracyCFrameMark

#define FA_PROFILER_SET_THREAD_NAME(name) TracyCSetThreadName(name)

#define FA_PROFILER_FIBER_ENTER(name) TracyCFiberEnter(name)
#define FA_PROFILER_FIBER_LEAVE TracyCFiberLeave
#else
#define FA_PROFILER_ZONE(ctx, active)
#define FA_PROFILER_ZONE_N(ctx, name, active)
#define FA_PROFILER_ZONE_END(ctx)

#define FA_PROFILER_TRACK_ALLOC(ptr, size)
#define FA_PROFILER_FREE_ALLOC(ptr)

#define FA_PROFILER_FRAME_MARK

#define FA_PROFILER_SET_THREAD_NAME(name)

#define FA_PROFILER_FIBER_ENTER(name)
#define FA_PROFILER_FIBER_LEAVE
#endif