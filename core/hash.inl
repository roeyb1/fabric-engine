#pragma once

#include <string.h>

#include <core/allocator.h>
#include <core/core_types.h>
#include <core/error.h>
#include <core/stretchy_buffers.inl>

static const uint64_t FA_HASH_TOMBSTONE = UINT64_MAX;
static const uint64_t FA_HASH_UNUSED = UINT64_MAX - 1;

/**
 * Usage:
 *      typedef struct FA_HASH_T(k, v) some_hash_table;
 */
#define FA_HASH_T(K, V)                                                                         \
    {                                                                                           \
        /* sbuff */ K* keys;                                                                    \
        /* keys are indices into an external data array*/                                       \
        /* sbuff */ V* values;                                                                  \
        /* store the number of keys since we might have tombstone values in the keys array.     \
           Should always be a power of two to simplify the modulus operations to bitwise and */ \
        uint64_t num_keys;                                                                      \
        uint64_t num_used;                                                                      \
        uint64_t temp;                                                                          \
        V default_val;                                                                          \
    }

#define fa_hash_index(hashmap, key) \
    (_fa_hash_get_index((const uint64_t*)(hashmap)->keys, (hashmap)->num_keys, *(const uint64_t*)&(key)))

#define fa_hash_has(hashmap, key) \
    (fa_hash_index(hashmap, key) != UINT64_MAX)

#define fa_hash_get(hashmap, key)                   \
    ((hashmap)->temp = fa_hash_index(hashmap, key), \
     (hashmap)->temp != UINT64_MAX ? (hashmap)->values[(hashmap)->temp] : (hashmap)->default_val)

#define fa_hash_get_ts(hashmap, key, temp) \
    ((temp) = fa_hash_index(hashmap, key), \
     (temp) != UINT64_MAX ? (hashmap)->values[temp] : (hashmap)->default_val)

#define fa_hash_get_ts_checked(hashmap, key, temp)                                                        \
    ((temp) = _fa_hash_get_index_checked((hashmap)->keys, (hashmap)->num_keys, *(const uint64_t*)&(key)), \
     (temp) != UINT64_MAX ? (hashmap)->values[temp] : (hashmap)->default_val)

#define fa_hash_add(hashmap, k, v)                                                                                                                                                      \
    ((hashmap)->temp = _fa_hash_add((uint64_t**)&((hashmap)->keys), &((hashmap)->num_keys), &((hashmap)->num_used), (void**)(&((hashmap)->values)), *(const uint64_t*)&(k), sizeof(v)), \
     (hashmap)->values[(hashmap)->temp] = (v))

#define fa_hash_remove(hashmap, k) \
    (_fa_hash_remove((uint64_t*)(hashmap)->keys, &((hashmap)->num_keys), &((hashmap)->num_used), *(const uint64_t*)&(k)))

#define fa_hash_free(hashmap) \
    (fa_sbuff_free((hashmap)->keys), fa_free((hashmap)->values))

/* gets the index given a key. Currently using linear probing */
static inline uint64_t _fa_hash_probe(uint64_t key, uint64_t distance, uint64_t num_keys)
{
    // const uint64_t base = key & (num_keys - 1);
    uint32_t* k = (uint32_t*)&key;
    const uint32_t base = (k[0] ^ k[1]) & (num_keys - 1);
    return base + distance;
}

static inline uint64_t _fa_hash_get_index(const uint64_t* keys, uint64_t num_keys, uint64_t key)
{
    if (keys == NULL)
    {
        return UINT64_MAX;
    }

    uint64_t i = _fa_hash_probe(key, 0, num_keys);
    uint64_t distance = 0;
    const uint64_t max_distance = num_keys;
    while (keys[i] != key)
    {
        if (distance > max_distance)
        {
            return UINT64_MAX;
        }
        if (keys[i] == FA_HASH_UNUSED)
        {
            return UINT64_MAX;
        }
        ++distance;
        i = (i + 1) & (num_keys - 1);
    }

    return i;
}

static inline uint64_t _fa_hash_add_no_grow(const uint64_t* keys, uint64_t num_keys, uint64_t key)
{
    uint64_t i = _fa_hash_probe(key, 0, num_keys);

    uint64_t distance = 0;
    const uint64_t max_distance = num_keys;
    while (keys[i] != FA_HASH_TOMBSTONE && keys[i] != FA_HASH_UNUSED)
    {
        if (distance > max_distance)
        {
            break;
        }
        ++distance;
        i = (i + 1) & (num_keys - 1);
    }

    return i;
}

static inline void _fa_hash_grow(uint64_t** keys, uint64_t* num_keys, void** values, size_t value_size)
{
    const uint64_t new_num = *num_keys ? 2 * (*num_keys) : 16;

    uint64_t* new_keys = NULL;

    fa_sbuff_resize(new_keys, new_num);
    check(new_keys != NULL);
    for (uint32_t i = 0; i < new_num; ++i)
    {
        new_keys[i] = FA_HASH_UNUSED;
    }

    void* new_values = fa_malloc(new_num * value_size);

    // compute the new position of every key within the newly resized map
    const uint64_t* old_keys = *keys;
    if (old_keys)
    {
        const uint64_t old_num_keys = *num_keys;
        const void* old_values = *values;
        for (uint64_t i = 0; i < old_num_keys; ++i)
        {
            const uint64_t key = old_keys[i];
            if (key != FA_HASH_UNUSED && key != FA_HASH_TOMBSTONE)
            {
                const uint64_t index = _fa_hash_add_no_grow(new_keys, new_num, key);

                new_keys[index] = key;
                memcpy((char*)new_values + (index * value_size), (const char*)old_values + (i * value_size), value_size);
            }
        }

        fa_free(*values);
    }
    *keys = new_keys;
    *values = new_values;
    *num_keys = new_num;
}

static inline uint64_t _fa_hash_add(uint64_t** keys, uint64_t* num_keys, uint64_t* num_used, void** values, uint64_t key, size_t value_size)
{
    uint64_t index = _fa_hash_get_index(*keys, *num_keys, key);
    if (index == UINT64_MAX)
    {
        // if we exceed the load factor or the key array doesn't exist, resize.
        if ((*keys == NULL) || ((float)*num_used / (float)*num_keys > 0.7f))
        {
            _fa_hash_grow(keys, num_keys, values, value_size);
        }

        index = _fa_hash_add_no_grow(*keys, *num_keys, key);
    }

    check(index < fa_sbuff_num(*keys) && index != UINT64_MAX);
    (*keys)[index] = key;
    *num_used = *num_used + 1;
    return index;
}

static inline bool _fa_hash_remove(uint64_t* keys, uint64_t* num_keys, uint64_t* num_used, uint64_t key)
{
    const uint64_t index = _fa_hash_get_index(keys, *num_keys, key);

    if (index != UINT64_MAX)
    {
        keys[index] = FA_HASH_TOMBSTONE;

        *num_used = *num_used - 1;

        return true;
    }
    return false;
}

typedef struct FA_HASH_T(uint64_t, uint64_t) fa_hash64_t;
typedef struct FA_HASH_T(uint64_t, uint32_t) fa_hash32_t;
typedef struct FA_HASH_T(uint64_t, float) fa_hashfloat_t;
