#pragma once

#include "core_types.h"

#pragma region SharedLib

typedef struct fa_os_shared_lib_t
{
    uint64_t handle;
    bool valid;
} fa_os_shared_lib_t;

struct fa_os_shared_lib_api
{
    fa_os_shared_lib_t (*load_lib)(const char* filename);
    void* (*get_symbol)(fa_os_shared_lib_t lib, const char* filename);
    void (*close_lib)(fa_os_shared_lib_t lib);
};

#pragma endregion

#pragma region Thread

typedef void (*fa_thread_entry)(void* user_data);
typedef void (*fa_fiber_entry)(void* user_data);

typedef struct fa_os_thread_t
{
    uint64_t handle;
    uint32_t thread_id;
} fa_os_thread_t;

typedef struct fa_os_fiber_t
{
    uint64_t handle;
} fa_os_fiber_t;

typedef struct fa_semaphore_t
{
    uint64_t handle;
} fa_semaphore_t;

struct fa_os_thread_api
{
    fa_os_thread_t (*thread_create)(fa_thread_entry entry, void* user_data, uint32_t stack_size, const char* debug_name);
    void (*thread_join)(fa_os_thread_t thread);

    void (*yield_thread)(void);
    uint32_t (*get_thread_id)(void);
    void (*sleep)(float time);

    fa_os_fiber_t (*create_fiber)(fa_fiber_entry entry, void* user_data, uint32_t stack_size);
    void (*destroy_fiber)(fa_os_fiber_t fiber);
    void (*switch_to_fiber)(fa_os_fiber_t fiber);
    fa_os_fiber_t (*convert_thread_to_fiber)(void* user_data);
    void (*convert_fiber_to_thread)(void);
    void* (*get_fiber_user_data)(void);

    fa_semaphore_t (*semaphore_create)(uint32_t initial_count);
    void (*semaphore_add)(fa_semaphore_t sem, uint32_t count);
    void (*semaphore_wait)(fa_semaphore_t sem);
    void (*semaphore_destroy)(fa_semaphore_t sem);
};

#pragma endregion

#pragma region Info

struct fa_os_info_api
{
    uint32_t (*get_num_logical_processors)(void);
    bool (*is_debugger_attached)(void);
};

#pragma endregion

#pragma region File

typedef struct fa_file_time_t
{
    uint64_t handle;
} fa_file_time_t;

struct fa_os_file_api
{
    /** Outputs the current working directory in the passed buffer. */
    char* (*get_cwd)(char* buffer, size_t max_buffer_size);

    /** Returns an OS specific handle to the last time a file was modified */
    fa_file_time_t (*get_last_modified_time)(const char* filename);
    void (*set_last_modified_time)(const char* filename, fa_file_time_t file_time);

    /** Returns the size of a file. */
    uint64_t (*get_file_size)(const char* filename);
};
#pragma endregion

#pragma region Time

typedef struct fa_clock_t
{
    uint64_t ticks;
} fa_clock_t;

struct fa_os_time_api
{
    fa_clock_t (*now)(void);

    uint64_t (*get_tick_freq)(void);

    double (*time_sub)(fa_clock_t a, fa_clock_t b);
};

#pragma endregion

struct fa_os_api
{
    struct fa_os_shared_lib_api* shared_lib;
    struct fa_os_thread_api* thread;
    struct fa_os_info_api* info;
    struct fa_os_file_api* file;
    struct fa_os_time_api* time;
};

#define FA_OS_API_NAME "FA_OS_API"

#if defined(FA_LINKS_TO_CORE)
extern struct fa_os_api* fa_os_api;
#endif
