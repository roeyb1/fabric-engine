#pragma once

#include <core/core_defines.h>
#include <core/core_types.h>
#include <stdlib.h>

// Implementation of strechy buffers inspired by Sean Barrett's stb stretchy buffers

typedef struct fa_sbuff_header_t
{
    uint64_t capacity;
    uint64_t num;
} fa_sbuff_header_t;

/** Gets a pointer to the header of a stretchy buffer */
#define fa_sbuff_header(a) ((fa_sbuff_header_t*)((byte_t*)(a) - sizeof(fa_sbuff_header_t)))

/** Get the number of elements currently in a stretchy buffer */
#define fa_sbuff_num(a) ((a) ? fa_sbuff_header(a)->num : 0)

/** Get the maximum number of elements that can be stored in a stretchy buffer */
#define fa_sbuff_capacity(a) ((a) ? fa_sbuff_header(a)->capacity : 0)

/** Gets the memory footprint of a stretchy buffer */
#define fa_sbuff_size(a) (fa_sbuff_num(a) * sizeof(*(a)))

/** Gets a pointer to the last element of a stretchy buffer */
#define fa_sbuff_end(a) ((a) ? (a) + fa_sbuff_num(a) : 0)

#define fa_sbuff_needs_grow(a, n) ((n) > fa_sbuff_capacity((a)))

#define fa_sbuff_pop(a) ((a)[--fa_sbuff_header(a)->num])

#define fa_sbuff_grow(a, n) ((*(void**)&(a)) = fa_sbuff_grow_internal((void*)a, n, sizeof(*(a))))

#define fa_sbuff_ensure(a, n) (fa_sbuff_needs_grow(a, n) ? fa_sbuff_grow(a, n) : 0)

#define fa_sbuff_set_capacity(a, n) ((*(void**)&(a)) = fa_sbuff_set_capacity_internal((void*)a, n, sizeof(*(a))))

#define fa_sbuff_push(a, v) (fa_sbuff_ensure(a, fa_sbuff_num(a) + 1), (a)[fa_sbuff_header(a)->num++] = (v), (a) + fa_sbuff_header(a)->num - 1)

#define fa_sbuff_push_array(a, vs, n) ((n) ? ((fa_sbuff_ensure(a, fa_sbuff_num(a) + n), memcpy(a + fa_sbuff_num(a), vs, n * sizeof(*(a))), fa_sbuff_header(a)->num += n), 0) : 0)

#define fa_sbuff_resize(a, n) ((fa_sbuff_needs_grow(a, n) ? fa_sbuff_set_capacity(a, n) : 0), (a) ? fa_sbuff_header(a)->num = n : 0)

#define fa_sbuff_free(a) ((*(void**)&(a)) = fa_sbuff_set_capacity_internal((void*)a, 0, sizeof(*(a))))

static inline void* fa_sbuff_set_capacity_internal(void* sbuff, uint64_t new_capacity, uint64_t value_size)
{
    byte_t* p = sbuff ? (byte_t*)fa_sbuff_header(sbuff) : 0;

    const uint64_t num = fa_sbuff_num(sbuff);
    const uint64_t extra_size = sizeof(fa_sbuff_header_t);
    const uint64_t new_size = new_capacity ? value_size * new_capacity + extra_size : 0;

    p = (byte_t*)realloc(p, new_size);

    void* new_sbuff = p ? p + extra_size : p;

    if (new_sbuff)
    {
        fa_sbuff_header(new_sbuff)->num = num;
        fa_sbuff_header(new_sbuff)->capacity = new_capacity;
    }

    return new_sbuff;
}

static inline void* fa_sbuff_grow_internal(void* sbuff, uint64_t num, uint64_t value_size)
{
    const uint64_t capacity = fa_sbuff_capacity(sbuff);
    if (capacity >= num)
    {
        return sbuff;
    }

    // resize to either double the capacity current capacity or 2x the old size.
    const uint64_t new_num = fa_max(capacity ? capacity * 2 : 16, num);
    return fa_sbuff_set_capacity_internal(sbuff, new_num, value_size);
}