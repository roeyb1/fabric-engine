#include <core/allocator.h>
#include <core/error.h>
#include <core/string.h>

#include <string.h>

static fa_string_t create_from_c_str(const char* str)
{
    check(str != NULL);
    fa_string_t result = { 0 };
    result.len = strlen(str);
    result.str = fa_malloc(sizeof(char) * result.len + 1);
    strcpy(result.str, str);
    return result;
}

static void copy_to_str(fa_string_t* to, const char* from)
{
    check(to != NULL && from != NULL);
    to->len = strlen(from);
    to->str = fa_realloc(to->str, sizeof(char) * to->len + 1);
    strcpy(to->str, from);
}

struct fa_string_api* fa_string_api = &(struct fa_string_api){
    .create_from_c_str = create_from_c_str,
    .copy = copy_to_str
};